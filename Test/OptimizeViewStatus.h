//
//  OptimizeViewStatus.h
//  iopsys Wifi Joe
//
//  Created by Kurt Vermeersch on 14/04/14.
//
//
#include <iostream>
#include "OptimizeViewController.h"
#include <pthread.h>
#include "Observable.h"
#include "CloudFriendsWrapper.h"

@interface OptimizeViewStatus : ObservableInt  {
@public
    CloudFriendsWrapper* client;
    std::string gwJid;
    std::string permanentSsid;
    std::string permanentChannel;
    std::string testChannel;
    int optimizeGraphStatus;
    int currentGraphStatus;
    bool stop;
    int secondsToTest;
    std::string wifiIf;
    bool gwIsLocalAccessible;
    bool lTestOngoing;
    long lStart;
    double measuredSpeedReceiving;
    double measuredSpeedSending;
    double measuredSpeedFromEvents;
    int measuredDataFromEvents;
    int measuredTimeFromEvents;
    pthread_t runner;
}

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap andMaxSecondsToTest:(int)secs;
-(void) start;
-(void) setChannelToTest:(std::string) channel;
-(void) run;
-(void) determineConnection;
-(bool) transmit:(std::string) bytesPerSecond maxSeconds: (long) lSeconds maxBytes: (long) lBytes;
-(bool) available:(int) port;
-(void) stopRunning;
-(void) channelSet;


@end
