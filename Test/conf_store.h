#ifndef CONF_STORE_H
#define CONF_STORE_H


extern "C" int configInit(char *inputSrc);
extern "C" void configExecActions(const char *fileName);
extern "C" char * configGet(const char * section, const char *name, char *buf, int bufsize, bool isMandatory = false) ;
extern "C" long configGetLong(const char * section, const char *name, long defVal) ;
extern "C" int configSet(const char * section, const char *name, const char *value);
extern "C" int configSetLong(const char *section, const char *name, long value);

#endif
