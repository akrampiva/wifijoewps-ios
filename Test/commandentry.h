#ifndef COMMAND_ENTRY_H__
#define COMMAND_ENTRY_H__

#include <list>
#include <string>
#include <sys/time.h>
#include "paramentry.h"

class EventNotifier;
class Observable;

class CommandEntry
{
    public:

		static const std::string PUBLIC_OBSERVABLE_ID_PARAM_NAME;
		static const std::string PRIVATE_OBSERVABLE_ID_PARAM_NAME;

        typedef std::list<ParamEntry*> ParamList_t;

        enum execAction {
            execActionInit,
            execActionExecute,
            execActionCancel,
            execActionComplete
        };

        enum execState {
            execStateInit,
            execStateExecuting,
            execStateCanceled,
            execStateCompleted
        };

        CommandEntry(const std::string &cmdName, const std::string &cmdDescr);
        CommandEntry(const char *cmdName, const char *cmdDescr);
        virtual ~CommandEntry();


        void registerInputParam(const std::string &paramName, 
                                ParamEntry::paramType type, 
                                const std::string &paramDescr);
        void registerReplyParam(const std::string &paramName, 
                                ParamEntry::paramType type, 
                                const std::string &paramDescr);
        void registerPublicEventParam(const std::string &paramName, 
                                        ParamEntry::paramType type, 
                                        const std::string &paramDescr);
        void registerPrivateEventParam(const std::string &paramName, 
                                        ParamEntry::paramType type, 
                                        const std::string &paramDescr);

        std::string commandName(void);
        std::string commandDescr(void);

        bool isInputSet(const std::string &name);
        bool isReplySet(const std::string &name);
        bool isPublicObservable();
        bool isPrivateObservable();
        void getInput(const std::string &name, std::string *value);
        std::string getInput(const std::string &name);
        void getInput(const std::string &name, ParamEntry::valueList_t &valueList);
        void getReply(const std::string &name, std::string *value);
        std::string getReply(const std::string &name);
        void getReply(const std::string &name, ParamEntry::valueList_t &valueList);

        /* The Public available events are addressible by an ID that is
         * known up front. It is not needed to discover this first.
         * The node is created using the logical path: JID.topic.cmd
         */
        void getPublicObservableId(std::string *value);
        /* The Private Id is owned by the application which provides the Observable object.
         * When the command arrives, the observable ID is used to send the commands to.
         * The application first needs to call setPrivateObservableID before 
         * sending the command if it wants to receive events.
         */
        void getPrivateObservableId(std::string *value);
        ParamEntry *findInputParam(const std::string &name);
        ParamEntry *findReplyParam(const std::string &name);
        ParamEntry *findEventParam(const std::string &name);


        void setInput(const std::string &name, const std::string &value);
        void setInput(const std::string &name, const ParamEntry::valueList_t &valueList);
        void setReply(const std::string &name, const std::string &value);
        void setReply(const std::string &name, const ParamEntry::valueList_t &valueList);
         
        /* Public observable is owned by the command structure that 
         * provides the service
         * The ObservableID is where the events can be found
         * setPublicId will be created and set during registration of the command
         * after setting the event parameters as a service
         * Applications should not call this
         * Applications should call setPublicObservable() instead
         * if they want to receive events
         */
        void setPublicId(const std::string &Id);
        void setPublicObservable(void);

        /* Private Observable is owned by the application that requests to be notified
         * When the application no longer needs the eventing, it needs to destroy the 
         * Observable object.
         */
        void setPrivateObservable(Observable *observable);

        void clearInputValues(void);
        void clearReplyValues(void);

        const ParamList_t &inputParams(void);
        const ParamList_t &replyParams(void);
        const ParamList_t &eventParams(void);

        EventNotifier*  getEventNotifier();

        static void fillList(const std::string &value, ParamEntry::valueList_t &vList);

        virtual void execute(void);

        void serialize(std::string &serialized, bool description=true, bool values=true, 
                        bool input=true, bool reply=true, bool events=true);

        /* For internal use only */
        void setPrivateId(const std::string &Id);

        execAction eAction;
        execState eState;
        std::string from; /* The JID where the cmd comes from */

    private:
        void clearParamValues(CommandEntry::ParamList_t paramList);
        void registerParam(const std::string &paramName, ParamEntry::paramType type, const std::string &paramDescr, CommandEntry::ParamList_t &paramList);
        bool isSet(ParamEntry *entry);
        void getParamValue(ParamEntry *entry, std::string *value);
        void getParamValue(ParamEntry *entry, ParamEntry::valueList_t &valueList);
        ParamEntry* findParam(const std::string &paramName, const CommandEntry::ParamList_t &paramList);
        void setParam(ParamEntry *entry, const std::string &value);
        void setParam(ParamEntry *entry, const ParamEntry::valueList_t &valueList);

        /* Called during the registration process of the command parameters
         * the events will be set to a certain type
         * Either Private, where between caller and callee a private eventing channel is setup
         * Either Public, where multiple users can connect to receive events
         */
        bool makePrivateObservable();
        bool makePublicObservable();

        ParamList_t inPList;
        ParamList_t outPList;
        ParamList_t eventPList;

        std::string lCmdName;
        std::string lCmdDescr;

        int observableType;
        std::string privateId;
        std::string publicId;
};

class CommandHandler
{
public:
        enum ErrorType {
            ErrorCancel,
            ErrorUnknown
        };
        virtual ~CommandHandler(){};
        virtual void commandReply(const std::string &topic,
                              CommandEntry *cmd, const std::string & from) = 0;
        virtual void commandError(const std::string &topic, CommandEntry *cmd,
                              const std::string & from, CommandHandler::ErrorType err) = 0;
};

class CommandEventHandler : public CommandHandler
{
private:
        std::string subscribedNode;        
        bool isSubscribed;
public:
        CommandEventHandler();
        virtual ~CommandEventHandler();
        virtual void commandEvent(const std::string  eventSubject, const std::string value) = 0;
        bool subscribe(const std::string &node);
        bool unsubscribe();
};

class Observable
{
    public:
        Observable();
        virtual ~Observable();
        const std::string & getId(void);
    private:
        std::string observableId;
};

class BlockedCommandResult
{
    public:
        enum {REPLY, ERROR, EVENT} resultType;

        // Command result and errors
        std::string topic;
        CommandEntry *cmd;
        std::string from;
        CommandHandler::ErrorType err;
#if 0
        // for eventing
        std::string eventSubject;
        std::string value;
#endif
};

class BlockedCommandHandler : public CommandHandler
{
public:
      BlockedCommandHandler();
      virtual ~BlockedCommandHandler();
      BlockedCommandResult waitForResult(void);
      virtual void commandReply(const std::string &topic, CommandEntry* cmd, const std::string& from);
      virtual void commandError(const std::string &topic, CommandEntry* cmd, const std::string& from, CommandHandler::ErrorType err);
private:
      void signal();
      BlockedCommandResult result;
      int pipeFd[2];
};


class EventNotifier
{
public:
      EventNotifier(std::string observableId, const CommandEntry::ParamList_t &eventParams);
      virtual ~EventNotifier();
      void trigger(std::string eventSubject, std::string value);
      void triggerForced(std::string eventSubject, std::string value);
      void trigger(std::string eventSubject, const ParamEntry::valueList_t &valueList);
private:
      std::string observerableId;
      CommandEntry::ParamList_t eventParams;
      struct timeval lastTime;
};

#endif
