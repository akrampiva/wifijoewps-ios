#include <stdlib.h>
#include "corecommunication.h"
#include "speedtest.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <vector>
#include <sstream>

SpeedTest::SpeedTest(const std::string dst, int testSeconds, ssize_t maxBytes)
{
    lDst = dst;
    lSeconds = testSeconds;
    lBytes = maxBytes;
    lTestOngoing = false;
    lMeasuredSpeedDown = 0;
    lMeasuredSpeedUp = 0;
    lTotalBytes = 0;
}

SpeedTest::~SpeedTest()
{
}

bool SpeedTest::closeTransfers()
{
    CoreCommunication cc;
    cc.closeTransferSessions(lSessionIdList);
    return true;
}

bool SpeedTest::isPortAvailable(int port){
    int sockfd;
    struct sockaddr_in serv_addr;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        // TODO: Error handling
        printf("Error creating socket");
        return false;
    }
    // connect the socket to the local LAN server
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    serv_addr.sin_port = htons(port);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(struct sockaddr)) < 0) {
        //syslog(LOG_ERR, "ERROR connecting to: %s:%d", ipAddr, port);
        printf("Port: %d available", port);
        close(sockfd);
        return true;
    }
    printf("Port: %d not available", port);
    close(sockfd);
    return false;
}

bool 
SpeedTest::receive(std::string &bytesPerSecond)
{
    if (lTestOngoing) {
        printf("Test already ongoing, wait for it to complete\n");
        return false;
    }
    CoreCommunication cc;
    lTestOngoing = true;
    lMeasuredSpeedDown = 0;
    lTotalBytes = 0;
    /* Create a proxy where we locally accept a socket that can connect with
     * the created proxy tunnel. We use local address 127.0.0.1:5556 for this.
     * The other side will be setup with an element that writes zeros
     * We will measure the throughput speed in the thread.
     */
    int port =5556;
    while (!isPortAvailable(port) && port<65535){
        port++;
    }
    std::ostringstream ss;
    ss << "127.0.0.1:" << port;
    std::string loc(ss.str());
    Observable *pObs = new Observable();
    int res = cc.createProxies(lDst, 1, lSessionIdList, "accept", loc,
                                "readZero", "", NULL, this, pObs);
    if (res < 0) {
        printf("Failed to create the needed proxy tunnel\n");
        delete pObs;
        lTestOngoing = false;
        return false;
    }
    int sock = cc.connectToProxyAccept(loc);
    if (sock == -1) {
        printf("Failed to setup the connection\n");
        cc.closeTransferSessions(lSessionIdList);
        delete pObs;
        lTestOngoing = false;
        return false;
    }
    printf("Initializing timer to calculate transfer speed\n");
    time_t myStart;
    bool firstIteration = true;
    /* Pump data for a while */
    ssize_t totalRead = 0;
    ssize_t maxToRead = lBytes;
    double totalTestTime = lSeconds;
    time_t start = time(0);
    double seconds_since_start = difftime( time(0), start);
    double secondLater = 1;
    while ((seconds_since_start < totalTestTime) && (totalRead < maxToRead)) {
        char buf[64*1024];
        int n = read(sock, buf, 64*1024);
        if (n <= 0) {
            printf("ERROR reading from socket \r\n");
            if(seconds_since_start == 0){
                seconds_since_start = 1;
            }
            break;
        } else if (firstIteration){
            firstIteration=false;
            myStart=time(0);
            continue;
        }
        totalRead += n;
        seconds_since_start = difftime( time(0), start);
        if (secondLater < seconds_since_start) {
            printf("Progress: %ld bytes\n", totalRead);
            secondLater += 1;
        }
    }
    printf("\n=============================================\n");
    /* Calculate speed */
    double seconds_since_start_calc = difftime( time(0), myStart);
    if (seconds_since_start_calc == 0) seconds_since_start_calc = 1;
    lMeasuredSpeedDown = (totalRead/(seconds_since_start_calc));
    printf(">> Download speed: bytes per second %f (%fMB/s)\n",
           lMeasuredSpeedDown, lMeasuredSpeedDown/(1024 * 1024));
    /* Close connection */
    close(sock);
    /* Close transfer session */
    cc.closeTransferSessions(lSessionIdList);
    delete pObs;
    lTestOngoing = false;
    return true;
}

double SpeedTest::getMeasuredSpeedDown(){
    return lMeasuredSpeedDown;
}

double SpeedTest::getMeasuredSpeedUp(){
    return lMeasuredSpeedUp;
}

ssize_t SpeedTest::getBytesTransfered(){
    return lTotalBytes;
}

bool 
SpeedTest::transmit(std::string &bytesPerSecond)
{
    if (lTestOngoing) {
        printf("Test already ongoing, wait for it to complete\n");
        return false;
    }
    lTestOngoing = true;
    lMeasuredSpeedUp = 0;
    lTotalBytes = 0;
    /* Create a proxy where we locally accept a socket that can connect with
     * the created proxy tunnel. We use local address 127.0.0.1:5557 for this.
     * The other side will be setup with an element that discards the input
     * Events are requested which will show the progress from the other side
     */
    CoreCommunication cc;
    int port =5656;
    while (!isPortAvailable(port) && port<65535){
        port++;
    }
    std::ostringstream ss;
    ss << "127.0.0.1:" << port;
    std::string loc(ss.str());

    Observable *pObs = new Observable();
    int res = cc.createProxies(lDst, 1, lSessionIdList, "accept", loc,
                                "writeDiscard", "", NULL, this, pObs);
    if (res < 0) {
        printf("Failed to create the needed proxy tunnel\n");
        lTestOngoing = false;
        delete pObs;
        return false;
    }
    int sock = cc.connectToProxyAccept(loc);
    if (sock == -1) {
        printf("Failed to setup the connection\n");
        cc.closeTransferSessions(lSessionIdList);
        lTestOngoing = false;
        delete pObs;
        return false;
    }
    printf("Initializing timer to calculate transfer speed\n");
    lStart = time(0);
    /* Pump data for a while */
    ssize_t totalWrite = 0;
    ssize_t maxToWrite = lBytes;
    double totalTestTime = lSeconds;
    time_t start = time(0); 
    double seconds_since_start = difftime( time(0), start);
    double secondLater = 1;
    while ((seconds_since_start < totalTestTime) && (totalWrite < maxToWrite)) {
        char buf[64*1024];
        int n = write(sock, buf, 64*1024);
        if (n <= 0) {
            printf("ERROR writing to socket\n");
            if(seconds_since_start == 0){
                seconds_since_start = 1;
            }
            break;
        }
        totalWrite += n;
        seconds_since_start = difftime( time(0), start);
        if (secondLater < seconds_since_start) {
            printf("Progress: %ld bytes\n", totalWrite);
            secondLater += 1;
        }
    }
    printf("\n=============================================\n");
    /* Calculate speed */
    printf("Bytes read %ld, over time %f (%fMB/s)\n",
            totalWrite, seconds_since_start, 
            (totalWrite/(seconds_since_start * 1024 * 1024)));

    char buf[64];
    sprintf(buf, "%f", lMeasuredSpeedUp);
    bytesPerSecond = buf;
    /* Close connection */
    close(sock);
    /* Close transfer session */
    cc.closeTransferSessions(lSessionIdList);
    delete pObs;
    lTestOngoing = false;
    return true;
}

void 
SpeedTest::commandError(const std::string &topic, CommandEntry *cmd,
                    const std::string & from, CommandEventHandler::ErrorType err)
{
}

void 
SpeedTest::commandReply(const std::string &topic, CommandEntry *cmd, const std::string & from)
{
}

void 
SpeedTest::commandEvent(const std::string  eventSubject, const std::string value)
{
	printf("testCoreMgr::%s received event with subject %s and value %s\r\n", __FUNCTION__,
			eventSubject.c_str(), value.c_str());
    SessionIdList::iterator it = lSessionIdList.begin();
    while (it != lSessionIdList.end()) {
        unsigned found = eventSubject.find_last_of(*it);
        if (found == std::string::npos) {
            return;
        }
        std::string param = eventSubject.substr((*it).size() + 1);
        if (param == "Tx") {
            if (value == "0") {
                /* Set the timer to this moment in time 
                 * We want to measure the transfer speed, not the transfer time
                 * plus the setup time.
                 * The setup time is rather large per session
                 */
                printf("Initializing timer to calculate transfer speed\n");
                lStart = time(0);
                return;
            }
            double seconds_since_start = difftime( time(0), lStart);
            lTotalBytes = atol(value.c_str());
            if (seconds_since_start == 0) seconds_since_start = 1;
            lMeasuredSpeedUp = (lTotalBytes/(seconds_since_start));
            printf("+++Event speed: bytes per second %f (%fMB/s)\n",
                    lMeasuredSpeedUp, lMeasuredSpeedUp/(1024 * 1024));
            return;
        }
        it++;
    }
}
