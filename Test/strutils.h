//
//  strutils.h
//  Test
//
//  Created by Kurt Vermeersch on 05/02/13.
//
//

#ifndef __Test__strutils__
#define __Test__strutils__

#include <iostream>
#include <list>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <algorithm>
#include <string>

void split(const std::string& str, const std::string& delimiters , std::list<std::string>& tokens);

#endif /* defined(__Test__strutils__) */
