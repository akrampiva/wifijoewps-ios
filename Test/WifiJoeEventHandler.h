//
//  WifiJoeEventHandler.h
//  Test
//
//  Created by Kurt Vermeersch on 25/02/13.
//
//

#ifndef Test_WifiJoeEventHandler_h
#define Test_WifiJoeEventHandler_h

//
//  WifiJoeEventHandler.cpp
//  Test
//
//  Created by Kurt Vermeersch on 25/02/13.
//
//
#include "WifiJoeEventHandler.h"
#include "commandentry.h"

class WifiJoeEventHandler : public CommandEventHandler
{
private:
    CommandEntry* reply;
    bool error;
public:
    
    WifiJoeEventHandler(){
        reply=NULL;
        error=false;
    }
    
    bool gotReply(){
        if(reply != NULL){
            return true;
        }
        return false;
    }
    
    CommandEntry* getReply(){
        return reply;
    }
    
    bool isError(){ 
        return error;
    }
    
    void commandReply(const std::string &topic,
                      CommandEntry *cmd, const std::string & from) {
        reply=cmd;
        error=false;
    }
    
    void commandError(const std::string &topic, CommandEntry *cmd,
                      const std::string & from, CommandEventHandler::ErrorType err) {
        reply=cmd;
        error=true;
    }
    
    void commandEvent(const std::string  eventSubject, const std::string value){
    };

};

#endif
