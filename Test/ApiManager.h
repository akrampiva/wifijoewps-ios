//
//  ApiManager.h
//  iopsys Wifi Joe
//
//  Created by PIVA on 21/03/14.
//
//

#import <Foundation/Foundation.h>
#define SERVER_ALERT 548
#define GATEWAY_ALERT 555
#define UPDATE_ALERT 455

#define A_TOPIC @"guestMgr"
#define A_APINAME @"Wifi Joe"

@class CloudFriendsWrapper;
@interface ApiManager : NSObject
{
    
}

+ (ApiManager *)sharedInstance;

- (BOOL) isApiAvailableWithCloudFriendWrapper:(CloudFriendsWrapper*) cf;

- (void) showAlertWithTitle:(NSString*)title andMessage:(NSString*) message andDelegate:(UIViewController*)delegate andTag:(int)tag;

- (BOOL) connectedToTheServer:(CloudFriendsWrapper*) cf;

- (BOOL) gatewayAvailable:(CloudFriendsWrapper*) cf;

- (void) makeAPIsAvailableWithdeviceAccount:(NSString*) deviceAccount andTopic:(NSString*)topic andApiName:(NSString*)apiName andCloudFriendWrapper:(CloudFriendsWrapper*) cf finishBlock:(void (^)(BOOL installed))block;
@end
