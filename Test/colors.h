//
//  colors.h
//  Test
//
//  Created by Kurt Vermeersch on 22/11/13.
//
//


#define THEME_COLOR [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]
#define YELLOW_COLOR [UIColor colorWithRed:0.969 green:0.867 blue:0 alpha:1] /*#f7dd00*/
#define RED_COLOR [UIColor colorWithRed:0.816 green:0.137 blue:0.078 alpha:1] /*#d02314*/
#define GREEN_COLOR [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]


#ifndef Test_colors_h
#define Test_colors_h

#import <UIKit/UIKit.h>


    /*
    <color name="theme_color">#FFe22e3d</color>
    R: 226 G: 46 B: 61
    */
    extern UIColor *  const themecolor;
    /*
    <color name="theme_color_gradient1">#FFd22e3d</color>
    R: 210 G: 46 B: 61
    */
    extern UIColor *  const themecolorgradient1;
    /*
    <color name="theme_color_gradient2">#FFc02e3d</color>
    R: 192 G: 46 B: 61
    */
    extern UIColor *  const themecolorgradient2;
    /*
    <color name="theme_color_light">#FFf24e5d</color>
    R: 242 G: 78 B: 93
    */
    extern UIColor *  const themecolorlight;
    /*
    <color name="theme_color_light_gradient1">#FFff8e9d</color>
    R: 255 G: 142 B: 157
    */
    extern UIColor *  const themecolorlightgradient1;
    /*
    <color name="theme_color_light_gradient2">#FFf26e7d</color>
    R: 242 G: 110 B: 125
    */
    extern UIColor *  const themecolorlightgradient2;
    extern UIColor *  const themecolorverylight;



#endif
