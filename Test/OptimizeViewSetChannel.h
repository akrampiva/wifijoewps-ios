//
//  OptimizeViewSetChannel.h
//  iopsys Wifi Joe
//
//  Created by Kurt Vermeersch on 14/04/14.
//
//

#ifndef iopsys_Wifi_Joe_OptimizeViewSetChannel_h
#define iopsys_Wifi_Joe_OptimizeViewSetChannel_h

#import "OptimizeViewStatus.h"
#import "CloudFriendsWrapper.h"
#include "EventingProtocol.h"

@class OptimizeViewStatus;

class OptimizeViewSetChannel {
public:
    OptimizeViewStatus *wStatus;
    CloudFriendsWrapper *client;
    
    OptimizeViewSetChannel() {
    }
    
    void setChannel(int channel) {
        WlSettings currentSettings=client->cf->getWireless();
        client->cf->setWireless(WlSettings(currentSettings.ssid, channel));
        [wStatus channelSet];
    }
    
};

#endif
