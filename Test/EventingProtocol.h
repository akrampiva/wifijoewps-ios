//
//  ReloadEventProtocol.h
//  Test
//
//  Created by Kurt Vermeersch on 10/12/13.
//
//

#ifndef Test_ReloadEventProtocol_h
#define Test_ReloadEventProtocol_h

@protocol EventingProtocol
- (void)speedTestStopped;
- (void)optimizeGraphUpdate;
- (void)channelSet;
- (void)notLocalConnected;
- (void)isLocalConnected;
- (void)noInternetAvailable;
- (void)internetAvailable;
- (void)connectionDetermined;
- (void)failedTransmitTest;
@end


#endif
