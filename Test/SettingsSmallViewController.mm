//
//  SettingsSmallViewController.cpp
//  Test
//
//  Created by Kurt Vermeersch on 22/01/14.
//
//

#include "SettingsSmallViewController.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#import "deviceutils.h"
#import "cloudfriendsAppDelegate.h"

@implementation SettingsSmallViewController

@synthesize usernameField;
@synthesize passwordField;
@synthesize passwordFieldEye;
@synthesize serverField;
@synthesize versionLabel;
@synthesize masterView;

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    if(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad){
        const int movementDistance = 130;
        float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    settingChanged = YES;
}

- (void)viewDidLoad
{
    openA=0;
    settingChanged=FALSE;
    [super viewDidLoad];
    [usernameField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [passwordField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [serverField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    appdelegate.settingIsShowing = NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    appdelegate.settingIsShowing = YES;
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    usernameField.layer.borderColor=themecolor.CGColor;
    [usernameField.layer setCornerRadius:10.0f];
    usernameField.borderStyle = UITextBorderStyleRoundedRect;
    usernameField.layer.borderWidth=1.0;
    passwordField.layer.borderColor=themecolor.CGColor;
    [passwordField.layer setCornerRadius:10.0f];
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    passwordField.layer.borderWidth=1.0;
    passwordField.secureTextEntry = YES;
    serverField.layer.borderColor=themecolor.CGColor;
    [serverField.layer setCornerRadius:10.0f];
    serverField.borderStyle = UITextBorderStyleRoundedRect;
    serverField.layer.borderWidth=1.0;
    serverField.secureTextEntry = FALSE;
    [super viewWillAppear:animated];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"SettingsTitle", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    NSString* un=[[NSUserDefaults standardUserDefaults] stringForKey:@"name_preference"];
    if(un!=nil){
        usernameField.text=un;
    }
    NSString* pw=[[NSUserDefaults standardUserDefaults] stringForKey:@"password_preference"];
    if(pw!=nil){
        passwordField.text=pw;
    }
    NSString* srv=[[NSUserDefaults standardUserDefaults] stringForKey:@"server_preference"];
    if(srv!=nil){
        serverField.text=srv;
    }
    NSString* version =[[NSString alloc] initWithFormat:@"Version %@",  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    versionLabel.text=version;
}

- (IBAction)back{
    if(settingChanged)
        [self showActionSheet];
    else
        [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}



-(void)showActionSheet
{
    
    UIActionSheet *actionSheet;
    NSString* deleteMessage = [NSString stringWithFormat:@"Do you want to apply the new settings?"];
    actionSheet = [[UIActionSheet alloc] initWithTitle:deleteMessage
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"NO",@"")
                                destructiveButtonTitle:NSLocalizedString(@"YES",@"")
                                     otherButtonTitles:nil];
    
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    if(IS_IPAD)
    {
        [actionSheet addButtonWithTitle:@"NO"];
    }
	[actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - Action sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
        [self saveSettings];
        
	} else if (buttonIndex==1) {
        
    }
    [self.navigationController popViewControllerAnimated:YES];
    [actionSheet release];
}

- (id) saveSettings {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:usernameField.text forKey:@"name_preference"];
    [prefs setObject:passwordField.text forKey:@"password_preference"];
    [prefs setObject:serverField.text forKey:@"server_preference"];
    [prefs synchronize];
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) switchPassVisibility:(id)sender{
    UIImage *btnImage1;
    if (openA == 0){
        openA = 1;
        btnImage1 = [UIImage imageNamed:@"permanent_access_eye_closed.png"];
        passwordField.secureTextEntry = FALSE;
    }
    else{
        openA = 0;
        btnImage1 = [UIImage imageNamed:@"permanent_access_eye_open.png"];
        passwordField.secureTextEntry = TRUE;
    }
    [passwordFieldEye setImage:btnImage1];
}


-(IBAction)textFieldDoneEditing:(id)sender{
    // Save the changed settings
    /*NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if(sender==usernameField){
        [prefs setObject:usernameField.text forKey:@"name_preference"];
    } else if(sender==passwordField){
        [prefs setObject:passwordField.text forKey:@"password_preference"];
    } else if(sender==serverField){
        [prefs setObject:serverField.text forKey:@"server_preference"];
    }*/
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
