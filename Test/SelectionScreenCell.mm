//
//  SelectionScreenCell.mm
//  Test
//
//  Created by Kurt Vermeersch on 08/11/13.
//
//

#include "SelectionScreenCell.h"
#import "deviceutils.h"
#import "colors.h"

@implementation SelectionScreenCell
@synthesize imageView = _imageView;
@synthesize labelView = _labelView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor=themecolor.CGColor;
        if(IS_IPHONE_5) {
            [self.layer setCornerRadius:10.0f];
        } else if(IS_IPHONE){
            [self.layer setCornerRadius:10.0f];
        } else {
            [self.layer setCornerRadius:14.0f];
        }
        self.layer.borderWidth=1.0;
        if(IS_IPHONE_5) {
            self.imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.origin.x+35, self.bounds.origin.y+20, self.bounds.size.width-70, self.bounds.size.height-60)] autorelease];
        } else if(IS_IPHONE){
            self.imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.origin.x+35, self.bounds.origin.y+20, self.bounds.size.width-70, self.bounds.size.height-60)] autorelease];
        } else {
            self.imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.origin.x+90, self.bounds.origin.y+40, self.bounds.size.width-180, self.bounds.size.height-120)] autorelease];
        }
        self.imageView.contentMode = UIViewContentModeScaleAspectFit ;
        self.imageView.clipsToBounds = YES;
        [self.contentView addSubview:self.imageView];
        self.labelView = [[[UILabel alloc] initWithFrame:CGRectMake(self.bounds.origin.x+5, self.bounds.size.height-35, self.bounds.size.width-10, 30)] autorelease];
        //self.labelView.frame=CGRectMake(self.bounds.origin.x+25, self.bounds.size.height-40, self.bounds.size.width-50, 40);
        [self.labelView setNumberOfLines:0];
        self.labelView.textColor = themecolor;
        self.labelView.textAlignment = NSTextAlignmentCenter;
        self.labelView.backgroundColor = [UIColor clearColor];
        if(IS_IPHONE_5)
        {
            self.labelView.font= [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
            
        } else if(IS_IPHONE){
            self.labelView.font= [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
        }
        else
        {
            self.labelView.font= [UIFont fontWithName:@"Helvetica Neue" size:24.0f];
        }
        [self.contentView addSubview:self.labelView];
        
    }
    return self;
}

- (IBAction)btn:(id)sender {
}
@end