#ifndef SERVICE_PRESENCE_HANDLER_H___
#define SERVICE_PRESENCE_HANDLER_H___
#include <string>

class ServicePresenceHandler
{
    public:
        enum State {
            Up,
            Down
        };
        virtual ~ServicePresenceHandler() {};
        virtual void handlePresence(std::string from, State state) = 0;
};

#endif

