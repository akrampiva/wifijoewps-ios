//
//  WE2InstallViewController.h
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"


@interface WE2InstallViewController : UIViewController
{
    NSString* connectionType;
    CloudFriendsWrapper *client;
}
@property (nonatomic,strong) IBOutlet UIButton* cabledButton;
@property (nonatomic,strong) IBOutlet UIButton* wirelessButton;
@property (nonatomic,strong) IBOutlet UIButton* cabledOnlyButton;
@property (nonatomic,strong) IBOutlet UILabel* infoLabel;
@property (nonatomic,strong) IBOutlet UIImageView* cabledImage;
@property (nonatomic,strong) IBOutlet UIImageView* cabledWirelessImage;

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap andConnectionType:(NSString*)type;

@end
