#ifndef _SPEED_TEST_H___
#define _SPEED_TEST_H___


#include <string>
#include <time.h>
#include "commandentry.h"
#include "servicecommunication.h"
#include "corecommunication.h"


class SpeedTest : public CommandEventHandler, CoreCommunication
{
    public:
        /* Provide the destination to test with
         * Provide the maximum of time the test will take
         * Provide the maximum bytes that will be transferred
         * The test will be performed until either the maximum time or 
         * amount to bytes requested is reached
         */
        SpeedTest(const std::string dst, int testSeconds, ssize_t maxBytes);
        virtual ~SpeedTest();

        /* Return false if the test failed */
        virtual bool receive(std::string &bytesPerSecond);
        virtual bool transmit(std::string &bytesPerSecond);
        virtual bool closeTransfers();

        bool isPortAvailable(int port);
        double getMeasuredSpeedDown();
        double getMeasuredSpeedUp();
        ssize_t getBytesTransfered();
        virtual void commandReply(const std::string &topic,
                                  CommandEntry *cmd, const std::string & from);
        virtual void commandError(const std::string &topic, CommandEntry *cmd,
                                  const std::string & from, CommandHandler::ErrorType err);
        virtual void commandEvent(const std::string  eventSubject, const std::string value);
    private:
        std::string lDst;
        int lSeconds;
        ssize_t lBytes;
        bool lTestOngoing;
        double lMeasuredSpeedDown;
        double lMeasuredSpeedUp;
        ssize_t lTotalBytes;
        time_t lStart;
        SessionIdList lSessionIdList;
};

#endif

