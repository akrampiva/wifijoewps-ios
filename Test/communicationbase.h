#ifndef COMMUNICATIONBASE_H__
#define COMMUNICATIONBASE_H__

#include <list>
#include <string>
#include "commandentry.h"

class CommunicationBase
{
    public:
        typedef std::list<CommandEntry*> CmdList_t;

        enum execError {
            execErrorBadRequest,
            execErrorNotImplemented,
            execErrorForbidden
        };

        CommunicationBase(const std::string &topicName);
        CommunicationBase(void);
        virtual ~CommunicationBase();

        void setName(const std::string &topicName);
        std::string & name(void);

        void addCommand(CommandEntry *entry);
        virtual CommandEntry *findCommand(const std::string cmdName);
        const CmdList_t &list(void);
        virtual void serialize(std::string &serialized);

    private:

        CmdList_t cmdList;

        std::string lName;

};

#endif
