//
//  LanDevice.h
//  iopsys Wifi Joe
//
//  Created by PIVA on 01/08/14.
//
//

#import <Foundation/Foundation.h>

@interface LanDevice : NSObject

@property (nonatomic,strong) NSString* jid;
@property (nonatomic,strong) NSString* type;

@end
