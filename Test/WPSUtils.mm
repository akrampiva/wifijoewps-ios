//
//  WPSUtils.m
//  iopsys Wifi Joe WPS
//
//  Created by PIVA on 09/09/14.
//
//

#import "WPSUtils.h"

@implementation WPSUtils


- (id) initWithClient:(CloudFriendsWrapper*) cfwrap andDelegate:(WPSViewController*)dgt{
    self = [super init];
    if (self) {
        // Custom initialization
        client = cfwrap;
        delegate = dgt;
        wpsEventHandler=new WPSStatusCommandEventHandler();
        wpsEventHandler->delegate=delegate;
    }
    return self;
    
}

- (void) launch{
    std::string result = client->cf->getWPSStatus(wpsEventHandler);
     NSLog(@"wpstatus result %s",result.c_str());
}

- (void) stopRunning{
    wpsEventHandler = nil;
    delegate = nil;
}

@end
