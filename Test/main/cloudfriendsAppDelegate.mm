//
//  cloudfriendsAppDelegate.m
//
//  The UIApplicationDelegate for the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//
#import "cloudfriendsAppDelegate.h"
#import "LoginViewController.h"
#import "deviceutils.h"
//#import "MongooseSingleton.h"
#import <HockeySDK/HockeySDK.h>

@implementation cloudfriendsAppDelegate

@synthesize window = _window;
@synthesize navigationController = _navigationController;

- (void)dealloc
{
    [_window release];
    [_navigationController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"69220506897b177e690cd18f34440cb4"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator
     authenticateInstallation];

    _settingIsShowing = NO;
    _loginIsShowing = NO;
    //[[MongooseSingleton sharedMongooseSingleton] start];
    [application setStatusBarHidden:NO];
    LoginViewController* loginViewContr;
    if(IS_IPHONE_5)
    {
        NSLog(@"IPHONE 5");
        loginViewContr= [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    } else if(IS_IPHONE){
        NSLog(@"IPHONE");
        loginViewContr= [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    }
    else{
        NSLog(@"IPAD");
        loginViewContr= [[LoginViewController alloc] initWithNibName:@"LoginViewController-iPad" bundle:nil];
    }
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewContr];
    self.window.rootViewController=self.navigationController;
    [self.window makeKeyAndVisible];
    [self.window setNeedsLayout];
    [self.window setNeedsDisplay];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    //[[MongooseSingleton sharedMongooseSingleton] stop];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}


@end
