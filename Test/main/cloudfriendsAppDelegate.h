//
//  cloudfriendsAppDelegate.h
//
//  The UIApplicationDelegate for the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface cloudfriendsAppDelegate : NSObject <UIApplicationDelegate>{
    IBOutlet UIWindow *window;
    IBOutlet UINavigationController *navigationController;
}

@property BOOL settingIsShowing;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic,retain) IBOutlet UINavigationController *navigationController;
@property BOOL loginIsShowing;

@end
