//
//  main.m
//
//  The main method that starts the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cloudfriendsAppDelegate.h"

int main(int argc, char *argv[])
{
    signal(SIGPIPE,SIG_IGN);
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([cloudfriendsAppDelegate class]));
}
