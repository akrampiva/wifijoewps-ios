//
//  WPSViewController.m
//  iopsys Wifi Joe WPS
//
//  Created by PIVA on 15/09/2014.
//
//

#import "WPSViewController.h"
#import "WPSUtils.h"
#import "deviceutils.h"

@interface WPSViewController ()

@end

@implementation WPSViewController

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap{
    self = [super init];
    if (self) {
        // Custom initialization
        client = cfwrap;
    }
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSString *className = [[self class] description];
    if(IS_IPAD)
    {
        self = [super initWithNibName:[NSString stringWithFormat:@"%@-iPad",className] bundle:nibBundleOrNil];
    }
    else{
        self = [super initWithNibName:className bundle:nibBundleOrNil];
    }
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"wps", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    
    
    [self initForWPS];
}

-(void)initForWPS{
    wpsActivated = NO;
    [self activateWPS];
    
    // init
    [_searchBar setHidden:NO];
    [_searchBar startAnimating];
    [_resultImageView setHidden:YES];
    [_infoText setText:NSLocalizedString(@"wps_activation", nil)] ;
}

-(void)activateWPS{
    if (!wpsActivated || wpsStatus == nil) {
        wpsStatus = [[WPSUtils alloc] initWithClient:client andDelegate:self];
        [wpsStatus launch];
    }
}

-(void)removeWPS {
    // TODO Auto-generated method stub
    if([_searchBar isAnimating])
        [_searchBar stopAnimating];
    if (wpsStatus) {
        [wpsStatus stopRunning];
        wpsStatus = nil;
    }
    wpsActivated = false;
}
//wps delegate function
-(void) updateStatusOrMac:(NSString*)statusOrMac {
    if (statusOrMac) {
        if ([statusOrMac isEqualToString:@"active"]) {
            wpsActivated = true;
            dispatch_async(dispatch_get_main_queue(), ^{
                [_infoText setText:NSLocalizedString(@"Please_press_the_WPS_button", nil)] ;
                [_searchBar setHidden:NO];
                if(![_searchBar isAnimating])
                    [_searchBar startAnimating];
                [_resultImageView setHidden:YES];
            });

        } else if (wpsActivated && [statusOrMac isEqualToString:@"timeout"]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self removeWPS];
                [_infoText setText:NSLocalizedString(@"Failed_to_connect_your_device", nil)] ;
                [_searchBar setHidden:YES];
                if([_searchBar isAnimating])
                    [_searchBar stopAnimating];
                [_resultImageView setHidden:NO];
                [_resultImageView setImage:[UIImage imageNamed:@"warning.png" ]];
            });
            

        } else if (wpsActivated && [statusOrMac rangeOfString:@"MAC"].location != NSNotFound) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self removeWPS];
                [_infoText setText:NSLocalizedString(@"Success_a_device_was_now_connected", nil)] ;
                [_searchBar setHidden:YES];
                if([_searchBar isAnimating])
                    [_searchBar stopAnimating];
                [_resultImageView setHidden:NO];
                [_resultImageView setImage:[UIImage imageNamed:@"check.png" ]];
            });
           

        }
    }
}
- (IBAction)back{
    [self removeWPS];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
