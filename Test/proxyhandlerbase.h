#ifndef PROXY_HANDLER_BASE_H___
#define PROXY_HANDLER_BASE_H___

#include <list>
#include <string>
#include <stdio.h>

class ProxyEventHandler
{
    public:
        enum Status {
            ProxyDescriptorAvailable,
            ProxyEstablished,
            ProxyClosed
        };
        virtual ~ProxyEventHandler() {};
        virtual void proxyStatus(const std::string &sessionID, const Status &status) = 0;
};

/* The info list is defined outside the the ProxyHandler
 * This will allow extending the information for specific handlers
 */
class ProxyHandlerSessionInfo
{
    public:
        std::string sessionID;
        std::string connect;
        ProxyEventHandler *pEHcaller; // Caller can register to receive status callbacks
        ProxyEventHandler *pEHinternal; // Internal callbacks needed to take action
        int fd;
};

/* 
 * Create a proxy handler that will be called as a specific mount path
 * for the proxy transfer commands.
 * The handlers allow to register new 'poxy ends'. Examples are:
 * - Writing the proxy data directly to a files
 * - Providing a listening socket to connect to
 * - Connecting to any TCP server
 * - Why not creating a UDP/TCP bridge?
 * When the 'create proxy'command is executed, the phScheduleCreateDescriptor function
 * is called to create a local file descriptor. For sockets this can take a while
 * so this connection setup process should run in the background without blocking the 
 * proxy tunnel setup. When the descriptor is available, the callback needs to be called.
 * The proxy process will take the descriptor and add it to the select loop and wait for 
 * the descriptor to have data available. The read function will be called to get data and
 * send it down the proxy. When the proxy pipe has data, it will present it through the 
 * write function.
 * The descriptor is also used to check if data can be written on the descriptor.
 * When -1 is returned on reading or writing, close is called and the creation of a new
 * descriptor is scheduled. When this returns -1, the process is stopped and the proxy tunnel is
 * closed.
 * It is possible to decouple the file descriptor in the select loop from 'whatever' generates
 * or accepts the data locally.
 */
class ProxyHandler 
{
    public:
        ProxyHandler() {};
        virtual ~ProxyHandler() {};
        virtual std::string id() = 0;
        virtual std::string description() = 0;
        virtual ProxyHandlerSessionInfo *find(const std::string &sessionID);
        virtual int phScheduleCreateDescriptor(const std::string &sessionID, 
                                                const std::string &connect,
                                                ProxyEventHandler *pEHcaller,
                                                ProxyEventHandler *pEHinternal) = 0;
        virtual int phDescriptor(const std::string &sessionID);
        virtual void proxyEstablished(const std::string &sessionID);
        virtual ssize_t phRead(const std::string &sessionID, void *buf, size_t count);
        virtual ssize_t phWrite(const std::string &sessionID, const void *buf, size_t count);
        virtual int phClose(const std::string &sessionID);
        virtual int phRescheduleCreateDescriptor(const std::string &sessionID) = 0;
        virtual void phRemove(const std::string &sessionID) = 0;
    protected:
        typedef std::list<ProxyHandlerSessionInfo*> SessionInfoList;
        SessionInfoList sessionInfoList;
};

/* 
 * The manager is used for proxy tunnels to select
 * which handler should be used. This is done by the mount path. The mount path
 * in the command is mapped to the 'id' of the proxyhandler.
 */
class ProxyHandlerManager
{
    public:
        typedef std::list<ProxyHandler*> ProxyHandlerList;

        static ProxyHandlerManager *instance();
        ~ProxyHandlerManager();

        void registerHandler(ProxyHandler *pHandler);
        void unregisterHandler(ProxyHandler *pHandler);

        ProxyHandler* find(const std::string &id);

    private:
        ProxyHandlerManager();

        ProxyHandlerList proxyHandlerList;
};

#endif
