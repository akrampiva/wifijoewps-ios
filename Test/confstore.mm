/*
*   @file LoginViewController.h
*   @author Kurt Vermeersch
*
*   The login viewcontroller handles the ios application login screen functions
*
*   Copyright 2013 Cloud Friends. All rights reserved.
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include "conf_store.h"

/*
 * Initialize the configuration parameter store
 *
 * @return 0 on success, erro code when failed
 */
int configInit(char *inputSrc) {
	syslog(LOG_DEBUG, "configInit");
	return 0;
}

/*
 * Get a configuration parameter
 *
 * @return the value on success, an empty string when the config parameter was not found
 */
char *
configGet(const char *section, const char *name, char *buf, int bufsize, bool isMandatory) {
    syslog(LOG_DEBUG, "configGetIOS");
    if(strcmp(section,"Core") == 0){
        if(strcmp(name, "logLevel") == 0){
            strcpy(buf,"LOG_DEBUG");
            return buf;
        } else if(strcmp(name, "printLogInConsole") == 0){
            strcpy(buf,"true");
            return buf;
        } else if(strcmp(name, "cert_mount") == 0){
            NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
            strcpy(buf,[bundlePath UTF8String]);
            return buf;
        } else if(strcmp(name, "pubsub_service") == 0){
            NSString* se=[[NSUserDefaults standardUserDefaults] stringForKey:@"server_preference"];
            if(se!=nil){
                strcpy(buf,[[@"pubsub." stringByAppendingString:se] UTF8String]);
            } else {
                strcpy(buf,"pubsub.intenocloud.com");
            }
            return buf;
        } else if(strcmp(name, "http_transfer_threads") == 0){
            strcpy(buf,"5");
            return buf;
        } else if(strcmp(name, "cloudfriends_host") == 0){
            printf("cloudfriends_host\n");
            NSString* se=[[NSUserDefaults standardUserDefaults] stringForKey:@"server_preference"];
            if(se!=nil){
                strcpy(buf,[se UTF8String]);
            } else {
                strcpy(buf,"intenocloud.com");
            }
            return buf;
        } else if(strcmp(name, "deviceType") == 0){
            strcpy(buf,"mobile");
            return buf;
        }
    } else if(strcmp(section,"Certificates") == 0){
        if(strcmp(name, "cert0") == 0){
            strcpy(buf,"netmediacloud.cer");
            return buf;
        } else if(strcmp(name, "cert1") == 0){
            strcpy(buf,"intenocloud.crt");
            return buf;
        } else if(strcmp(name, "cert2") == 0){
            strcpy(buf,"cloudfriends.crt");
            return buf;
        } else if(strcmp(name, "cert3") == 0){
            strcpy(buf,"startcom_intermediate.crt");
            return buf;
        } else if(strcmp(name, "cert4") == 0){
            strcpy(buf,"iopsyscloud.cer");
            return buf;
        } else if(strcmp(name, "cert5") == 0){
            strcpy(buf,"startcom_root.crt");
            return buf;
        } else if(strcmp(name, "cert6") == 0){
            strcpy(buf,"Equifax_Secure_Certificate_Authority.cer");
            return buf;
        } else if(strcmp(name, "cert7") == 0){
            strcpy(buf,"PrimaryGeotrustCrossRootCA.cer");
            return buf;
        } else if(strcmp(name, "cert8") == 0){
            strcpy(buf,"secondaryRapidSSLCA.cer");
            return buf;
        } else if(strcmp(name, "cert9") == 0){
            strcpy(buf,"intenocloud_fi.cer");
            return buf;
        }
    }
    return (char*)"";
}

/*
 * Get a configuration parameter
 *
 * @return the value on success, -1 when the config parameter was not found
 */
long configGetLong(const char * section, const char *name, long defVal) {
    syslog(LOG_DEBUG, "configGetLong");
    if(strcmp(section,"Core") ==0){
        if(strcmp(name, "createLocalSOCKS5server") == 0){
            defVal=1;
            return defVal;
        } else if(strcmp(name, "s5port") == 0){
            defVal=16667;
            return defVal;
        } else if(strcmp(name, "http_transfer_port") == 0){
            defVal=40124;
            return defVal;
        } else if(strcmp(name, "http_transfer_threads") == 0){
            defVal=5;
            return defVal;
        } else if(strcmp(name, "sleep_on_disconnect") == 0){
            defVal=5;
            return defVal;
        } 
    }
    return -1;
}

/* 
 * Set a configuration parameter to the given value
 *
 * @return 1 on success, 0 when failed
 */
int configSet(const char *section, const char *name, const char *value) {
	return 1;
}
