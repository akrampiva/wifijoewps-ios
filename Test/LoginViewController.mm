//
//  @name LoginViewController.mm
//
//  @desc The login viewcontroller handles the ios application login screen functions
//  @author Kurt Vermeersch.
//
//  Copyright 2013 Cloud Friends. All rights reserved.
//

#import "LoginViewController.h"
#import "SelectionScreenViewController.h"
#import "CloudFriendsWrapper.h"
#import "Reachability.h"
#import "conf_store.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#import <QuartzCore/QuartzCore.h>
#import "deviceutils.h"
#import "ApiManager.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "cloudfriendsAppDelegate.h"
#import "SettingsSmallViewController.h"
#import "colors.h"

@implementation LoginViewController

// Text Fields
@synthesize usernameField;
@synthesize passwordField;
// Labels
@synthesize usernameLabel;
@synthesize passwordLabel;
// Buttons
@synthesize loginButton;
@synthesize forgotButton;
@synthesize noAccountButton;
@synthesize loginIndicator;
@synthesize selectionScreenViewController;
@synthesize settingsViewController;
@synthesize signupViewController;
@synthesize cf;
@synthesize alert;
@synthesize reconnectionalert;
@synthesize connectionStatusLabel;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    autologin=TRUE;
    logoutclicked=FALSE;
    loginclicked=FALSE;
    usernameField.delegate=self;
    passwordField.delegate=self;
    [self LocalizeStrings];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}


-(void)LocalizeStrings
{
    usernameLabel.text = NSLocalizedString(@"Username", nil) ;
    passwordLabel.text = NSLocalizedString(@"Password", nil) ;
    [loginButton setTitle:NSLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [forgotButton setTitle:NSLocalizedString(@"Forgot", nil) forState:UIControlStateNormal];
    [noAccountButton setTitle:NSLocalizedString(@"NoAccount", nil) forState:UIControlStateNormal];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setHidden:YES];
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];
    usernameField.delegate=self;
    passwordField.delegate=self;
    NSString* un=[[NSUserDefaults standardUserDefaults] stringForKey:@"name_preference"];
    if(un!=nil){
        usernameField.text=un;
    }
    NSString* pw=[[NSUserDefaults standardUserDefaults] stringForKey:@"password_preference"];
    if(pw!=nil){
        passwordField.text=pw;
    }
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    appdelegate.settingIsShowing = NO;
    appdelegate.loginIsShowing = YES;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    usernameField.layer.borderColor=themecolor.CGColor;
    [usernameField.layer setCornerRadius:10.0f];
    usernameField.borderStyle = UITextBorderStyleRoundedRect;
    usernameField.layer.borderWidth=1.0;
    passwordField.layer.borderColor=themecolor.CGColor;
    [passwordField.layer setCornerRadius:10.0f];
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    passwordField.layer.borderWidth=1.0;
    CALayer *btnLayer = [loginButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    [loginButton setUserInteractionEnabled:YES];
    loginButton.enabled = TRUE;
    [loginIndicator stopAnimating];
    loginIndicator.hidden = TRUE;
    forgotButton.enabled = TRUE;
    [super viewDidAppear:animated];
    if(usernameField.text.length>0 && passwordField.text.length>0 && autologin==TRUE){
        [loginButton sendActionsForControlEvents: UIControlEventTouchDown];
        loginIndicator.hidden = FALSE;
        [loginIndicator startAnimating];
        loginButton.enabled = FALSE;
    }
    //connectionStatusLabel.text = @"";
}

- (void)viewWillDisappear:(BOOL)animated
{
    //connectionStatusLabel.text = @"";
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    appdelegate.loginIsShowing = NO;
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (IBAction) noaccount: (id) sender
{
    NSString* createAccountStr = NSLocalizedString(@"NoAccountCreate", nil);
    char buffer[100];
    char * serverStr=configGet("Core","cloudfriends_host",buffer,sizeof(buffer),true);
    NSString* se = [[NSString alloc] initWithCString:serverStr encoding:NSUTF8StringEncoding];
    createAccountStr = [createAccountStr stringByAppendingString:se];
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NoAccountYet", nil) message:createAccountStr delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
    [av show];
}

- (IBAction) settings: (id) sender
{
    
    if(IS_IPHONE_5)
    {
        settingsViewController =[[SettingsSmallViewController alloc] initWithNibName:@"SettingsSmallViewController" bundle:nil];
    } else if(IS_IPHONE){
        settingsViewController =[[SettingsSmallViewController alloc] initWithNibName:@"SettingsSmallViewController" bundle:nil];
    }
    else{
        settingsViewController =[[SettingsSmallViewController alloc] initWithNibName:@"SettingsSmallViewController-iPad" bundle:nil];
    }
    [[self navigationController] pushViewController:settingsViewController animated:YES];
}

- (IBAction) forgot: (id) sender
{
}

-(void)checkNetworkConnection : (id) sender{
    /*NSLog(@"checkNetworkConnection current = %s saved = %s",cf->cf->fetchCurrentNetworkKey().c_str(),cf->cf->getSavedNetworkKey().c_str());
    if((cf->cf->isTryingToReconnect())){
        NSLog(@"trying to reconnect");
    }*/
    /*if (cf->cf->isConnectedToServer()){
        NSLog(@"checkNetworkConnection: is connected to server");

    }
    if(cf->cf->isConnectedToServer()){
        std::string currentNWKey=cf->cf->fetchCurrentNetworkKey();
        if(currentNWKey.compare(cf->cf->getSavedNetworkKey())!=0){
            NSLog(@"NETWORK CONNECTION HANDOVER checkNetworkConnection");
            SEL isdisconnselector = NSSelectorFromString(@"isdisconnected:");
            [self performSelectorOnMainThread:isdisconnselector withObject:[[NSNumber alloc] initWithInt:78] waitUntilDone:YES];
        }
    }*/
}


-(void)checkInternetConnection : (id) sender{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    UIViewController *lastViewController = [[self.navigationController viewControllers] lastObject];
    /*if(cf->cf->isConnectedToServer()){
        std::string currentNWKey=cf->cf->fetchCurrentNetworkKey();
        if(currentNWKey.compare(cf->cf->getSavedNetworkKey())!=0){
            NSLog(@"NETWORK CONNECTION HANDOVER checkNetworkConnection");
            SEL isdisconnselector = NSSelectorFromString(@"isdisconnected:");
            [self performSelectorOnMainThread:isdisconnselector withObject:[[NSNumber alloc] initWithInt:78] waitUntilDone:YES];
        }
    }*/
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
        if([lastViewController isKindOfClass:[OptimizeViewController class]]) {
            NSLog(@"OptimizeViewController currently active, do not show no internet popup");
            [(id <EventingProtocol>)lastViewController noInternetAvailable];
            return;
        }
        else {
            if(isShowingAlert==FALSE){
                NSLog(@"SHOW INTERNET POPUP");
                alert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NoInternetConnection", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
                UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
                if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
                    [alert setValue:spinner forKey:@"accessoryView"]; //works only in iOS7
                } else {
                    alert.message = @"\n"; //or just add \n to the end of the message (it's a workaround to create space inside the alert view
                    [alert addSubview:spinner];
                }
                [spinner startAnimating];
                 cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
                if(!appdelegate.settingIsShowing)
                    [alert show];
                isShowingAlert=TRUE;
                if(cf!=NULL && cf->cf!=NULL){
                    cf->cf->cf->disconnect();
                }
            }
        }
    }
    else if(isShowingAlert==TRUE){
        NSLog(@"REMOVE INTERNET POPUP");
        [alert dismissWithClickedButtonIndex:0 animated:YES];
        isShowingAlert=FALSE;
        cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
        if(!appdelegate.settingIsShowing)
            [self.navigationController popToRootViewControllerAnimated:YES];
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        loginButton.enabled = TRUE;
    } else {
        if([lastViewController isKindOfClass:[OptimizeViewController class]]) {
            [(id <EventingProtocol>)lastViewController internetAvailable];
            if(cf->cf->isConnectedToServer()){
                if(cf->cf->isGatewayLocal()){
                    NSLog(@"OptimizeViewController currently active, is local connected");
                    [(id <EventingProtocol>)lastViewController isLocalConnected];
                } else {
                    NSLog(@"OptimizeViewController currently active, is NOT local connected");
                    [(id <EventingProtocol>)lastViewController notLocalConnected];
                }
            }
            
        } else {
            std::string currentNWKey=cf->cf->fetchCurrentNetworkKey();
            if(!loginclicked && !logoutclicked && !cf->cf->isConnectedToServer()){
                if(reconnectionalert==NULL){
                    NSLog(@"SHOW RECONNECTING");
                    SEL isdisconnselector = NSSelectorFromString(@"isdisconnected:");
                    [self performSelectorOnMainThread:isdisconnselector withObject:[[NSNumber alloc] initWithInt:78] waitUntilDone:YES];
                }
            } else if(!loginclicked && !logoutclicked && currentNWKey.compare(cf->cf->getSavedNetworkKey())!=0){
                cf->cf->setSavedNetworkKey(currentNWKey);
                NSLog(@"SHOW RECONNECTING [new network]");
                SEL isdisconnselector = NSSelectorFromString(@"isdisconnected:");
                [self performSelectorOnMainThread:isdisconnselector withObject:[[NSNumber alloc] initWithInt:78] waitUntilDone:YES];
            } else if (!loginclicked && !logoutclicked && cf->cf->isConnectedToServer()){
                if(reconnectionalert!=NULL){
                    NSLog(@"HIDE RECONNECTING");
                    [reconnectionalert dismissWithClickedButtonIndex:0 animated:YES];
                    reconnectionalert=NULL;
                }
            }
        }
    }
}

- (IBAction) login: (id) sender
{
    NSLog(@"login");
    logoutclicked=FALSE;
    if([[usernameField text] length]==0 || [[passwordField text] length]==0){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"CredentialsAlertTitle", nil) message:NSLocalizedString(@"CredentialsAlertDesc", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] autorelease];
        cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
        if(!appdelegate.settingIsShowing)
            [av show];
    } else {
        loginclicked=TRUE;
        loginIndicator.hidden = FALSE;
        [loginIndicator startAnimating];
        loginButton.enabled = FALSE;
        if(cf!=NULL){
            cf->cf->cf->destroy();
            cf=NULL;
        }
        cf=[[CloudFriendsWrapper alloc] init];
        isShowingAlert=FALSE;
        [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(checkInternetConnection:) userInfo:nil repeats:YES];
        [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(checkNetworkConnection:) userInfo:nil repeats:YES];
        if(connectionObserver==NULL){
            connectionObserver=new ConnectionObserver();
            connectionObserver->delegate=self;
            cf->cf->cf->registerConnectionHandler(connectionObserver);
        }
        NSMutableDictionary *arguments =[[NSMutableDictionary alloc] initWithObjectsAndKeys:[usernameField text],@"name",[passwordField text],@"pass",cf,@"client",[NSNumber numberWithInt:0], @"reconnectcount",NULL];
        [NSThread detachNewThreadSelector:@selector(performLogin:) toTarget:self withObject:arguments];
    }
}

- (void)performLogin:(NSMutableDictionary *)arguments  {
    dict=arguments;
    NSLog(@"performLogin");
    CloudFriendsWrapper *clientWrapper=(CloudFriendsWrapper*)[arguments objectForKey:@"client"];
    char buffer[100];
    char * serverStr=configGet("Core","cloudfriends_host",buffer,sizeof(buffer),true);
    NSString* se = [[NSString alloc] initWithCString:serverStr encoding:NSUTF8StringEncoding];
    NSLog(@"server = %@",se);
    if(presenceObserver==NULL){
        presenceObserver=new PresenceObserver();
        presenceObserver->delegate=self;
        cf->cf->cf->registerPresenceHandler(presenceObserver);
    }
    clientWrapper->cf->cf->connect([((NSString*)[arguments objectForKey:@"name"]) cStringUsingEncoding:[NSString defaultCStringEncoding]],[(((NSString*)[arguments objectForKey:@"pass"])) cStringUsingEncoding:[NSString defaultCStringEncoding]],[se cStringUsingEncoding:[NSString defaultCStringEncoding]]);
    NSLog(@"AFTER CONNECT");
}

- (IBAction) reconnectfailure: (id) sender {
    NSLog(@"reconnectfailure");
    [reconnectionalert dismissWithClickedButtonIndex:0 animated:YES];
    reconnectionalert=NULL;
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ReconnectFailure", nil) message:NSLocalizedString(@"ReconnectFailureDesc", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] autorelease];
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    if(!appdelegate.settingIsShowing)
        [av show];
}


- (void)isconnected:(NSNumber*) err{
    NSLog(@"isconnected");
    //Save network ssid
    std::string bssid = cf->cf->fetchCurrentNetworkKey();
    cf->cf->setSavedNetworkKey(bssid);
    if(loginclicked==TRUE){
        loginclicked=FALSE;
        if(IS_IPHONE_5)
        {
            selectionScreenViewController =[[SelectionScreenViewController alloc] initWithNibName:@"SelectionScreenViewController" bundle:nil];
        } else if(IS_IPHONE){
            selectionScreenViewController =[[SelectionScreenViewController alloc] initWithNibName:@"SelectionScreenViewController" bundle:nil];
        }
        else{
            selectionScreenViewController =[[SelectionScreenViewController alloc] initWithNibName:@"SelectionScreenViewController-iPad" bundle:nil];
        }
        selectionScreenViewController->cf = self.cf;
        selectionScreenViewController->loginViewController = self;
        cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
        if(!appdelegate.settingIsShowing)
            [[self navigationController] pushViewController:selectionScreenViewController animated:YES];
    } else {
        NSLog(@"isconnected, background");
        //dispatch_sync(dispatch_get_main_queue(), ^{
            [reconnectionalert dismissWithClickedButtonIndex:0 animated:YES];
            reconnectionalert=NULL;
        //});
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"clickedButtonAtIndex");
    if (alertView == reconnectionalert) {
        [self performSelectorOnMainThread:@selector(logout:) withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
        cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
        if(!appdelegate.settingIsShowing)
            [[self navigationController] popToRootViewControllerAnimated:YES];
    } else {
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        loginButton.enabled = TRUE;
    }
}

- (void)isPresent:(NSString*) strFrom{
    self.cf->cf->isPresent([strFrom cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void)isNotPresent:(NSString*) strFrom{
    self.cf->cf->isNotPresent([strFrom cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void)logout:(NSNumber*) err{
    NSLog(@"logout");
    loginIndicator.hidden = TRUE;
    autologin=FALSE;
    logoutclicked=TRUE;
    if(cf!=NULL && cf->cf!=NULL){
        cf->cf->cf->disconnect();
    }
}

- (void)isdisconnectedAuth:(NSNumber*) err{
    NSLog(@"isdisconnectedAuth");
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"AuthFailure", nil) message:NSLocalizedString(@"AuthFailureDesc", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] autorelease];
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    if(!appdelegate.settingIsShowing)
        [av show];
    [loginIndicator stopAnimating];
    loginIndicator.hidden = TRUE;
    loginButton.enabled = TRUE;
    if(!appdelegate.settingIsShowing)
        [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)isdisconnected:(NSNumber*) err{
    NSLog(@"isdisconnected");
    if(logoutclicked==TRUE){
        [loginIndicator stopAnimating];
        loginIndicator.hidden = TRUE;
        loginButton.enabled = TRUE;
        cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
        if(!appdelegate.settingIsShowing){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        logoutclicked=FALSE;
    }
    else if ([err intValue] == 78 || [err intValue] == 7){
        Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
        NetworkStatus internetStatus = [r currentReachabilityStatus];
        if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
            
        } else {
            if(reconnectionalert==NULL){
                reconnectionalert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ReConnecting", nil) message:nil delegate:self cancelButtonTitle:@"Logout" otherButtonTitles: nil];
                UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
                if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
                    [reconnectionalert setValue:spinner forKey:@"accessoryView"]; //works only in iOS7
                } else {
                    reconnectionalert.message = @"\n"; //or just add \n to the end of the message (it's a workaround to create space inside the alert view
                    [reconnectionalert addSubview:spinner];
                }
                [spinner startAnimating];
                cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
                if(!(appdelegate.settingIsShowing || appdelegate.loginIsShowing))
                    [reconnectionalert show];
            }
            
            if(reconnectionalert!=NULL){
                if(dict!=NULL){
                NSInteger c = [[dict objectForKey:@"reconnectcount"] intValue]+1;
                if(c>=3){
                    [dict setValue:[NSNumber numberWithInt:0] forKey:@"reconnectcount"];
                    [self performSelectorOnMainThread:@selector(reconnectfailure:) withObject:self waitUntilDone:YES];
                    [loginIndicator stopAnimating];
                    loginIndicator.hidden = TRUE;
                    loginButton.enabled = TRUE;
                    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
                    if(!appdelegate.settingIsShowing)
                        [self.navigationController popToRootViewControllerAnimated:YES];
                } else {
                    [dict setValue:[NSNumber numberWithInt:c] forKey:@"reconnectcount"];
                    [NSThread detachNewThreadSelector:@selector(performLogin:) toTarget:self withObject:dict];
                }
                } else {
                    NSLog(@"dict is null");
                }
            }

        }
    }
    
}

-(IBAction)textFieldDoneEditing:(id)sender{
    [sender resignFirstResponder];
}

@end







