//
//  WifiJoeClient.cpp
//  Test
//
//  Created by Kurt Vermeersch on 25/02/13.
//
//

#include "WifiJoeClient.h"
#include <iostream>
#include <list>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <algorithm>
#include <string>
#include "WifiJoeEventHandler.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "LanDevice.h"
WifiJoeClient::WifiJoeClient(){
    cf=new CloudFriends();
    gatewayJID=NULL;
    networkKey=NULL;
    tryingToReconnect=FALSE;
}

WifiJoeClient::~WifiJoeClient(){
    delete cf;
    cf=NULL;
    gatewayJID=NULL;
    tryingToReconnect=FALSE;
    networkKey=NULL;
}

std::string WifiJoeClient::getGatewayJID(){
    if(gatewayJID!=NULL){
        return *gatewayJID;
    }
    return "";
}

std::string WifiJoeClient::getSavedNetworkKey(){
    if(networkKey!=NULL){
        return *networkKey;
    }
    return "";
}

void WifiJoeClient::setSavedNetworkKey(std::string networkkey){
    networkKey=new std::string(networkkey);
}

std::string WifiJoeClient::fetchCurrentNetworkKey(){
    std::string returnvalue = "";
    NSArray *ifs = ( id)CNCopySupportedInterfaces();
    //NSLog(@"Supported interfaces: %@", ifs);
    id info = nil;
    NSString *ifnam = @"";
    for (ifnam in ifs) {
        info = (id)CNCopyCurrentNetworkInfo((CFStringRef)ifnam);
        if (info[@"BSSID"]) {
            //NSLog(@"BSSID = %@", info[@"BSSID"]);
            returnvalue=[info[@"BSSID"] UTF8String];
        }
        if (info && [info count]) { break; }
    }
    return returnvalue;
}


bool WifiJoeClient::isConnectedToServer(){
    return cf->isConnectedToServer();
}

/*void WifiJoeClient::setTryingToReconnect(bool onoff){
 tryingToReconnect=onoff;
 }
 
 bool WifiJoeClient::isTryingToReconnect(){
 //TODO CHANGE THIS
 return tryingToReconnect;
 }*/


bool WifiJoeClient::deviceIsConnected(){
    if(gatewayJID==NULL){
        return false;
    }
    std::string to = this->getGatewayJID();
    return cf->isPresent(to);
}

void WifiJoeClient::isPresent(const char *from){
    std::string toStr(from);
    IdentifyDeviceParameters identifyDevParams;
    IdentifyDeviceParameters returnParams = this->identifyDevice(toStr, identifyDevParams);
    if (returnParams.hasType()){
        std::string type=returnParams.getType();
        if(type.compare("gateway")==0){
            gatewayJID=new std::string(toStr);
        }
    }
}

void WifiJoeClient::isNotPresent(const char *from){
    std::string fromStr(from);
    if(gatewayJID!=NULL && (*gatewayJID).compare(fromStr)==0){
        delete gatewayJID;
        gatewayJID=NULL;
    }
}

IdentifyDeviceParameters WifiJoeClient::identifyDevice(std::string to, IdentifyDeviceParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("device", "device information");
    //BUILD REQUEST
    //SEND REQUEST
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("identify", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* typeEntry=reply->findReplyParam("type");
    if(typeEntry!=NULL){
        param.setType(typeEntry->value);
    }
    ParamEntry* uidEntry=reply->findReplyParam("uniqueDeviceId");
    if(uidEntry!=NULL){
        param.setUniqueDeviceId(uidEntry->value);
    }
    return param;
}

InterfacesParameters WifiJoeClient::interfaces(InterfacesParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("interfaces", "interface information, lists match");
    //BUILD REQUEST
    if(param.hasDevice()){
        cEntry->setInput("device",param.getDevice());
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("netMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* typeEntry=reply->findReplyParam("type");
    if(typeEntry!=NULL){
        std::list<std::string> values=typeEntry->valueList;
        param.setTypes(values);
    }
    ParamEntry* ipAddrEntry=reply->findReplyParam("ipAddr");
    if(ipAddrEntry!=NULL){
        std::list<std::string> values=ipAddrEntry->valueList;
        param.setIPAddresses(values);
    }
    ParamEntry* deviceEntry=reply->findReplyParam("device");
    if(deviceEntry!=NULL){
        std::list<std::string> values=deviceEntry->valueList;
        param.setDeviceNames(values);
    }
    ParamEntry* macEntry=reply->findReplyParam("mac");
    if(macEntry!=NULL){
        std::list<std::string> values=macEntry->valueList;
        param.setMacAddresses(values);
    }
    ParamEntry* maskEntry=reply->findReplyParam("mask");
    if(maskEntry!=NULL){
        std::list<std::string> values=maskEntry->valueList;
        param.setMasks(values);
    }
    ParamEntry* stateEntry=reply->findReplyParam("state");
    if(stateEntry!=NULL){
        std::list<std::string> values=stateEntry->valueList;
        param.setStates(values);
    }
    ParamEntry* rxBytesEntry=reply->findReplyParam("rxBytes");
    if(rxBytesEntry!=NULL){
        std::list<std::string> values=rxBytesEntry->valueList;
        param.setRXBytes(values);
    }
    ParamEntry* txBytesEntry=reply->findReplyParam("txBytes");
    if(txBytesEntry!=NULL){
        std::list<std::string> values=txBytesEntry->valueList;
        param.setTXBytes(values);
    }
    return param;
}

ARPPingParameters WifiJoeClient::arpping(ARPPingParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("arpPing", "arp ping a specific IP addr");
    //BUILD REQUEST
    if(param.hasIPAddress()){
        cEntry->setInput("ipAddr",param.getIPAddress());
    }
    if(param.hasTimeout()){
        cEntry->setInput("timeOut",param.getTimeout());
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("netMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setResult(value);
    }
    ParamEntry* errorEntry=reply->findReplyParam("error");
    if(errorEntry!=NULL){
        std::string value=errorEntry->value;
        param.setError(value);
    }
    return param;
}

LeasesParameters WifiJoeClient::leases(LeasesParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("leases", "list of provided DHCP leases, lists match");
    //BUILD REQUEST
    if(param.hasIPAddress()){
        cEntry->setInput("ipAddr",param.getIPAddress());
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("netMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* errorEntry=reply->findReplyParam("error");
    if(errorEntry!=NULL){
        std::string value=errorEntry->value;
        param.setError(value);
    }
    ParamEntry* ipEntry=reply->findReplyParam("ipAddr");
    if(ipEntry!=NULL){
        std::list<std::string> values=ipEntry->valueList;
        param.setIPAddresses(values);
    }
    ParamEntry* macAddressEntry=reply->findReplyParam("mac");
    if(macAddressEntry!=NULL){
        std::list<std::string> values=macAddressEntry->valueList;
        param.setMacAddresses(values);
    }
    ParamEntry* nameEntry=reply->findReplyParam("name");
    if(nameEntry!=NULL){
        std::list<std::string> values=nameEntry->valueList;
        param.setNames(values);
    }
    return param;
}

WifiParameters WifiJoeClient::wifi(WifiParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("wifi", "gather wifi information");
    cEntry->clearInputValues();
    cEntry->clearReplyValues();
    //BUILD REQUEST
    if(param.hasInterface()){
        cEntry->setInput("interface",param.getInterface());
    }
    if(param.hasSSID()){
        cEntry->setInput("ssid",param.getSSID());
    }
    if(param.hasChannel()){
        cEntry->setInput("channel",param.getChannel());
    }
    if(param.hasState()){
        cEntry->setInput("state",param.getState());
    }
    if(param.hasPassword()){
        cEntry->setInput("password",param.getPassword());
    }
    if(param.hasSecurity()){
        cEntry->setInput("security",param.getSecurity());
    }
    if(param.hasNoise()){
        cEntry->setInput("noise","");
    }
    if(param.hasSignalStrength()){
        cEntry->setInput("signalStrength","");
    }
    if(param.hasAssocTime()){
        cEntry->setInput("assocTime","");
    }
    if(param.hasMacAddress()){
        cEntry->setInput("macAddress","");
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("netMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setResult(value);
    }
    ParamEntry* ssidEntry=reply->findReplyParam("ssid");
    if(ssidEntry!=NULL){
        std::string value=ssidEntry->value;
        param.setSSID(value);
    }
    ParamEntry* channelEntry=reply->findReplyParam("channel");
    if(channelEntry!=NULL){
        std::string value=channelEntry->value;
        param.setChannel(value);
    }
    ParamEntry* stateEntry=reply->findReplyParam("state");
    if(stateEntry!=NULL){
        std::string value=stateEntry->value;
        param.setState(value);
    }
    ParamEntry* passwordEntry=reply->findReplyParam("password");
    if(passwordEntry!=NULL){
        std::string value=passwordEntry->value;
        param.setPassword(value);
    }
    ParamEntry* securityEntry=reply->findReplyParam("security");
    if(securityEntry!=NULL){
        std::string value=securityEntry->value;
        param.setSecurity(value);
    }
    ParamEntry* noiseEntry=reply->findReplyParam("noise");
    if(noiseEntry!=NULL){
        std::string value=noiseEntry->value;
        param.setNoise(value);
    }
    ParamEntry* signalStrengthEntry=reply->findReplyParam("signalStrength");
    if(signalStrengthEntry!=NULL){
        std::list<std::string> values=signalStrengthEntry->valueList;
        param.setSignalStrengths(values);
    }
    ParamEntry* assocTimeEntry=reply->findReplyParam("assocTime");
    if(assocTimeEntry!=NULL){
        std::list<std::string> values=assocTimeEntry->valueList;
        param.setAssocTimes(values);
    }
    ParamEntry* macAddressEntry=reply->findReplyParam("macAddress");
    if(macAddressEntry!=NULL){
        std::list<std::string> values=macAddressEntry->valueList;
        param.setMacAddresses(values);
    }
    ParamEntry* interfacesEntry=reply->findReplyParam("interfaces");
    if(interfacesEntry!=NULL){
        std::list<std::string> values=interfacesEntry->valueList;
        param.setInterfaces(values);
    }
    return param;
}

ARPTableParameters WifiJoeClient::arptable(ARPTableParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("arpTable", "retrieve the arp table");
    cEntry->clearInputValues();
    cEntry->clearReplyValues();
    //BUILD REQUEST
    if(param.hasDevice()){
        cEntry->setInput("device",param.getDevice());
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("netMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("error");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setError(value);
    }
    ParamEntry* macEntry=reply->findReplyParam("mac");
    if(macEntry!=NULL){
        std::list<std::string> values=macEntry->valueList;
        param.setMacAddresses(values);
    }
    ParamEntry* ipEntry=reply->findReplyParam("ipAddr");
    if(ipEntry!=NULL){
        std::list<std::string> values=ipEntry->valueList;
        param.setIPAddresses(values);
    }
    ParamEntry* deviceEntry=reply->findReplyParam("device");
    if(deviceEntry!=NULL){
        std::list<std::string> values=deviceEntry->valueList;
        param.setDeviceNames(values);
    }
    return param;
}

GuestCreateParameters WifiJoeClient::guestcreate(GuestCreateParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("create", "Create a new guest interface");
    cEntry->clearInputValues();
    cEntry->clearReplyValues();
    //BUILD REQUEST
    if(param.hasSSID()){
        cEntry->setInput("ssid",param.getSSID());
    }
    if(param.hasInputDelay()){
        cEntry->setInput("delay",param.getInputDelay());
    }
    if(param.hasState()){
        if(param.getState()==UP){
            cEntry->setInput("state","true");
        } else {
            cEntry->setInput("state","false");
        }
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("guestMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setResult(value);
    }
    ParamEntry* delayEntry=reply->findReplyParam("delay");
    if(delayEntry!=NULL){
        std::string value=delayEntry->value;
        param.setOutputDelay(value);
    }
    ParamEntry* ssidEntry=reply->findReplyParam("ssid");
    if(ssidEntry!=NULL){
        std::string value=ssidEntry->value;
        param.setSSID(value);
    }
    ParamEntry* stateEntry=reply->findReplyParam("state");
    if(stateEntry!=NULL){
        std::string value=stateEntry->value;
        bool state;
        std::istringstream(value) >> std::boolalpha >> state;
        if(state){
            param.setState(UP);
        } else {
            param.setState(DOWN);
        }
    }
    return param;
}

GuestRemoveParameters WifiJoeClient::guestremove(GuestRemoveParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("remove", "Remove the guest interface");
    cEntry->clearInputValues();
    cEntry->clearReplyValues();
    //BUILD REQUEST
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("guestMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setResult(value);
    }
    return param;
}

GuestStatusParameters WifiJoeClient::gueststatus(GuestStatusParameters param,CommandEventHandler* gueststatusevents){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("status", "Retrieve information of the wifi guest interface Set parameter but leave empty to get current value");
    cEntry->setPublicObservable();
    //BUILD REQUEST
    if(param.hasConnected()){
        cEntry->setInput("connected",param.getConnected());
    }
    if(param.hasAccess()){
        cEntry->setInput("access",param.getAccess());
    }
    if(param.hasTime()){
        cEntry->setInput("time",param.getTime());
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("guestMgr", cEntry, to, ceh, gueststatusevents);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setResult(value);
    }
    ParamEntry* interfaceEntry=reply->findReplyParam("interface");
    if(interfaceEntry!=NULL){
        std::string value=interfaceEntry->value;
        param.setInterface(value);
    }
    ParamEntry* connectedEntry=reply->findReplyParam("connected");
    if(connectedEntry!=NULL){
        std::list<std::string> values=connectedEntry->valueList;
        param.setConnectedDevices(values);
    }
    ParamEntry* accessEntry=reply->findReplyParam("access");
    if(accessEntry!=NULL){
        std::list<std::string> values=accessEntry->valueList;
        param.setAccesses(values);
    }
    ParamEntry* timeEntry=reply->findReplyParam("time");
    if(timeEntry!=NULL){
        std::list<std::string> values=timeEntry->valueList;
        param.setTimes(values);
    }
    ParamEntry* stateEntry=reply->findReplyParam("state");
    if(stateEntry!=NULL){
        std::string value=stateEntry->value;
        bool state;
        std::istringstream(value) >> std::boolalpha >> state;
        if(state){
            param.setState(UP);
        } else {
            param.setState(DOWN);
        }
    }
    return param;
}

GuestAccessParameters WifiJoeClient::guestaccess(GuestAccessParameters param){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("access", "Set the access rights of an IP");
    cEntry->clearInputValues();
    cEntry->clearReplyValues();
    //BUILD REQUEST
    if(param.hasIPAddress()){
        cEntry->setInput("ipAddr",param.getIPAddress());
    }
    if(param.hasType()){
        cEntry->setInput("type",param.getType());
    }
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("guestMgr", cEntry, to, ceh);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    CommandEntry* reply=ceh->getReply();
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        param.setResult(value);
    }
    return param;
}

std::string WifiJoeClient::getWifiInterface(){
    WifiParameters wifiParams;
    WifiParameters returnWifiParams=this->wifi(wifiParams);
    std::string wirelessinterf="wl0";
    if(returnWifiParams.getInterfaces().size()>0){
        wirelessinterf=returnWifiParams.getInterfaces().front();
    }
    return wirelessinterf;
}

WlSettings WifiJoeClient::setWireless(WlSettings settings){
    WifiParameters wifiParams;
    wifiParams.setInterface(this->getWifiInterface());
    if (settings.ssid.size()){
        wifiParams.setSSID(settings.ssid);
    } else {
        wifiParams.setSSID("");
    }
    if (settings.channel > 0){
        char channel[8];
		sprintf(channel, "%d", settings.channel);
        wifiParams.setChannel(channel);
    } else {
        wifiParams.setChannel("");
    }
    if(settings.key.size()){
        wifiParams.setPassword(settings.key);
    } else {
        wifiParams.setPassword("");
    }
    wifiParams.setSecurity("");
    WifiParameters returnParams=this->wifi(wifiParams);
    WlSettings ret (returnParams.getSSID(), atoi(returnParams.getChannel().c_str()));
    ret.security = returnParams.getSecurity();
    ret.key = returnParams.getPassword();
    return ret;
}

WlSettings WifiJoeClient::getWireless(){
	return setWireless(WlSettings("", 0));
}

GuestIface WifiJoeClient::setGuestnet(GuestIface netsettings){
    GuestIface ret(netsettings.ssid, ERROR, netsettings.timeout);
    if(netsettings.status == UP){
        GuestCreateParameters createParams;
        createParams.setSSID(netsettings.ssid);
        std::stringstream ss;
        ss << netsettings.timeout;
        createParams.setInputDelay(ss.str());
        createParams.setState(UP);
        GuestCreateParameters returnParams=this->guestcreate(createParams);
        ret.status = UP;
        ret.ssid = returnParams.getSSID();
        ret.timeout = atoi(returnParams.getOutputDelay().c_str());
    } else if (netsettings.status == DOWN){
        GuestRemoveParameters removeParams;
        GuestRemoveParameters returnParams=this->guestremove(removeParams);
        ret.status = DOWN;
    } else if ( netsettings.status == NOCHANGE){
        GuestStatusParameters statusSetParams;
        GuestStatusParameters statusParams = this->gueststatus(statusSetParams, NULL);
        Result currentState = statusParams.getState();
        GuestCreateParameters createParams;
        if(netsettings.ssid.compare("")==0){
            //createParams.setSSID("intenoguest");
        } else {
            createParams.setSSID(netsettings.ssid);
        }
        if(netsettings.timeout>0){
            std::stringstream ss;
            ss << netsettings.timeout;
            createParams.setInputDelay(ss.str());
        } else {
            createParams.setInputDelay("");
        }
        createParams.setState(currentState);
        GuestCreateParameters returnParams=this->guestcreate(createParams);
        ret.status = currentState;
        ret.ssid = returnParams.getSSID();
        ret.timeout = atoi(returnParams.getOutputDelay().c_str());
    }
    return ret;
}

GuestIface WifiJoeClient::getGuestnet(){
	return setGuestnet(GuestIface("", NOCHANGE, NOCHANGE));
}

void WifiJoeClient::setGuestRule(std::string ip, Result policy){
    GuestAccessParameters accessParams;
    accessParams.setIPAddress(ip);
    if(policy == LAN){
        accessParams.setType("lan");
    } else if(policy == WAN){
        accessParams.setType("wan");
    } else if(policy == FULL){
        accessParams.setType("lanwan");
    }
    this->guestaccess(accessParams);
}

std::vector<DhcpLease> WifiJoeClient::getLeases(CommandEventHandler* guestEventHandler){
    std::vector<DhcpLease> ret;
    //WIRELESS
    WifiParameters wifiParams;
    wifiParams.setInterface(this->getWifiInterface());
    wifiParams.setMacAddress("");
    wifiParams.setNoise("");
    wifiParams.setSignalStrength("");
    WifiParameters returnWifiParams=this->wifi(wifiParams);
    std::list<std::string> wirelessMacs= returnWifiParams.getMacAddresses();
    std::list<std::string> wirelessStrengths= returnWifiParams.getSignalStrengths();
    std::string wirelessNoise= returnWifiParams.getNoise();
    //GUEST
    GuestStatusParameters guestStatusParams;
    guestStatusParams.setConnected("true");
    guestStatusParams.setAccess("true");
    GuestStatusParameters returnGuestStatusParams=this->gueststatus(guestStatusParams, guestEventHandler);
    std::list<std::string> guestNames=returnGuestStatusParams.getConnectedDevices();
    std::list<std::string> accessLevels=returnGuestStatusParams.getAccesses();
    //LEASES
    LeasesParameters leasesParams;
    LeasesParameters returnParams=this->leases(leasesParams);
    std::list<std::string> devs=returnParams.getNames();
    std::list<std::string> ips=returnParams.getIPAddresses();
    std::list<std::string> macs=returnParams.getMacAddresses();
    while(!ips.empty()){
        std::string ip=ips.front();
        std::string mac=macs.front();
        std::string dev=devs.front();
        std::transform(mac.begin(), mac.end(),mac.begin(), ::toupper);
        const bool isguest = (std::find(guestNames.begin(),guestNames.end(),mac)!=guestNames.end());
        std::list<std::string>::iterator it=std::find(wirelessMacs.begin(),wirelessMacs.end(),mac);
        const bool iswireless = (it!=wirelessMacs.end());
        SignalStrength strength = SignalStrength::PERFECT;
        if(iswireless || isguest){
            int index = std::distance(wirelessMacs.begin(), it);
            std::list<std::string>::iterator itS = wirelessStrengths.begin();
            std::advance(itS, index);
            std::string wirelessStrength = *itS;
            std::string::size_type sz;
            double wStrength = std::stod (wirelessStrength,&sz);
            double wNoise = std::stod (wirelessNoise,&sz);
            double SNR = wStrength - wNoise;
            if(SNR<15){
                strength = SignalStrength::BAD;
            } else if (SNR<25){
                strength = SignalStrength::SLOW;
            } else if (SNR<40){
                strength = SignalStrength::GOOD;
            }
        }
        if(isguest){
            std::list<std::string>::iterator it = std::find(guestNames.begin(),guestNames.end(),mac);
            int position=std::distance(guestNames.begin(),it);
            std::list<std::string>::iterator itnew = accessLevels.begin();
            std::advance(itnew, position);
            std::string access=*itnew;
            Result accessRes=NOCHANGE;
            if(std::strcmp(access.c_str(), "lan")==0){
                accessRes=LAN;
            } else if(std::strcmp(access.c_str(), "wan")==0){
                accessRes=WAN;
            } else if(std::strcmp(access.c_str(), "lanwan")==0){
                accessRes=FULL;
            }
            ret.push_back(DhcpLease(mac,ip,dev,GUEST,accessRes,strength));
        } else if (iswireless){
            ret.push_back(DhcpLease(mac,ip,dev,WIRELESS,NOCHANGE,strength));
        } else {
            ret.push_back(DhcpLease(mac,ip,dev,WIRED,NOCHANGE,strength));
        }
        ips.pop_front();
        macs.pop_front();
        devs.pop_front();
    }
    return ret;
}

double WifiJoeClient::getWELeases(CommandEventHandler* guestEventHandler){
    std::vector<DhcpLease> ret;
    //WIRELESS
    WifiParameters wifiParams;
    wifiParams.setInterface(this->getWifiInterface());
    wifiParams.setMacAddress("");
    wifiParams.setNoise("");
    wifiParams.setSignalStrength("");
    WifiParameters returnWifiParams=this->wifi(wifiParams);
    std::list<std::string> wirelessMacs= returnWifiParams.getMacAddresses();
    std::list<std::string> wirelessStrengths= returnWifiParams.getSignalStrengths();
    std::string wirelessNoise= returnWifiParams.getNoise();
    //GUEST
    GuestStatusParameters guestStatusParams;
    guestStatusParams.setConnected("true");
    guestStatusParams.setAccess("true");
    GuestStatusParameters returnGuestStatusParams=this->gueststatus(guestStatusParams, guestEventHandler);
    std::list<std::string> guestNames=returnGuestStatusParams.getConnectedDevices();
    std::list<std::string> accessLevels=returnGuestStatusParams.getAccesses();
    //LEASES
    LeasesParameters leasesParams;
    LeasesParameters returnParams=this->leases(leasesParams);
    std::list<std::string> devs=returnParams.getNames();
    std::list<std::string> ips=returnParams.getIPAddresses();
    std::list<std::string> macs=returnParams.getMacAddresses();
    while(!ips.empty()){
        std::string ip=ips.front();
        std::string mac=macs.front();
        std::string dev=devs.front();
        std::transform(mac.begin(), mac.end(),mac.begin(), ::toupper);
        const bool isguest = (std::find(guestNames.begin(),guestNames.end(),mac)!=guestNames.end());
        std::list<std::string>::iterator it=std::find(wirelessMacs.begin(),wirelessMacs.end(),mac);
        const bool iswireless = (it!=wirelessMacs.end());
        SignalStrength strength = SignalStrength::PERFECT;
        double SNR = 0;
        if(iswireless || isguest){
            int index = std::distance(wirelessMacs.begin(), it);
            std::list<std::string>::iterator itS = wirelessStrengths.begin();
            std::advance(itS, index);
            std::string wirelessStrength = *itS;
            std::string::size_type sz;
            double wStrength = std::stod (wirelessStrength,&sz);
            double wNoise = std::stod (wirelessNoise,&sz);
            SNR = wStrength - wNoise;
            if(SNR < 16){
                strength = SignalStrength::BAD;
            } else if (SNR >= 16 && SNR<26){
                strength = SignalStrength::SLOW;
            } else if (SNR >= 26){
                strength = SignalStrength::GOOD;
            }
        }
        return SNR;
    }
    return 0;
}
double WifiJoeClient::getWESignal(CommandEventHandler* guestEventHandler){
    std::vector<DhcpLease> leases=getLeases();
    for(std::vector<int>::size_type i = 0; i != leases.size(); i++) {
        DhcpLease lease=leases[i];
        if(lease.type==WIRELESS){
            NSLog(@"strength %d",lease.strength);
            return lease.strength;
        }
    }
    return 0;
}

Result WifiJoeClient::arpPing(std::string ip, int timeout){
    ARPPingParameters arppingParams;
    arppingParams.setIPAddress(ip);
    char timeoutStr[8];
    sprintf(timeoutStr, "%d", timeout);
    arppingParams.setTimeout(timeoutStr);
    ARPPingParameters returnParams=this->arpping(arppingParams);
    if (returnParams.getResult() == "success"){
		return SUCCESS;
	}else if (returnParams.getResult() == "failed"){
		return FAIL;
	}else{
		return ERROR;
	}
}

std::vector<ArpEntry> WifiJoeClient::getArpTable(std::string devname){
    std::vector<ArpEntry> ret;
    ARPTableParameters arptableParams;
    arptableParams.setDevice(devname);
    ARPTableParameters returnParams=this->arptable(arptableParams);
    std::list<std::string> ips=returnParams.getIPAddresses();
    std::list<std::string> macs=returnParams.getMacAddresses();
    std::list<std::string> devs=returnParams.getDeviceNames();
    while(!ips.empty()){
        std::string ip=ips.front();
        std::string mac=macs.front();
        std::string dev=devs.front();
        ret.push_back(ArpEntry(mac,ip,dev));
        ips.pop_front();
        macs.pop_front();
        devs.pop_front();
    }
    return ret;
}

std::vector<IfaceType> WifiJoeClient::getInterfaces(){
    std::vector<IfaceType> ret;
    InterfacesParameters interfacesParams;
    InterfacesParameters returnParams=this->interfaces(interfacesParams);
    std::string wirelessinterf=this->getWifiInterface();
    ret.push_back(IfaceType(wirelessinterf,"wireless"));
    GuestStatusParameters guestStatusParams;
    GuestStatusParameters returnGuestStatusParams=this->gueststatus(guestStatusParams,NULL);
    std::string guestinterf=returnGuestStatusParams.getInterface();
    ret.push_back(IfaceType(guestinterf,"guest"));
    std::list<std::string> types=returnParams.getTypes();
    std::list<std::string> devs=returnParams.getDeviceNames();
    while(!types.empty()){
        std::string type=types.front();
        std::string dev=devs.front();
        if(dev.compare(wirelessinterf)!=0 && dev.compare(guestinterf)!=0){
            ret.push_back(IfaceType(dev,type));
        }
        types.pop_front();
        devs.pop_front();
    }
    return ret;
}

Iface WifiJoeClient::getInterface(std::string iface){
    InterfacesParameters interfacesParams;
    interfacesParams.setDevice(iface);
    InterfacesParameters returnParams=this->interfaces(interfacesParams);
    std::list<std::string> macs=returnParams.getMacAddresses();
    std::list<std::string> ips=returnParams.getIPAddresses();
    std::list<std::string> masks=returnParams.getMasks();
    std::string ip=ips.front();
    std::string mac=macs.front();
    std::string mask=masks.front();
    Iface ret(iface,mac,ip,mask);
    return ret;
}

std::string WifiJoeClient::getWifiPermanentIf(){
    std::vector<IfaceType> ifaces=getInterfaces();
    for(std::vector<int>::size_type i = 0; i != ifaces.size(); i++) {
        /* std::cout << someVector[i]; ... */
        IfaceType intf=ifaces[i];
        if(intf.type.compare("wireless")!=0){
            return intf.devname;
        }
    }
    return "";
}

std::string WifiJoeClient::getGuestIf(){
    std::vector<IfaceType> ifaces=getInterfaces();
    for(std::vector<int>::size_type i = 0; i != ifaces.size(); i++) {
        /* std::cout << someVector[i]; ... */
        IfaceType intf=ifaces[i];
        if(intf.type.compare("guest")!=0){
            return intf.devname;
        }
    }
    return "";
    
}

std::string WifiJoeClient::getPermanentSsid() {
    return getWireless().ssid;
}

int WifiJoeClient::getPermanentChannel() {
    return getWireless().channel;
}

bool WifiJoeClient::isGatewayLocal() {
    NSLog(@"isGatewayLocal");
    std::string currentNWKey=fetchCurrentNetworkKey();
    if(currentNWKey.compare(*networkKey)!=0){
        NSLog(@"NETWORK CONNECTION HANDOVER isGatewayLocal");
        return false;
    }
    NSLog(@"isGatewayLocal continue");
    if(deviceIsConnected()){
        std::string tmpGwJid = getGatewayJID();
        bool tmpGwLocal = false;
        //if ((cf->isPresent(tmpGwJid) == false)) {
        CoreCommunication::ConnectList accList;
        CoreCommunication::ConnectList typeList;
        CoreCommunication::ConnectList localList;
        cf->getPresenceList(accList, typeList, localList);
        std::vector<std::string> listAccount{ std::begin(accList), std::end(accList) };
        std::vector<std::string> listType{ std::begin(typeList), std::end(typeList) };
        std::vector<std::string> listLocal{ std::begin(localList), std::end(localList) };
        int j = 0;
        while (j < listAccount.size()) {
            //System.out.println("Account " + listAccount[j] + "  type: " + listType[j] + " local: " + listLocal[j]);
            NSLog(@"Account %s  type: %s local: %s",listAccount[j].c_str(),listType[j].c_str(), listLocal[j].c_str());
            if (listType[j].compare("gateway") == 0) {
                NSLog(@"found gateway jid: %s", listAccount[j].c_str());
                //System.out.println("LoginActivity: found gateway jid: " + listAccount[j]);
                tmpGwJid = listAccount[j];
                if (listLocal[j].compare("true") == 0) {
                    NSLog(@"Gateway is local connected jid: %s", tmpGwJid.c_str());
                    //System.out.println("LoginActivity: Gateway is local connected jid: " + tmpGwJid);
                    tmpGwLocal = true;
                }
                break;
            }
            j++;
        }
        if (cf->isPresent(tmpGwJid) == false) {
            //System.out.println("LoginActivity: gateway not present jid: " + tmpGwJid);
            NSLog(@"gateway not present jid: %s", tmpGwJid.c_str() );
            return false;
        }
        gatewayJID = new std::string(tmpGwJid);
        //gwIsLocalAccessible = tmpGwLocal;
        return tmpGwLocal;
    }
    //}
    return false;
}

bool WifiJoeClient::isExtenderAvailable() {
    NSLog(@"isExtenderAvailable");
    std::string currentNWKey=fetchCurrentNetworkKey();
    if(currentNWKey.compare(*networkKey)!=0){
        NSLog(@"NETWORK CONNECTION HANDOVER isExtenderAvailable");
        return false;
    }
    NSLog(@"isExtenderAvailable continue");
    if(deviceIsConnected()){
        std::string tmpGwJid = getGatewayJID();
        bool tmpExt = false;
        //if ((cf->isPresent(tmpGwJid) == false)) {
        CoreCommunication::ConnectList accList;
        CoreCommunication::ConnectList typeList;
        CoreCommunication::ConnectList localList;
        cf->getPresenceList(accList, typeList, localList);
        std::vector<std::string> listAccount{ std::begin(accList), std::end(accList) };
        std::vector<std::string> listType{ std::begin(typeList), std::end(typeList) };
        std::vector<std::string> listLocal{ std::begin(localList), std::end(localList) };
        int j = 0;
        while (j < listAccount.size()) {
            //System.out.println("Account " + listAccount[j] + "  type: " + listType[j] + " local: " + listLocal[j]);
            NSLog(@"Account %s  type: %s local: %s",listAccount[j].c_str(),listType[j].c_str(), listLocal[j].c_str());
            if (listType[j].compare("extender") == 0) {
                NSLog(@"found extender jid: %s", listAccount[j].c_str());
                //System.out.println("LoginActivity: found gateway jid: " + listAccount[j]);
                tmpGwJid = listAccount[j];
                if (listLocal[j].compare("true") == 0) {
                    NSLog(@"extender is local connected jid: %s", tmpGwJid.c_str());
                    //System.out.println("LoginActivity: Gateway is local connected jid: " + tmpGwJid);
                    tmpExt = true;
                }
                break;
            }
            j++;
        }
        if (cf->isPresent(tmpGwJid) == false) {
            //System.out.println("LoginActivity: gateway not present jid: " + tmpGwJid);
            NSLog(@"extender not present jid: %s", tmpGwJid.c_str() );
            return false;
        }
        gatewayJID = new std::string(tmpGwJid);
        //gwIsLocalAccessible = tmpGwLocal;
        return tmpExt;
    }
    //}
    return false;
}

std::string  WifiJoeClient::getWPSStatus(CommandEventHandler* wpsEventHandler){
    //BUILD COMMAND
    CommandEntry *cEntry=new CommandEntry("wps", "");
    //BUILD REQUEST
    cEntry->setInput("activate","");
    cEntry->setPublicObservable();
    //SEND REQUEST
    std::string to = this->getGatewayJID();
    WifiJoeEventHandler* ceh=new WifiJoeEventHandler();
    cf->sendCommand("netMgr", cEntry, to, ceh, wpsEventHandler);
    //WAIT FOR REPLY
    bool gotRep=false;
    while(!gotRep){
        sleep(1);
        gotRep=ceh->gotReply();
    }
    //BUILD REPLY
    std::string result;
    CommandEntry* reply=ceh->getReply();
    /*
    ParamEntry* resultEntry=reply->findReplyParam("result");
    if(resultEntry!=NULL){
        std::string value=resultEntry->value;
        result=value;
    }*/
    ParamEntry* presult=reply->findReplyParam("result");
    if(presult!=NULL){
        std::list<std::string> values=presult->valueList;
        std::vector<std::string> listResult{ std::begin(values), std::end(values) };
        result = listResult[0].c_str();
    };

    
    return result;
}

NSMutableArray* WifiJoeClient::getLANDevices() {
    NSLog(@"getLANDevices");
    NSMutableArray* listDevices = [[NSMutableArray alloc] init];
    if (!deviceIsConnected()) {
        NSLog(@"getLANDevices device not connected");
        return listDevices;
    }
    
    CoreCommunication::ConnectList accList;
    CoreCommunication::ConnectList typeList;
    CoreCommunication::ConnectList localList;
    cf->getPresenceList(accList, typeList, localList);
    std::vector<std::string> listAccount{ std::begin(accList), std::end(accList) };
    std::vector<std::string> listType{ std::begin(typeList), std::end(typeList) };
    std::vector<std::string> listLocal{ std::begin(localList), std::end(localList) };
    int j = 0;
    while (j < listAccount.size()) {
        LanDevice* device= [[LanDevice alloc] init];
        device.type=[NSString stringWithUTF8String:listType[j].c_str()];
        device.jid=[NSString stringWithUTF8String:listAccount[j].c_str()];
        [listDevices addObject:device];
        [device release];
        j++;
    }
    
    return listDevices;
    
    
}
