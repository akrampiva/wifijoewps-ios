/*
 *   @file CloudFriendsWrapper.mm
 *   @author Kurt Vermeersch
 *
 *   Wrapper for Objective-C to gain access to Cloud Friends API
 *
 *   Copyright 2013 Cloud Friends. All rights reserved.
 */

#import "CloudFriendsWrapper.h"

@implementation CloudFriendsWrapper

-(id)init{
    self=[super init];
    if(self){
        cf=new WifiJoeClient();
    }
    return self;
}

-(id)destroyClient{
    NSLog(@"DISCONNECT destroyClient");
    delete cf;
    return self;
}

@end
