/*
 *   @file CloudFriendsWrapper.h
 *   @author Kurt Vermeersch
 *
 *   Wrapper for Objective-C to gain access to Cloud Friends API
 *
 *   Copyright 2013 Cloud Friends. All rights reserved.
 */

#import <Foundation/Foundation.h>
#import "WifiJoeClient.h"
#import "CloudFriends.h"


@interface CloudFriendsWrapper : NSObject{
    @public
        WifiJoeClient* cf;
}

-(id)destroyClient;

@end
