//
//  WifiJoeClient.h
//  Test
//
//  Created by Kurt Vermeersch on 25/02/13.
//
//

#ifndef Test_WifiJoeClient_h
#define Test_WifiJoeClient_h

#import "WifiJoe.h"
#import "CloudFriends.h"

class WifiParameters {
private:
    std::string interface;
    bool hasinterface;
    std::string ssid;
    bool hasssid;
    std::string channel;
    bool haschannel;
    std::string state;
    bool hasstate;
    std::string password;
    bool haspassword;
    std::string security;
    bool hassecurity;
    std::string noise;
    bool hasnoise;
    std::string signalStrength;
    bool hassignalstrength;
    std::string assocTime;
    bool hasassoctime;
    std::string macAddress;
    bool hasmacaddress;
    std::list<std::string> signalStrengths;
    bool hassignalstrengths;
    std::list<std::string> signalNoises;
    bool hassignalnoises;
    std::list<std::string> assocTimes;
    bool hasassoctimes;
    std::list<std::string> macAddresses;
    bool hasmacaddresses;
    std::string result;
    bool hasresult;
    std::list<std::string> interfaces;
    bool hasinterfaces;
public:
    WifiParameters(void){
        hasinterface=false;
        hasssid=false;
        haschannel=false;
        hasstate=false;
        haspassword=false;
        hassecurity=false;
        hasnoise=false;
        hassignalstrength=false;
        hasassoctime=false;
        hasmacaddress=false;
        hassignalnoises=false;
        hassignalstrengths=false;
        hasassoctimes=false;
        hasmacaddresses=false;
        hasresult=false;
        hasinterfaces=false;
    }
    virtual ~WifiParameters(){
    }
    
    void setInterface(std::string interf){
        interface=interf;
        hasinterface=true;
    }
    bool hasInterface(){
        return hasinterface;
    }
    std::string getInterface(){
        return interface;
    }
    
    void setSSID(std::string ssidp){
        ssid=ssidp;
        hasssid=true;
    }
    bool hasSSID(){
        return hasssid;
    }
    std::string getSSID(){
        return ssid;
    }
    
    void setChannel(std::string channelp){
        channel=channelp;
        haschannel=true;
    }
    bool hasChannel(){
        return haschannel;
    }
    std::string getChannel(){
        return channel;
    }
    
    void setState(std::string statep){
        state=statep;
        hasstate=true;
    }
    bool hasState(){
        return hasstate;
    }
    std::string getState(){
        return state;
    }
    
    void setPassword(std::string passwordp){
        password=passwordp;
        haspassword=true;
    }
    bool hasPassword(){
        return haspassword;
    }
    std::string getPassword(){
        return password;
    }
    
    void setSecurity(std::string securityp){
        security=securityp;
        hassecurity=true;
    }
    bool hasSecurity(){
        return hassecurity;
    }
    std::string getSecurity(){
        return security;
    }
    
    void setNoise(std::string noisep){
        noise=noisep;
        hasnoise=true;
    }
    bool hasNoise(){
        return hasnoise;
    }
    std::string getNoise(){
        return noise;
    }
    
    void setSignalStrength(std::string signalStrengthp){
        signalStrength=signalStrengthp;
        hassignalstrength=true;
    }
    bool hasSignalStrength(){
        return hassignalstrength;
    }
    std::string getSignalStrength(){
        return signalStrength;
    }
    
    void setAssocTime(std::string assocTimep){
        assocTime=assocTimep;
        hasassoctime=true;
    }
    bool hasAssocTime(){
        return hasassoctime;
    }
    std::string getAssocTime(){
        return assocTime;
    }
    
    void setMacAddress(std::string macAddressp){
        macAddress=macAddressp;
        hasmacaddress=true;
    }
    bool hasMacAddress(){
        return hasmacaddress;
    }
    std::string getMacAddress(){
        return macAddress;
    }
    
    void setSignalStrengths(std::list<std::string> signalStrengthsp){
        signalStrengths=signalStrengthsp;
        hassignalstrengths=true;
    }
    bool hasSignalStrengths(){
        return hassignalstrengths;
    }
    std::list<std::string> getSignalStrengths(){
        return signalStrengths;
    }
    
    void setSignalNoises(std::list<std::string> signalStrengthsp){
        signalNoises=signalStrengthsp;
        hassignalnoises=true;
    }
    bool hasSignalNoises(){
        return hassignalnoises;
    }
    std::list<std::string> getSignalNoises(){
        return signalNoises;
    }
    
    void setAssocTimes(std::list<std::string> assocTimesp){
        assocTimes=assocTimesp;
        hasassoctimes=true;
    }
    bool hasAssocTimes(){
        return hasassoctimes;
    }
    std::list<std::string> getAssocTimes(){
        return assocTimes;
    }
    
    void setMacAddresses(std::list<std::string> macAddressesp){
        macAddresses=macAddressesp;
        hasmacaddresses=true;
    }
    bool hasMacAddresses(){
        return hasmacaddresses;
    }
    std::list<std::string> getMacAddresses(){
        return macAddresses;
    }
    
    void setResult(std::string resultp){
        result=resultp;
        hasresult=true;
    }
    bool hasResult(){
        return hasresult;
    }
    std::string getResult(){
        return result;
    }

    void setInterfaces(std::list<std::string> interfacesp){
        interfaces=interfacesp;
        hasinterfaces=true;
    }
    bool hasInterfaces(){
        return hasinterfaces;
    }
    std::list<std::string> getInterfaces(){
        return interfaces;
    }
    
};

class ARPTableParameters {
private:
    std::string error;
    bool haserror;
    std::string device;
    bool hasdevice;
    std::list<std::string> mac;
    bool hasmac;
    std::list<std::string> ipAddr;
    bool hasipaddr;
    std::list<std::string> devices;
    bool hasdevices;
public:
    ARPTableParameters(void){
        haserror=false;
        hasdevice=false;
        hasmac=false;
        hasipaddr=false;
        hasdevices=false;
    }
    virtual ~ARPTableParameters(){
    }
    
    void setError(std::string errorp){
        error=errorp;
        haserror=true;
    }
    bool hasError(){
        return haserror;
    }
    std::string getError(){
        return error;
    }
    
    void setDevice(std::string dev){
        device=dev;
        hasdevice=true;
    }
    bool hasDevice(){
        return hasdevice;
    }
    std::string getDevice(){
        return device;
    }
    
    void setMacAddresses(std::list<std::string> macs){
        mac=macs;
        hasmac=true;
    }
    bool hasMacAddresses(){
        return hasmac;
    }
    std::list<std::string> getMacAddresses(){
        return mac;
    }
    
    void setIPAddresses(std::list<std::string> ips){
        ipAddr=ips;
        hasipaddr=true;
    }
    bool hasIPAddresses(){
        return hasipaddr;
    }
    std::list<std::string> getIPAddresses(){
        return ipAddr;
    }
    
    void setDeviceNames(std::list<std::string> devicesp){
        devices=devicesp;
        hasdevices=true;
    }
    bool hasDeviceNames(){
        return hasdevices;
    }
    std::list<std::string> getDeviceNames(){
        return devices;
    }
};

class ARPPingParameters {
private:
    std::string error;
    bool haserror;
    std::string result;
    bool hasresult;
    std::string ipAddr;
    bool hasipaddr;
    std::string timeout;
    bool hastimeout;
public:
    ARPPingParameters(void){
        haserror=false;
        hasresult=false;
        hasipaddr=false;
        hastimeout=false;
    }
    virtual ~ARPPingParameters(){
    }
    
    void setError(std::string errorp){
        error=errorp;
        haserror=true;
    }
    bool hasError(){
        return haserror;
    }
    std::string getError(){
        return error;
    }
    
    void setResult(std::string resultp){
        result=resultp;
        hasresult=true;
    }
    bool hasResult(){
        return hasresult;
    }
    std::string getResult(){
        return result;
    }
    
    void setIPAddress(std::string ips){
        ipAddr=ips;
        hasipaddr=true;
    }
    bool hasIPAddress(){
        return hasipaddr;
    }
    std::string getIPAddress(){
        return ipAddr;
    }
    
    void setTimeout(std::string timeoutp){
        timeout=timeoutp;
        hastimeout=true;
    }
    bool hasTimeout(){
        return hastimeout;
    }
    std::string getTimeout(){
        return timeout;
    }
};

class LeasesParameters {
private:
    std::string error;
    bool haserror;
    std::string ipAddr;
    bool hasipaddr;
    std::list<std::string> mac;
    bool hasmac;
    std::list<std::string> ipAddrs;
    bool hasipaddrs;
    std::list<std::string> name;
    bool hasname;
public:
    LeasesParameters(void){
        haserror=false;
        hasipaddr=false;
        hasmac=false;
        hasipaddrs=false;
        hasname=false;
    }
    virtual ~LeasesParameters(){
    }
    
    void setError(std::string errorp){
        error=errorp;
        haserror=true;
    }
    bool hasError(){
        return haserror;
    }
    std::string getError(){
        return error;
    }
    
    void setIPAddress(std::string ips){
        ipAddr=ips;
        hasipaddr=true;
    }
    bool hasIPAddress(){
        return hasipaddr;
    }
    std::string getIPAddress(){
        return ipAddr;
    }
    
    void setMacAddresses(std::list<std::string> macs){
        mac=macs;
        hasmac=true;
    }
    bool hasMacAddresses(){
        return hasmac;
    }
    std::list<std::string> getMacAddresses(){
        return mac;
    }
    
    void setIPAddresses(std::list<std::string> ips){
        ipAddrs=ips;
        hasipaddrs=true;
    }
    bool hasIPAddresses(){
        return hasipaddrs;
    }
    std::list<std::string> getIPAddresses(){
        return ipAddrs;
    }
    
    void setNames(std::list<std::string> names){
        name=names;
        hasname=true;
    }
    bool hasNames(){
        return hasname;
    }
    std::list<std::string> getNames(){
        return name;
    }
};

class InterfacesParameters {
private:
    std::string device;
    bool hasdevice;
    std::list<std::string> type;
    bool hastype;
    std::list<std::string> mac;
    bool hasmac;
    std::list<std::string> ipAddrs;
    bool hasipaddrs;
    std::list<std::string> name;
    bool hasname;
    std::list<std::string> mask;
    bool hasmask;
    std::list<std::string> state;
    bool hasstate;
    std::list<std::string> rx;
    bool hasrx;
    std::list<std::string> tx;
    bool hastx;
public:
    InterfacesParameters(void){
        hasdevice=false;
        hastype=false;
        hasmac=false;
        hasipaddrs=false;
        hasname=false;
        hasmask=false;
        hasstate=false;
        hasrx=false;
        hastx=false;
    }
    virtual ~InterfacesParameters(){
    }
    
    void setDevice(std::string devicep){
        device=devicep;
        hasdevice=true;
    }
    bool hasDevice(){
        return hasdevice;
    }
    std::string getDevice(){
        return device;
    }
    
    void setTypes(std::list<std::string> types){
        type=types;
        hastype=true;
    }
    bool hasTypes(){
        return hastype;
    }
    std::list<std::string> getTypes(){
        return type;
    }
    
    void setMacAddresses(std::list<std::string> macs){
        mac=macs;
        hasmac=true;
    }
    bool hasMacAddresses(){
        return hasmac;
    }
    std::list<std::string> getMacAddresses(){
        return mac;
    }
    
    void setIPAddresses(std::list<std::string> ips){
        ipAddrs=ips;
        hasipaddrs=true;
    }
    bool hasIPAddresses(){
        return hasipaddrs;
    }
    std::list<std::string> getIPAddresses(){
        return ipAddrs;
    }
    
    void setDeviceNames(std::list<std::string> names){
        name=names;
        hasname=true;
    }
    bool hasDeviceNames(){
        return hasname;
    }
    std::list<std::string> getDeviceNames(){
        return name;
    }
    
    void setMasks(std::list<std::string> masks){
        mask=masks;
        hasmask=true;
    }
    bool hasMasks(){
        return hasmask;
    }
    std::list<std::string> getMasks(){
        return mask;
    }
    
    void setStates(std::list<std::string> states){
        state=states;
        hasstate=true;
    }
    bool hasStates(){
        return hasstate;
    }
    std::list<std::string> getStates(){
        return state;
    }
    
    void setRXBytes(std::list<std::string> rxs){
        rx=rxs;
        hasrx=true;
    }
    bool hasRXBytes(){
        return hasrx;
    }
    std::list<std::string> getRXBytes(){
        return rx;
    }
    
    void setTXBytes(std::list<std::string> txs){
        tx=txs;
        hastx=true;
    }
    bool hasTXBytes(){
        return hastx;
    }
    std::list<std::string> getTXBytes(){
        return tx;
    }
};

class GuestStatusParameters {
private:
    std::string connected;
    bool hasconnected;
    std::string access;
    bool hasaccess;
    std::string time;
    bool hastime;
    std::string result;
    bool hasresult;
    std::string interface;
    bool hasinterface;
    std::list<std::string> connecteds;
    bool hasconnecteds;
    std::list<std::string> accesses;
    bool hasaccesses;
    std::list<std::string> times;
    bool hastimes;
    Result state;
    bool hasstate;
public:
    GuestStatusParameters(void){
        hasconnected=false;
        hasaccess=false;
        hastime=false;
        hasresult=false;
        hasinterface=false;
        hasconnecteds=false;
        hasaccesses=false;
        hastimes=false;
        hasstate=false;
    }
    virtual ~GuestStatusParameters(){
    }
    
    void setConnected(std::string conn){
        connected=conn;
        hasconnected=true;
    }
    bool hasConnected(){
        return hasconnected;
    }
    std::string getConnected(){
        return connected;
    }

    void setState(Result st){
        state=st;
        hasstate=true;
    }
    bool hasState(){
        return hasstate;
    }
    Result getState(){
        return state;
    }
    
    void setAccess(std::string accessp){
        access=accessp;
        hasaccess=true;
    }
    bool hasAccess(){
        return hasaccess;
    }
    std::string getAccess(){
        return access;
    }
    
    void setTime(std::string t){
        time=t;
        hastime=true;
    }
    bool hasTime(){
        return hastime;
    }
    std::string getTime(){
        return time;
    }
    
    void setResult(std::string res){
        result=res;
        hasresult=true;
    }
    bool hasResult(){
        return hasresult;
    }
    std::string getResult(){
        return result;
    }
    
    void setInterface(std::string interfacep){
        interface=interfacep;
        hasinterface=true;
    }
    bool hasInterface(){
        return hasinterface;
    }
    std::string getInterface(){
        return interface;
    }
    
    void setConnectedDevices(std::list<std::string> connectedDevs){
        connecteds=connectedDevs;
        hasconnecteds=true;
    }
    bool hasConnectedDevices(){
        return hasconnecteds;
    }
    std::list<std::string> getConnectedDevices(){
        return connecteds;
    }
    
    void setAccesses(std::list<std::string> accessesp){
        accesses=accessesp;
        hasaccesses=true;
    }
    bool hasAccesses(){
        return hasaccesses;
    }
    std::list<std::string> getAccesses(){
        return accesses;
    }
    
    void setTimes(std::list<std::string> timesp){
        times=timesp;
        hastimes=true;
    }
    bool hasTimes(){
        return hastimes;
    }
    std::list<std::string> getTimes(){
        return times;
    }
};

class GuestCreateParameters {
private:
    std::string ssid;
    bool hasssid;
    std::string inputdelay;
    bool hasinputdelay;
    std::string outputdelay;
    bool hasoutputdelay;
    std::string result;
    bool hasresult;
    Result state;
    bool hasstate;
public:
    GuestCreateParameters(void){
        hasssid=false;
        hasinputdelay=false;
        hasoutputdelay=false;
        hasresult=false;
        hasstate=false;
    }
    virtual ~GuestCreateParameters(){
    }
    
    void setSSID(std::string ssidp){
        ssid=ssidp;
        hasssid=true;
    }
    bool hasSSID(){
        return hasssid;
    }
    std::string getSSID(){
        return ssid;
    }
    
    void setInputDelay(std::string idelay){
        inputdelay=idelay;
        hasinputdelay=true;
    }
    bool hasInputDelay(){
        return hasinputdelay;
    }
    std::string getInputDelay(){
        return inputdelay;
    }
    
    void setOutputDelay(std::string odelay){
        outputdelay=odelay;
        hasoutputdelay=true;
    }
    bool hasOutputDelay(){
        return hasoutputdelay;
    }
    std::string getOutputDelay(){
        return outputdelay;
    }
    void setResult(std::string res){
        result=res;
        hasresult=true;
    }
    bool hasResult(){
        return hasresult;
    }
    std::string getResult(){
        return result;
    }
    void setState(Result res){
        state=res;
        hasstate=true;
    }
    bool hasState(){
        return hasstate;
    }
    Result getState(){
        return state;
    }
};

class GuestRemoveParameters {
private:
    std::string result;
    bool hasresult;
public:
    GuestRemoveParameters(void){
        hasresult=false;
    }
    virtual ~GuestRemoveParameters(){
    }
    
    void setResult(std::string res){
        result=res;
        hasresult=true;
    }
    bool hasResult(){
        return hasresult;
    }
    std::string getResult(){
        return result;
    }
};

class GuestAccessParameters {
private:
    std::string ipAddr;
    bool hasipaddr;
    std::string type;
    bool hastype;
    std::string result;
    bool hasresult;
public:
    GuestAccessParameters(void){
        hasipaddr=false;
        hastype=false;
        hasresult=false;
    }
    virtual ~GuestAccessParameters(){
    }
    
    void setIPAddress(std::string ipAddrp){
        ipAddr=ipAddrp;
        hasipaddr=true;
    }
    bool hasIPAddress(){
        return hasipaddr;
    }
    std::string getIPAddress(){
        return ipAddr;
    }
    
    void setType(std::string typep){
        type=typep;
        hastype=true;
    }
    bool hasType(){
        return hastype;
    }
    std::string getType(){
        return type;
    }
    
    void setResult(std::string res){
        result=res;
        hasresult=true;
    }
    bool hasResult(){
        return hasresult;
    }
    std::string getResult(){
        return result;
    }
};

class IdentifyDeviceParameters {
private:
    std::string uniquedeviceid;
    bool hasuniquedeviceid;
    std::string type;
    bool hastype;
public:
    IdentifyDeviceParameters(void){
        hasuniquedeviceid=false;
        hastype=false;
    }
    virtual ~IdentifyDeviceParameters(){
    }
    
    void setUniqueDeviceId(std::string uniqueDevId){
        uniquedeviceid=uniqueDevId;
        hasuniquedeviceid=true;
    }
    bool hasUniqueDeviceId(){
        return hasuniquedeviceid;
    }
    std::string getUniqueDeviceId(){
        return uniquedeviceid;
    }
    
    void setType(std::string typep){
        type=typep;
        hastype=true;
    }
    bool hasType(){
        return hastype;
    }
    std::string getType(){
        return type;
    }
};

class WifiJoeClient {
public:
    WifiJoeClient(void);
    virtual ~WifiJoeClient();
    bool isConnectedToServer();
    bool deviceIsConnected();
    std::string getGatewayJID();
    std::string getWifiInterface();
    WlSettings setWireless(WlSettings settings);
    WlSettings getWireless();
    GuestIface setGuestnet(GuestIface netsettings);
    GuestIface getGuestnet();
    void isPresent(const char *from);
    void isNotPresent(const char *from);
    std::vector<DhcpLease> getLeases(CommandEventHandler* guestEventHandler=NULL);
    double getWELeases(CommandEventHandler* guestEventHandler=NULL);
    double getWESignal(CommandEventHandler* guestEventHandler=NULL);
    Result arpPing(std::string ip, int timeout = 2);
    std::vector<ArpEntry> getArpTable(std::string devicename="");
    std::vector<IfaceType> getInterfaces();
    Iface getInterface(std::string iface);
    void setGuestRule(std::string ip, Result policy);
    CloudFriends* cf;
    std::string getWifiPermanentIf();
    std::string getGuestIf();
    std::string getPermanentSsid();
    int getPermanentChannel();
    bool isGatewayLocal();
    bool isExtenderAvailable();
    std::string getWPSStatus(CommandEventHandler* wpsEventHandler=NULL);
    NSMutableArray* getLANDevices();
    /*bool isTryingToReconnect();
    void setTryingToReconnect(bool onoff);*/
    std::string getSavedNetworkKey();
    void setSavedNetworkKey(std::string nwKey);
    std::string fetchCurrentNetworkKey();
private:
    IdentifyDeviceParameters identifyDevice(std::string to, IdentifyDeviceParameters params);
    WifiParameters wifi(WifiParameters wifiParams);
    InterfacesParameters interfaces(InterfacesParameters interfacesParams);
    LeasesParameters leases(LeasesParameters leasesParams);
    ARPTableParameters arptable(ARPTableParameters arptableParams);
    ARPPingParameters arpping(ARPPingParameters arppingParams);
    GuestCreateParameters guestcreate(GuestCreateParameters createParams);
    GuestRemoveParameters guestremove(GuestRemoveParameters removeParams);
    GuestStatusParameters gueststatus(GuestStatusParameters statusParams, CommandEventHandler* eventHandler);
    GuestAccessParameters guestaccess(GuestAccessParameters accessParams);
    std::string* gatewayJID;
    std::string* networkKey;
    int tryingToReconnect;
};

#endif
