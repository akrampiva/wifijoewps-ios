//
//  WifiJoe.h
//  Test
//
//  Created by Kurt Vermeersch on 25/02/13.
//
//

#ifndef Test_WifiJoe_h
#define Test_WifiJoe_h

#include <dirent.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <cstdio>
#include <semaphore.h>
#include <vector>
#include <exception>
#include <netinet/in.h>
#include <pthread.h>
#include <string>
#include <map>

typedef enum{SUCCESS=1, FAIL=0, ERROR=-1, UP=SUCCESS, DOWN=FAIL, NOCHANGE=ERROR, WIRED, WIRELESS, GUEST, DENY=0, WAN=1, LAN=2, FULL=WAN|LAN} Result;


typedef enum{BAD=0, SLOW=1, GOOD=2, PERFECT=3} SignalStrength;

//type is {"lan", "wan", "wireless", "guest"}
struct IfaceType{
	inline IfaceType(std::string a, std::string b):
    devname(a), type(b)
	{}
	
	std::string	devname;
	std::string	type;
};

struct Iface{
	inline Iface(std::string a, std::string b, std::string c, std::string d):
    devname(a), mac(b), ip(c), netmask(d)
	{}
    
	std::string	devname;
	std::string	mac;
	std::string	ip;
	std::string	netmask;
};

struct ArpEntry{
	inline ArpEntry(std::string a, std::string b, std::string c):
    mac(a), ip(b), devname(c)
	{}
    
	std::string	mac;
	std::string	ip;
	std::string	devname;
};

//An asterisk "*" as hostname means the pc had not correctly configured it
struct DhcpLease{
	inline DhcpLease(std::string a, std::string b, std::string c, Result d, Result ac,SignalStrength st):
    mac(a), ip(b), hostname(c), type(d), access(ac), strength(st)
	{}
    
	std::string	mac;
	std::string	ip;
	std::string	hostname;
	Result		type;
    Result		access;
    SignalStrength strength;
};

//status is UP/DOWN or NOCHANGE (when setting) or ERROR (when getting)
//timeout:	-1	Do not change
//		0	Disable automatic disabling
struct GuestIface{
	inline GuestIface(std::string a, Result b, int c):
    ssid(a), status(b), timeout(c)
	{}
    
	std::string	ssid;
	Result		status;
	int		timeout;
};

//negative channel leaves it unchanged
struct WlSettings{
	inline WlSettings(std::string a, int b):
    ssid(a), channel(b)
	{}
    inline WlSettings(std::string s, std::string p, int c):
    ssid(s), key(p), channel(c)
    {}
    
	std::string	ssid;
	int		channel;
    std::string	key;
	//read-only
	std::string	security; // disabled / wep / psk(=wpa) / psk2(=wpa2)
};


#endif
