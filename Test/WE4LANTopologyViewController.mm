//
//  WE4LANTopologyViewController.m
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import "WE4LANTopologyViewController.h"
#import "deviceutils.h"
#import "LanDevice.h"
#import "colors.h"

#define margin 20
#define nbzone 4
#define smargin 20

@interface WE4LANTopologyViewController ()

@end

@implementation WE4LANTopologyViewController

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap{
    self = [super init];
    if (self) {
        // Custom initialization
        client = cfwrap;
    }
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if(IS_IPAD)
        {
            self = [super initWithNibName:[NSString stringWithFormat:@"%@-iPad",nibNameOrNil] bundle:nibBundleOrNil];
        }
        else{
            self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(isIOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = @"LAN Topology" ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    
    [self getListDevicesInTheLAN];
}

- (IBAction)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getListDevicesInTheLAN{
    [_initialIndicator setHidden:NO];
    [_initialIndicator startAnimating];
    
    //Start an activity indicator here
    nbExtenders = 0;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray* landevices=client->cf->getLANDevices();
        if(zone3Devices)
            [zone3Devices removeAllObjects];
        else
            zone3Devices = [[NSMutableArray alloc] init];
        if(zone4Devices)
            [zone4Devices removeAllObjects];
        else
            zone4Devices = [[NSMutableArray alloc] init];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            
            [_initialIndicator setHidden:YES];
            [_initialIndicator stopAnimating];
            if(landevices && landevices.count > 0)
            {
                for(int i=0; i < landevices.count;i++)
                {
                    LanDevice* device = [landevices objectAtIndex:i];
                    NSLog(@"device %d jid %@",i , device.jid);
                    NSLog(@"device %d type %@",i , device.type);
                    if([device.type isEqual:@"extender"])
                    nbExtenders++;
                    if(![device.type isEqual:@"gateway"])
                    {
                        [zone3Devices  addObject:device];
                    }
                    
                    
                }
                [self drawLANTree];
            }
            
        });
    });
    
}
//zone 1 : internet
//zone 2 : gateway
//zone 3 : devices connected to gateway
//zone 4 : devices connected to extenders

-(void)drawLANTree{
    UIView* internetSubView = [[UIView alloc] init];
    [self.view addSubview:internetSubView];
    UIView* gatewaySubView = [[UIView alloc] init];
    [self.view addSubview:gatewaySubView];
    UIView* gatewayDevicesSubView = [[UIView alloc] init];
    [self.view addSubview:gatewayDevicesSubView];
    UIView* extendersDevicesSubView = [[UIView alloc] init];
    [self.view addSubview:extendersDevicesSubView];
    
    CGRect subViewFrame= CGRectMake(margin, margin, self.view.frame.size.width - margin*2, (self.view.frame.size.height - margin*2)/nbzone);
    internetSubView.frame = subViewFrame;
    subViewFrame.origin.y = subViewFrame.origin.y + subViewFrame.size.height;
    gatewaySubView.frame = subViewFrame;
    subViewFrame.origin.y = subViewFrame.origin.y + subViewFrame.size.height;
    gatewayDevicesSubView.frame = subViewFrame;
    subViewFrame.origin.y = subViewFrame.origin.y + subViewFrame.size.height;
    extendersDevicesSubView.frame = subViewFrame;
    
    
    //zone 1 : internet
    UIImageView* internetImgV=[[UIImageView alloc] init];
    CGRect f=internetImgV.frame;
    f.size.height = internetSubView.frame.size.height - smargin*2;
    f.size.width = internetSubView.frame.size.width - smargin*2;
    f.origin.y = smargin;
    f.origin.x = smargin;
    internetImgV.frame = f;
    [internetImgV setImage:[UIImage imageNamed:@"we_internet.png"]];
    internetImgV.contentMode = UIViewContentModeScaleAspectFit;
    [internetSubView addSubview:internetImgV];
    [internetImgV release];
    
    //zone 2 : gateway
    UIImageView* gatewayImgV=[[UIImageView alloc] init];
    f=gatewayImgV.frame;
    f.size.height = gatewaySubView.frame.size.height - smargin*2;
    f.size.width = gatewaySubView.frame.size.width - smargin*2;
    f.origin.y = smargin;
    f.origin.x = smargin;
    gatewayImgV.frame = f;
    [gatewayImgV setImage:[UIImage imageNamed:@"we_gateway.png"]];
    gatewayImgV.contentMode = UIViewContentModeScaleAspectFit;
    [gatewaySubView addSubview:gatewayImgV];
    [self drawLineToImage:gatewayImgV];
    [gatewayImgV release];
    
    //zone 3 : devices connected to gateway
    if(zone3Devices && zone3Devices.count > 0)
    {
        int nbdrawed = 0;
        for(int i=0; i < zone3Devices.count;i++)
        {
            LanDevice* device = [zone3Devices objectAtIndex:i];
            if([device.type isEqual:@"gateway"])
            continue;
            UIImageView* deviceImgV = [[UIImageView alloc] init];
            CGRect f=deviceImgV.frame;
            f.size.height = gatewayDevicesSubView.frame.size.height - smargin*2;
            f.size.width = gatewayDevicesSubView.frame.size.width/zone3Devices.count- smargin*2;
            f.origin.y = smargin;
            f.origin.x = smargin + (f.size.width + smargin)*nbdrawed;
            deviceImgV.frame = f;
            if([device.type isEqual:@"extender"])
            {
                [deviceImgV setImage:[UIImage imageNamed:@"we_extender.png"]];
            }
            else if([device.type isEqual:@"mobile"])
            {
                [deviceImgV setImage:[UIImage imageNamed:@"we_mobile.png"]];
            }
            deviceImgV.contentMode = UIViewContentModeScaleAspectFit;
            [gatewayDevicesSubView addSubview:deviceImgV];
            [self drawLineToImage:deviceImgV];
            nbdrawed ++;
            NSLog(@"deviceImgV height %f width %f x%f y%f",deviceImgV.frame.size.height
                  ,deviceImgV.frame.size.width
                  ,deviceImgV.frame.origin.x
                  ,deviceImgV.frame.origin.y);
            [deviceImgV release];
            
        }
        
    }
    //zone 4 : devices connected to extenders
    
    
}

-(void)drawLineToImage:(UIImageView*)imageView{
    UIImageView* imagev=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"we_line.png"]];
    CGRect f=imagev.frame;
    f.size.height = 24;
    f.size.width = 4;
    f.origin.y = -12;
    f.origin.x = imageView.frame.origin.x + imageView.frame.size.width/2 - 2;
    imagev.frame = f;
    [imageView.superview addSubview:imagev];
    [imagev release];
    
}
@end
