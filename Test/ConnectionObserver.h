//
//  ConnectionObserver.h
//  iopsys Wifi Joe
//
//  Created by Kurt Vermeersch on 30/03/14.
//
//

#ifndef iopsys_Wifi_Joe_ConnectionObserver_h
#define iopsys_Wifi_Joe_ConnectionObserver_h

@class LoginViewController;

class ConnectionObserver : public CoreConnectionHandler{
public:
    LoginViewController *delegate;
    
    ConnectionObserver(){
    }
    
    virtual ~ConnectionObserver(){}
    
    void connected()
    {
        NSLog(@"User connected.");
        SEL isconnselector = NSSelectorFromString(@"isconnected:");
        [delegate performSelectorOnMainThread:isconnselector withObject:[NSNumber numberWithBool:NO] waitUntilDone:NO];
    }
    
    void disconnected(const CoreConnectionHandler::DisconnectReason reason) {
        if(reason==AuthenticationFailed){
            NSLog(@"DISCONNECTED AuthenticationFailed");
            SEL isdisconnauthselector = NSSelectorFromString(@"isdisconnectedAuth:");
            [delegate performSelectorOnMainThread:isdisconnauthselector withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
        } else if (reason==ConnectionProblem){
             NSLog(@"DISCONNECTED ConnectionProblem");
            SEL isdisconnselector = NSSelectorFromString(@"isdisconnected:");
            [delegate performSelectorOnMainThread:isdisconnselector withObject:[NSNumber numberWithInt:7] waitUntilDone:YES];
        } else if (reason==OutOfMemory){
            NSLog(@"DISCONNECTED OutOfMemory");
            SEL isdisconnselector = NSSelectorFromString(@"isdisconnected:");
            [delegate performSelectorOnMainThread:isdisconnselector withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
        } else if (reason==UserDisconnect){
            NSLog(@"User disconnected.");
        }
    }
    
};


#endif
