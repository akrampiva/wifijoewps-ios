#ifndef CLOUDFRIENDS_IOS_COMMUNICATION_H__
#define CLOUDFRIENDS_IOS_COMMUNICATION_H__

#include <list>
#include <string>
#include "corecommunication.h"


class CoreConnectionHandler
{
    public:
        enum DisconnectReason {
            UserDisconnect,
            OutOfMemory,
            AuthenticationFailed,
            ConnectionProblem
        };

        virtual ~CoreConnectionHandler() {};
        virtual void connected(void) = 0;
        virtual void disconnected(const CoreConnectionHandler::DisconnectReason reason) = 0;
};

class CloudFriends: public CoreCommunication
{
    public:
//        CloudFriends(void);
        virtual ~CloudFriends();

        void registerConnectionHandler(CoreConnectionHandler *ch);
        void unregisterConnectionHandler(CoreConnectionHandler *ch);
        void connect(const std::string &userName,
                     const std::string &password, 
                     const std::string &server);
        void disconnect();
        void destroy();
        void pair(const std::string &server);
        bool isConnectedToServer();

    private:
};

#endif
