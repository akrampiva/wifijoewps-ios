//
//  @name LoginViewController.h
//
//  @desc The login viewcontroller handles the ios application login screen functions
//  @author Kurt Vermeersch.
//
//  Copyright 2013 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"
#import "SettingsSmallViewController.h"
#import "SignupViewController.h"
#import "ConnectionObserver.h"
#import "PresenceObserver.h"

@class SelectionScreenViewController;
@class SignupViewController;


@interface LoginViewController : UIViewController <UITextFieldDelegate> {
    
	// Text Fields
    IBOutlet UITextField *usernameField;
	IBOutlet UITextField *passwordField;
    // Labels
    IBOutlet UILabel *usernameLabel;
    IBOutlet UILabel *passwordLabel;
    // Buttons
    IBOutlet UIButton *forgotButton;
    IBOutlet UIButton *noAccountButton;
    IBOutlet UIButton *loginButton;
	IBOutlet UIActivityIndicatorView *loginIndicator;
    IBOutlet UILabel* connectionStatusLabel;
    // Controllers
    IBOutlet SelectionScreenViewController *selectionScreenViewController;
    IBOutlet SignupViewController *signupViewController;
    IBOutlet SettingsSmallViewController *settingsViewController;
    
    // Members
    PresenceObserver *presenceObserver;
    ConnectionObserver *connectionObserver;
    CloudFriendsWrapper *cf;
    UIAlertView *alert;
    UIAlertView *reconnectionalert;
    Boolean isShowingAlert;
    Boolean autologin;
    Boolean logoutclicked;
    Boolean loginclicked;
    NSMutableDictionary* dict;
}

@property (nonatomic, retain) UITextField *usernameField;
@property (nonatomic, retain) UITextField *passwordField;
@property (nonatomic, retain) UIButton *loginButton;
@property (nonatomic, retain) UILabel *usernameLabel;
@property (nonatomic, retain) UILabel *passwordLabel;
@property (nonatomic, retain) UIButton *forgotButton;
@property (nonatomic, retain) UIButton *noAccountButton;
@property (nonatomic, retain) UIActivityIndicatorView *loginIndicator;
@property (nonatomic, retain) SelectionScreenViewController *selectionScreenViewController;
@property (nonatomic, retain) SettingsSmallViewController *settingsViewController;
@property (nonatomic, retain) SignupViewController *signupViewController;
@property (nonatomic, retain) CloudFriendsWrapper *cf;
@property (nonatomic, retain) UIAlertView *alert;
@property (nonatomic, retain) UIAlertView *reconnectionalert;
@property (nonatomic, retain) UILabel* connectionStatusLabel;
-(void)LocalizeStrings;
-(IBAction) login: (id) sender;
-(IBAction) settings: (id) sender;
-(void)checkInternetConnection: (id) sender;
- (void)isPresent:(NSString*) strFrom;
- (void)isNotPresent:(NSString*) strFrom;
- (void)isconnected:(NSNumber*) err;


@end
