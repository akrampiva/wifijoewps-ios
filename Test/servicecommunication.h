#ifndef SERVICE_COMMUNICATION_H__
#define SERVICE_COMMUNICATION_H__

#include <list>
#include <string>
#include "communicationbase.h"
#include "corecommunication.h"
#include "servicepresencehandler.h"

class ServiceCommunicationEventHandler
{
    public:
        virtual void stopped(const std::string &topic) = 0;
};


class ServiceCommunication : public CommunicationBase, public CoreCommunication
{
    public:

        ServiceCommunication(void);
        ServiceCommunication(const std::string &topicName);
        virtual ~ServiceCommunication();

        void registerService(void);

        virtual void start(ServiceCommunicationEventHandler *sceh);
        virtual void stop(void);

        ServiceCommunicationEventHandler *lSceh;
    private:

};

#endif
