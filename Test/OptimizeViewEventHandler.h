//
//  OptimizeViewEventHandler.h
//  iopsys Wifi Joe
//
//  Created by Kurt Vermeersch on 14/04/14.
//
//

#ifndef __iopsys_Wifi_Joe__OptimizeViewEventHandler__
#define __iopsys_Wifi_Joe__OptimizeViewEventHandler__

#include <iostream>
#import "commandentry.h"
#import "OptimizeViewStatus.h"
#import "EventingProtocol.h"
#include <sstream> 

class OptimizeViewEventHandler: CommandEventHandler {
public:
    OptimizeViewStatus *wStatus;
    
    OptimizeViewEventHandler() {
    }
    
    void commandReply(const std::string &topic,
                      CommandEntry *cmd, const std::string & from) {
    }
    
    void commandError(const std::string &topic, CommandEntry *cmd,
                      const std::string & from, CommandEventHandler::ErrorType err) {
    }
    
    void commandEvent(const std::string  eventSubject, const std::string value){
        std::size_t found = eventSubject.find("Status");
        if (found!=std::string::npos){
            NSLog(@"Status found value=%s",value.c_str());
            if (value.compare("closed") == 0) {
                // Done testing, stream is closed
                NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                                selector:@selector(speedTestStopped)];
                [wStatus notifyObservers:inv];
            }
        }
        std::size_t foundtx = eventSubject.find("Tx");
        if (foundtx!=std::string::npos){
            printf("WifiSpeedEventHandler Event: %s value %s ", eventSubject.c_str(), value.c_str());
            if (value.compare("0") == 0) {
                /* Set the timer to this moment in time
                 * We want to measure the transfer speed, not the transfer time
                 * plus the setup time.
                 * The setup time is rather large per session
                 */
                NSLog(@"Initializing timer to calculate transfer speed");
                wStatus->lStart = time(0);
                wStatus->measuredDataFromEvents = 0;
                wStatus->measuredTimeFromEvents = 0;
                return;
            }
            if (value.compare("") != 0) {
                long seconds_since_start = (time(0) - wStatus->lStart)/(1000 * 1000 * 1000);
                long totalBytes;
                std::istringstream(value) >> totalBytes;
                if (seconds_since_start == 0) seconds_since_start = 1;
                wStatus->measuredSpeedFromEvents = (double)(totalBytes/(seconds_since_start));
                wStatus->measuredDataFromEvents = (int)totalBytes;
                wStatus->measuredTimeFromEvents = (int)seconds_since_start;
                printf("+++Event speed: bytes per second %f (%f MB/s)", wStatus->measuredSpeedFromEvents, wStatus->measuredSpeedFromEvents/(1024*1024));
                NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                                selector:@selector(optimizeGraphUpdate)];
                [wStatus notifyObservers:inv];
            }
            return;
        }
    }
};
    
#endif /* defined(__iopsys_Wifi_Joe__OptimizeViewEventHandler__) */
