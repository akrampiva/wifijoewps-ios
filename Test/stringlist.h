#ifndef __STRINGLIST_H__
#define __STRINGLIST_H__

#include <list>
#include <string>

typedef std::list<std::string> CoreStringList;
CoreStringList getEmptyCoreStringList(void);

#endif
