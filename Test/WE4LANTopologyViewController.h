//
//  WE4LANTopologyViewController.h
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"

@interface WE4LANTopologyViewController : UIViewController
{
    CloudFriendsWrapper *client;
    int nbExtenders;
    NSMutableArray* zone3Devices;
    NSMutableArray* zone4Devices;
}
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* initialIndicator;
-(id) initWithClient:(CloudFriendsWrapper*) cfwrap;
@end
