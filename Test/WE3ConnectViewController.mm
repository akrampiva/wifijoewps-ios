//
//  WE3ConnectViewController.m
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import "WE3ConnectViewController.h"
#import "InstallationType.h"
#import "deviceutils.h"
#import "colors.h"
#import "WE4LANTopologyViewController.h"

#define AVAILABLE_ALERT 53
#define NOT_AVAILABLE_ALERT 55
#define MAX_TIME 2
@interface WE3ConnectViewController ()

@end

@implementation WE3ConnectViewController

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap andInstalationType:(NSString*)type{
    self = [super init];
    if (self) {
        // Custom initialization
        client = cfwrap;
        instalationType=type;
    }
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if(IS_IPAD)
        {
            self = [super initWithNibName:[NSString stringWithFormat:@"%@-iPad",nibNameOrNil] bundle:nibBundleOrNil];
        }
        else{
            self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        }
    }
    return self;
}

- (id)initWithInstalationType:(NSString*)type
{
    self = [super init];
    if (self) {
        // Custom initialization
        instalationType=type;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(isIOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"connect_extender", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    CALayer *btnLayer = [_okButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    btnLayer = [_cancelButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    [_instructionsView.layer setCornerRadius:10.0f];
    if([instalationType isEqualToString:CABLED_INSTALATION])
    {
        _instruction1Label.text = NSLocalizedString(@"cabled_instructions_1", nil) ;
        _instruction2Label.text = NSLocalizedString(@"cabled_instructions_2", nil) ;
        _instruction3Label.text = NSLocalizedString(@"cabled_instructions_3", nil) ;
        _instruction4Label.text = NSLocalizedString(@"cabled_instructions_4", nil) ;
        
    }
    else if([instalationType isEqualToString:WIRELESS_INSTALATION])
    {
        _instruction1Label.text = NSLocalizedString(@"wireless_instructions_1", nil) ;
        _instruction2Label.text = NSLocalizedString(@"wireless_instructions_2", nil) ;
        _instruction3Label.text = NSLocalizedString(@"wireless_instructions_3", nil) ;
        _instruction4Label.text = NSLocalizedString(@"wireless_instructions_4", nil) ;
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initpProcess];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    processCanceled = YES;
}

-(void) initpProcess{
    [self setCurrentInstruction:1];
    extenderAvailable = NO;
    wpsStatus = "";
    processCanceled = NO;
    wirelessStepAlertViewlaunched = NO;
    tryToConnectAlertViewLaunched = NO;
    successAlertViewLaunched = NO;
    failAlertViewLaunched = NO;
    
}

-(void) cancel{
    if(autoTimer && [autoTimer isValid])
    {
        [autoTimer invalidate];
        autoTimer = nil;
    }
    processCanceled = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setCurrentInstruction:(int)nb{
    switch (nb) {
        case 1:
        {
            [_instruction1Next setHidden:NO];
            [_instruction2Next setHidden:YES];
            [_instruction3Next setHidden:YES];
            [_instruction4Next setHidden:YES];
            [_okButton setTitle:@"Next" forState:UIControlStateNormal];
            currentInstruction = 1;
            if([instalationType isEqualToString:CABLED_INSTALATION])
            {
                [_currentInstructionImage setImage:[UIImage imageNamed:@"wifi_ext_step_cable.png"]];
            }
            else if([instalationType isEqualToString:WIRELESS_INSTALATION])
            {
                [_currentInstructionImage setImage:[UIImage imageNamed:@"wifi_ext_step_power.png"]];
            }
        }
            break;
        case 2:
        {
            [_instruction1Next setHidden:YES];
            [_instruction2Next setHidden:NO];
            [_instruction3Next setHidden:YES];
            [_instruction4Next setHidden:YES];
            [_okButton setTitle:@"Next" forState:UIControlStateNormal];
            currentInstruction = 2;
            if([instalationType isEqualToString:CABLED_INSTALATION])
            {
                [_currentInstructionImage setImage:[UIImage imageNamed:@"wifi_ext_step_power.png"]];
            }
            else if([instalationType isEqualToString:WIRELESS_INSTALATION])
            {
                [_currentInstructionImage setImage:[UIImage imageNamed:@"wifi_ext_step_wifi_led.png"]];
            }
        }
            break;
        case 3:
        {
            [_instruction1Next setHidden:YES];
            [_instruction2Next setHidden:YES];
            [_instruction3Next setHidden:NO];
            [_instruction4Next setHidden:YES];
            [_okButton setTitle:@"OK" forState:UIControlStateNormal];
            currentInstruction = 3;
            if([instalationType isEqualToString:CABLED_INSTALATION])
            {
                [_currentInstructionImage setImage:[UIImage imageNamed:@"wifi_ext_step_wifi_led.png"]];
            }
            else if([instalationType isEqualToString:WIRELESS_INSTALATION])
            {
                [_currentInstructionImage setImage:[UIImage imageNamed:@"wifi_ext_step_wps.png"]];
            }
        }
            break;
        case 4:
        {
            [_instruction1Next setHidden:YES];
            [_instruction2Next setHidden:YES];
            [_instruction3Next setHidden:YES];
            [_instruction4Next setHidden:NO];
            currentInstruction = 4;
        }
            break;
            
        default:
            break;
    }
}
-(IBAction)okButtonPressed:(id)sender{
    NSLog(@"currentInstruction %d",currentInstruction);
    switch (currentInstruction) {
        case 1:
            [self setCurrentInstruction:2];
            break;
        case 2:
            [self setCurrentInstruction:3];
            break;
        case 3:
            [self setCurrentInstruction:4];
            [self start];
            break;
        default:
            break;
    }
}


-(void)start{
    if([instalationType isEqualToString:CABLED_INSTALATION])
    {
        [self launchTryToConnectAlert];
    }
    else if([instalationType isEqualToString:WIRELESS_INSTALATION])
    {
        [self launchWirelessStepAlert];
    }
}

-(void) step1{//Pairing
    
    [self setStepNumber:1 isLoading:YES isFinished:NO withError:NO];
    [self setStepNumber:2 isLoading:NO isFinished:NO withError:NO];
    [self setStepNumber:3 isLoading:NO isFinished:NO withError:NO];
    
    [self performSelector:@selector(step2) withObject:nil afterDelay:2.7];
}

-(void) step2{//configuring
    
    [self setStepNumber:1 isLoading:NO isFinished:YES withError:NO];
    [self setStepNumber:2 isLoading:YES isFinished:NO withError:NO];
    [self setStepNumber:3 isLoading:NO isFinished:NO withError:NO];
    [self performSelector:@selector(step3) withObject:nil afterDelay:2.7];
}

-(void) step3{//checkAvailibility
    [self setStepNumber:1 isLoading:NO isFinished:YES withError:NO];
    [self setStepNumber:2 isLoading:NO isFinished:YES withError:NO];
    [self setStepNumber:3 isLoading:YES isFinished:NO withError:NO];
    if(autoTimer && [autoTimer isValid])
    {
        [autoTimer invalidate];
        autoTimer = nil;
    }
    maxTime = 0;
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0)
                                                 target:self
                                               selector:@selector(getAvailableExtenders)
                                               userInfo:nil
                                                repeats:YES];
}

-(void)launchTryToConnectAlert{
    if(!tryToConnectAlertViewLaunched)
    {
        tryToConnectAlertViewLaunched = YES;
        CALayer *btnLayer;
        btnLayer = [_tryToConnectAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        [btnLayer setBorderWidth:2.0];
        [btnLayer setBorderColor:THEME_COLOR.CGColor];
        btnLayer.zPosition = 999;
        
        alertDisabledView = [[UIView alloc] initWithFrame:self.view.bounds];
        [alertDisabledView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        [self.view addSubview:alertDisabledView];
        [alertDisabledView addSubview:_tryToConnectAlertView];

        CGRect frame=_tryToConnectAlertView.frame;
        frame.origin.y = self.view.frame.size.height / 2 - (frame.size.height/2);
        frame.origin.x = self.view.frame.size.width / 2 - (frame.size.width/2);
        _tryToConnectAlertView.frame=frame;
        [_tryToConnectAlertIndicator startAnimating];
        [self step3];
        
    }
    
}
-(void)hideTryToConnectAlert{
    if(tryToConnectAlertViewLaunched)
    {
        [_tryToConnectAlertIndicator stopAnimating];
        tryToConnectAlertViewLaunched = NO;
        [_tryToConnectAlertView removeFromSuperview];
        if(alertDisabledView)
            [alertDisabledView removeFromSuperview];

    }
}

-(void)launchWirelessStepAlert{
    if(!wirelessStepAlertViewlaunched)
    {
        wirelessStepAlertViewlaunched = YES;
        CALayer *btnLayer;
        btnLayer = [_wirelessStepAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        [btnLayer setBorderWidth:2.0];
        [btnLayer setBorderColor:THEME_COLOR.CGColor];
        btnLayer.zPosition = 999;
        
        alertDisabledView = [[UIView alloc] initWithFrame:self.view.bounds];
        [alertDisabledView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        [self.view addSubview:alertDisabledView];
        [alertDisabledView addSubview:_wirelessStepAlertView ];

        CGRect frame=_wirelessStepAlertView.frame;
        frame.origin.y = self.view.frame.size.height / 2 - (frame.size.height/2);
        frame.origin.x = self.view.frame.size.width / 2 - (frame.size.width/2);
        _wirelessStepAlertView.frame=frame;
        
        [self step1];
        
    }
    
}
-(void)hideWirelessStepAlert{
    if(wirelessStepAlertViewlaunched)
    {
        wirelessStepAlertViewlaunched = NO;
        [_wirelessStepAlertView removeFromSuperview];
        if(alertDisabledView)
            [alertDisabledView removeFromSuperview];
    }
}

-(void)launchSuccessAlert{
    if(!successAlertViewLaunched)
    {
        successAlertViewLaunched = YES;
        CALayer *btnLayer = [_okButtonSuccessAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        btnLayer = [_successAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        [btnLayer setBorderWidth:2.0];
        [btnLayer setBorderColor:THEME_COLOR.CGColor];
        btnLayer.zPosition = 999;
        
        alertDisabledView = [[UIView alloc] initWithFrame:self.view.bounds];
        [alertDisabledView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        [self.view addSubview:alertDisabledView];
        [alertDisabledView addSubview:_successAlertView];

        CGRect frame=_successAlertView.frame;
        frame.origin.y = self.view.frame.size.height / 2 - (frame.size.height/2);
        frame.origin.x = self.view.frame.size.width / 2 - (frame.size.width/2);
        _successAlertView.frame=frame;
        
    }
    
}
-(void)hideSuccessAlert{
    if(successAlertViewLaunched)
    {
        successAlertViewLaunched = NO;
        [_successAlertView removeFromSuperview];
        if(alertDisabledView)
            [alertDisabledView removeFromSuperview];
    }
}

-(void)launchFailAlert{
    if(!failAlertViewLaunched)
    {
        failAlertViewLaunched = YES;
        CALayer *btnLayer = [_cancelButtonFailAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        btnLayer = [_restartButtonFailAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        btnLayer = [_failAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        [btnLayer setBorderWidth:2.0];
        [btnLayer setBorderColor:THEME_COLOR.CGColor];
        btnLayer.zPosition = 999;
        
        alertDisabledView = [[UIView alloc] initWithFrame:self.view.bounds];
        [alertDisabledView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        [self.view addSubview:alertDisabledView];
        [alertDisabledView addSubview:_failAlertView];

        CGRect frame=_failAlertView.frame;
        frame.origin.y = self.view.frame.size.height / 2 - (frame.size.height/2);
        frame.origin.x = self.view.frame.size.width / 2 - (frame.size.width/2);
        _failAlertView.frame=frame;
        
    }
    
}
-(void)hideFailAlert{
    if(failAlertViewLaunched)
    {
        failAlertViewLaunched = NO;
        [_failAlertView removeFromSuperview];
        if(alertDisabledView)
            [alertDisabledView removeFromSuperview];
    }
}

-(void)getAvailableExtenders{
    extenderAvailable = client->cf->isExtenderAvailable();
    wpsStatus = client->cf->getWPSStatus();
    NSLog(@"wps status %s",wpsStatus.c_str());
    maxTime = maxTime + 1;
    if((extenderAvailable && !wpsStatus.empty() && wpsStatus.compare("success")) || maxTime > MAX_TIME)
    {
        if(autoTimer && [autoTimer isValid])
        {
            [autoTimer invalidate];
            autoTimer = nil;
        }
        [self performSelector:@selector(finishing) withObject:nil afterDelay:0.7];
    }
}


-(void) finishing{
    [self setStepNumber:1 isLoading:NO isFinished:YES withError:NO];
    [self setStepNumber:2 isLoading:NO isFinished:YES withError:NO];
    [self setStepNumber:3 isLoading:NO isFinished:YES withError:!extenderAvailable];
    [self performSelector:@selector(showAvailablityStatus) withObject:nil afterDelay:0.7];
}

-(void) showAvailablityStatus{
    [self hideTryToConnectAlert];
    [self hideWirelessStepAlert];
    if(!extenderAvailable)
    {
        [self launchFailAlert];
    }
    else{
        [self launchSuccessAlert];
    }
}


-(IBAction)cancelPressed:(id)sender{
    [self cancel];
}
-(void)showLANTopology{
    WE4LANTopologyViewController* topologyViewC=[[WE4LANTopologyViewController alloc] initWithClient:client];
    [self.navigationController pushViewController:topologyViewC animated:YES];
    [topologyViewC release];
}
-(void)setStepNumber:(int)number isLoading:(BOOL)loading isFinished:(BOOL)finished withError:(BOOL)error{
    if(number==1)
    {
        if(loading)
        {
            [_step1Indicator setHidden:NO];
            [_step1Indicator startAnimating];
            [_step1CheckImage setHidden:YES];
        }
        else if(finished)
        {
            [_step1Indicator stopAnimating];
            [_step1Indicator setHidden:YES];
            [_step1CheckImage setHidden:NO];
            if(error)
                [_step1CheckImage setImage:[UIImage imageNamed:@"wifi_ext_warning_icon.png"]];
            else
                [_step1CheckImage setImage:[UIImage imageNamed:@"wifi_ext_file_check.png"]];
            
        }
        else {
            [_step1Indicator stopAnimating];
            [_step1Indicator setHidden:YES];
            [_step1CheckImage setHidden:YES];
        }
    }
    else if(number==2)
    {
        if(loading)
        {
            [_step2Indicator setHidden:NO];
            [_step2Indicator startAnimating];
            [_step2CheckImage setHidden:YES];
        }
        else if(finished)
        {
            [_step2Indicator stopAnimating];
            [_step2Indicator setHidden:YES];
            [_step2CheckImage setHidden:NO];
            if(error)
                [_step2CheckImage setImage:[UIImage imageNamed:@"wifi_ext_warning_icon.png"]];
            else
                [_step2CheckImage setImage:[UIImage imageNamed:@"wifi_ext_file_check.png"]];
        }
        else {
            [_step2Indicator stopAnimating];
            [_step2Indicator setHidden:YES];
            [_step2CheckImage setHidden:YES];
        }
    }
    else if(number==3)
    {
        if(loading)
        {
            [_step3Indicator setHidden:NO];
            [_step3Indicator startAnimating];
            [_step3CheckImage setHidden:YES];
        }
        else if(finished)
        {
            [_step3Indicator stopAnimating];
            [_step3Indicator setHidden:YES];
            [_step3CheckImage setHidden:NO];
            if(error)
                [_step3CheckImage setImage:[UIImage imageNamed:@"wifi_ext_warning_icon.png"]];
            else
                [_step3CheckImage setImage:[UIImage imageNamed:@"wifi_ext_file_check.png"]];
        }
        else {
            [_step3Indicator stopAnimating];
            [_step3Indicator setHidden:YES];
            [_step3CheckImage setHidden:YES];
        }
    }
    
}


- (IBAction)cancelButtonFailAlertViewPressed:(id)sender{
    [self hideFailAlert];
    [self cancel];
}

- (IBAction)restartButtonFailAlertViewPressed:(id)sender{
    [self hideFailAlert];
    [self initpProcess];
}

- (IBAction)okButtonSuccessAlertViewPressed:(id)sender{
    [self hideSuccessAlert];
    [self showLANTopology];
}



- (IBAction)back{
    [self cancel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
