//
//  colors.mm
//  Test
//
//  Created by Kurt Vermeersch on 22/11/13.
//
//

#include "colors.h"


/*
 <color name="theme_color">#FFe22e3d</color>
 R: 226 G: 46 B: 61
 */
UIColor* const themecolor = [[UIColor alloc] initWithRed:0.4 green:0.8 blue:0.2 alpha:1];

/*
 <color name="theme_color_gradient1">#FFd22e3d</color>
 R: 210 G: 46 B: 61
 */
UIColor* const themecolorgradient1 = [[UIColor alloc] initWithRed:0.824 green:0.18 blue:0.239 alpha:1];

/*
 <color name="theme_color_gradient2">#FFc02e3d</color>
 R: 192 G: 46 B: 61
 */
UIColor* const themecolorgradient2 = [[UIColor alloc] initWithRed:0.753 green:0.18 blue:0.239 alpha:1];

/*
 <color name="theme_color_light">#FFf24e5d</color>
 R: 242 G: 78 B: 93
 */
UIColor* const themecolorlight = [[UIColor alloc] initWithRed:0.949 green:0.306 blue:0.365 alpha:1];

/*
 <color name="theme_color_light_gradient1">#FFff8e9d</color>
 R: 255 G: 142 B: 157
 */
UIColor* const themecolorlightgradient1 = [[UIColor alloc] initWithRed:1 green:0.557 blue:0.616 alpha:1];

/*
 <color name="theme_color_light_gradient2">#FFf26e7d</color>
 R: 242 G: 110 B: 125
 */
UIColor* const themecolorlightgradient2 = [[UIColor alloc] initWithRed:0.949 green:0.431 blue:0.49 alpha:1];

UIColor* const themecolorverylight = [[UIColor alloc] initWithRed:0.859 green:0.898 blue:0.839 alpha:1]; /*#dbe5d6*/