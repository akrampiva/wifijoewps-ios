#ifndef CORE_COMMUNICATION_H__
#define CORE_COMMUNICATION_H__

#include <list>
#include <string>
#include "stringlist.h"
#include "communicationbase.h"
#include "commandentry.h"
#include "proxyhandlerbase.h"
#include "servicepresencehandler.h"


class CoreCommunication
{
    public:
        typedef std::list<CommunicationBase*> CmdBaseList_t;
        typedef CoreStringList ConnectList;
        typedef CoreStringList AccountList;
        typedef CoreStringList SessionIdList;

        CoreCommunication(void);
        virtual ~CoreCommunication();

        static const std::string engineVersion(void);
        void registerPresenceHandler(ServicePresenceHandler *sph);
        void unregisterPresenceHandler(ServicePresenceHandler *sph);
        bool isPresent(const std::string &accountID);
        /* Most likely not really needed can be handled using regular presence handler
        void registerLocalPresenceHandler(ServicePresenceHandler *sph);
        void unregisterLocalPresenceHandler(ServicePresenceHandler *sph);
        */
        void getLocalAccessList(const std::string &accountID, ConnectList &conList);
        void getPresenceList(AccountList &accList, CoreStringList &typeList, CoreStringList &localList);

        static std::string getLocalLogin(void);
        const CmdBaseList_t & getLocalCmdBaseList(void);
        const ParamEntry::valueList_t getRemoteTopicList(const std::string &dst);
        CommunicationBase* getRemoteTopicInfo(const std::string &dst, const std::string &topic);
        CmdBaseList_t getRemoteCommandList(const std::string &dst);

        /* the ceh command event handler has a limited life
         * it will only handle the reply comming from the command execution
         * the 'oeh' will handle the events which are a result of the execution
         * of the command
         */
        std::string sendCommand(const std::string &topic, CommandEntry * cEntry, 
                                const std::string &dst, CommandHandler * ceh,
                                CommandEventHandler *oeh = NULL);
        /* Send a command to an HTTP server,
         * The destination (dst) is of type: IPaddr:port
         */
        int sendHttpCmd(const std::string &topic, CommandEntry * cEntry, 
                        const std::string &dst, CommandHandler * ch);
        int addGetFile(const std::string &to, const std::string &srcFile, 
                        const std::string &dstFile, const std::string &sessionID);
        int addPutFile(const std::string &to, const std::string &srcFile, 
                        const std::string &dstFile, const std::string &sessionID);
        int createProxies(const std::string &dst, 
                            const int &proxies,
                            SessionIdList &sessionIdList,
                            const std::string &localConnectionType, 
                            const std::string &localConnectionAddress, 
                            const std::string &remoteConnectionType,
                            const std::string &remoteConnectionAddress,
                            ProxyEventHandler *pEH = NULL,
                            CommandEventHandler *oeh = NULL,
                            Observable *pObs = NULL);
        int closeTransferSession(const std::string &sessionID);
        int closeTransferSessions(const SessionIdList &sessionIdList);
        int connectToProxyAccept(const std::string &ipPortCombo);
        int createListeningSocket(const std::string &ipPortCombo);
        int acceptConnection(const int listeningSocket);
        void cancelOutstandingCmds(const std::string &dst);

    private:

};

#endif
