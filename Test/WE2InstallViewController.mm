//
//  WE2InstallViewController.m
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import "WE2InstallViewController.h"
#import "WE3ConnectViewController.h"
#import "ConnectionType.h"
#import "deviceutils.h"
#import "InstallationType.h"

@interface WE2InstallViewController ()

@end

@implementation WE2InstallViewController

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap andConnectionType:(NSString *)type{
    self = [super init];
    if (self) {
        // Custom initialization
        client = cfwrap;
        connectionType=type;
    }
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if(IS_IPAD)
        {
            self = [super initWithNibName:[NSString stringWithFormat:@"%@-iPad",nibNameOrNil] bundle:nibBundleOrNil];
        }
        else{
            self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(isIOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    CALayer *btnLayer = [_cabledButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    btnLayer = [_wirelessButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"connect_extender", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    
    [_cabledWirelessImage setHidden:NO];
    [_cabledImage setHidden:YES];
    
    [_cabledButton setHidden:NO];
    [_wirelessButton setHidden:NO];
    [_cabledOnlyButton setHidden:YES];
    
    if([connectionType isEqualToString:STRONG_CONNECTION])
    {
        _infoLabel.text = [NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"great_connexion", nil),NSLocalizedString(@"choose_if_you_want_to_connect", nil) ];
    }
    else if([connectionType isEqualToString:POOR_CONNECTION])
    {
        _infoLabel.text = NSLocalizedString(@"choose_if_you_want_to_connect", nil) ;
    }
    else
    {
        [_cabledWirelessImage setHidden:YES];
        [_cabledImage setHidden:NO];
        [_cabledButton setHidden:YES];
        [_wirelessButton setHidden:YES];
        [_cabledOnlyButton setHidden:NO];
        _infoLabel.text = NSLocalizedString(@"choose_if_you_still_want_to_connect", nil) ;
    }
    [_infoLabel.layer setCornerRadius:10.0f];
    [_infoLabel setBackgroundColor:[UIColor whiteColor]];
    [_infoLabel setNumberOfLines:0];
    [_infoLabel sizeToFit];
    
}

- (IBAction)back{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)cabledPressed:(id)sender{
    
    WE3ConnectViewController* installViewC=[[WE3ConnectViewController alloc] initWithClient:client andInstalationType:CABLED_INSTALATION];
    [self.navigationController pushViewController:installViewC animated:YES];
    [installViewC release];
    
}

-(IBAction)wirelessPressed:(id)sender{
    
    WE3ConnectViewController* installViewC=[[WE3ConnectViewController alloc] initWithClient:client andInstalationType:WIRELESS_INSTALATION];
    [self.navigationController pushViewController:installViewC animated:YES];
     [installViewC release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
