//
//  SpeechBalloonView.h
//  Test
//
//  Created by Kurt Vermeersch on 12/11/13.
//
//

#import <UIKit/UIKit.h>

@interface SpeechBalloonView : UIView {
}

- (void)drawRect:(CGRect)rect;
@end


