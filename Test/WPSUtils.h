//
//  WPSUtils.h
//  iopsys Wifi Joe WPS
//
//  Created by PIVA on 09/09/14.
//
//

#import <Foundation/Foundation.h>
#import "CloudFriendsWrapper.h"
#import "WPSViewController.h"

@interface WPSUtils : NSObject
{
    CloudFriendsWrapper* client;
    WPSViewController *delegate;

    class WPSStatusCommandEventHandler : public CommandEventHandler
    {
    public:
        UIViewController *delegate;
        
        WPSStatusCommandEventHandler(){
        }
        
        virtual ~WPSStatusCommandEventHandler(){
        }
        
        virtual void commandReply(const std::string &topic,
                                  CommandEntry *cmd, const std::string & from){
        }
        virtual void commandError(const std::string &topic, CommandEntry *cmd,
                                  const std::string & from, CommandHandler::ErrorType err){
        }
        virtual void commandEvent(const std::string  eventSubject, const std::string value){
            NSString* statusOrMac;
            if (value.empty()) return;
            if (eventSubject.empty()) return;
            NSLog(@"WPSStatusUpdateEvent event: %s value %s",eventSubject.c_str(),value.c_str());
            
            if (eventSubject.find("status") != std::string::npos)
            {
                statusOrMac = [NSString stringWithUTF8String:value.c_str()];
            }
            if (eventSubject.find("mac") != std::string::npos)
            {
                statusOrMac = [NSString stringWithUTF8String:value.c_str()];
                statusOrMac = [NSString stringWithFormat:@"MAC:%@",statusOrMac];
            }
            
             if (delegate && [delegate respondsToSelector:@selector(updateStatusOrMac:)]) {
                 NSLog(@"WPSStatusUpdateEvent performSelector");
                 [delegate performSelector:@selector(updateStatusOrMac:) withObject:statusOrMac];
             }
            
        }
        
    };
    WPSStatusCommandEventHandler *wpsEventHandler;
}

- (id) initWithClient:(CloudFriendsWrapper*) cfwrap andDelegate:(WPSViewController*)dgt;
- (void) launch;
- (void) stopRunning;
@end
