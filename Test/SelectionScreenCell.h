//
//  SelectionScreenCell.h
//  Test
//
//  Created by Kurt Vermeersch on 08/11/13.
//
//

#import <UIKit/UIKit.h>

@interface SelectionScreenCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *labelView;
@end

