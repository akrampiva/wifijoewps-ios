//
//  OptimizeViewStatus.mm
//  iopsys Wifi Joe
//
//  Created by Kurt Vermeersch on 14/04/14.
//
//

#include "OptimizeViewStatus.h"
#import "OptimizeViewEventHandler.h"
#import <sstream>


@implementation OptimizeViewStatus

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap andMaxSecondsToTest:(int)secs{
    self=[super init];
    self->client = cfwrap;
    secondsToTest = secs;
    gwJid = "";
    permanentSsid = "";
    permanentChannel = "";
    testChannel = "";
    optimizeGraphStatus = 0;
    currentGraphStatus = 0;
    stop = false;
    wifiIf = "";
    gwIsLocalAccessible = false;
    lTestOngoing = false;
    lStart = 0;
    measuredSpeedReceiving = 0.0;
    measuredSpeedSending = 0.0;
    measuredSpeedFromEvents = 0.0;
    measuredDataFromEvents = 0;
    measuredTimeFromEvents = 0;
    runner = 0;
    return self;
}

-(void) start {
    stop = false;
    //Save network ssid
    std::string bssid = client->cf->fetchCurrentNetworkKey();
    client->cf->setSavedNetworkKey(bssid);
    /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [self run];
    });*/
     [NSThread detachNewThreadSelector:@selector(run) toTarget:self withObject:nil];
    
}

-(void) setChannelToTest:(std::string) channel {
    testChannel = channel;
}

-(void) run{
    //CloudFriendsWrapper *cf=[CloudFriendsWrapper init];
    NSLog(@"optimizerun A");
    gwJid = client->cf->getGatewayJID();
    NSLog(@"optimizerun B");
    while (((gwJid.compare("") == 0))
           && (stop == false)) {
        // Still to figure out which device to contact
        gwJid = client->cf->getGatewayJID();
        NSLog(@"optimizerun B1");
        sleep(1);
    }
    NSLog(@"optimizerun C");
    if (stop) return;
    NSLog(@"optimizerun D");
    if (client->cf->isGatewayLocal() == false) {
        // Wrongly connected, send message to the user to reconnect properly
        NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                        selector:@selector(notLocalConnected)];
        [self notifyObservers:inv];
        NSLog(@"optimizerun D1");
        return;
    }
    NSLog(@"optimizerun E");
    std::string lGwJid = gwJid;
    
    /*
    WlSettings wifisettings=client->cf->getWireless();
    NSLog(@"optimizerun F");
    std::istringstream buffer(testChannel);
    int intchannel;
    buffer >> intchannel;
    NSLog(@"optimizerun G");
    client->cf->setWireless(WlSettings(wifisettings.ssid, intchannel));
    NSLog(@"optimizerun H");
    */
    
    //Retrieve the Wifi SSID, security and password
    //BUILD COMMAND
    CommandEntry *cmd=new CommandEntry("wifi", "");
    cmd->setInput("interface", wifiIf);
    cmd->setInput("channel", testChannel);
    
    BlockedCommandHandler* request = new BlockedCommandHandler();
    client->cf->cf->sendCommand("netMgr", cmd, lGwJid, request);
    BlockedCommandResult result = request->waitForResult();
    
    while ((stop == false) && (result.resultType != BlockedCommandResult::REPLY)) {
        NSLog(@"WifiStatus: Did not receive reply on setting channel");
        sleep(1);
        if (client->cf->isConnectedToServer() == false) {
            //if (client->cf->isTryingToReconnect() == false) {
                // We have stopped trying to connect, user intervention needed
                // We have stopped trying to connect, user intervention needed
                NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                                selector:@selector(notLocalConnected)];
                [self notifyObservers:inv];
                return;

            //}
            continue;
        }
        // Retry setting the wifi (should be set but can cause connection loss
        cmd->setInput("interface", wifiIf);
        cmd->setInput("channel", testChannel);
        
        request = new BlockedCommandHandler();
        //LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, lGwJid, request);
        client->cf->cf->sendCommand("netMgr", cmd, lGwJid, request);
        result = request->waitForResult();
    }
    std::string channel = result.cmd->getReply("channel");
    if (channel.compare(testChannel) != 0) {
        NSLog(@"Channel could not be set %s",channel.c_str());
    }
    
    
    
    // set the correct testing channel
    //Retrieve the Wifi SSID, security and password
    /*CommandEntry cmd = new CommandEntry("wifi", "");
    cmd.setInput("interface", wifiIf);
    cmd.setInput("channel", testChannel);
    
    
    BlockedCommandHandler request = new BlockedCommandHandler();
    LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, lGwJid, request);
    BlockedCommandResult result = request.waitForResult();
    
    while ((stop == false) && (result.getResultType() != BlockedCommandResult.REPLY)) {
        printf("WifiStatus: Did not receive reply on setting channel");
        sleep(1);
        if (cf->cf->isConnectedToServer() == false) {
            if (cf->cf->isTryingToReConnect() == false) {
                // We have stopped trying to connect, user intervention needed
                updateScreens("notConnectedLocal");
                return;
            }
            continue;
        }
        // Retry setting the wifi (should be set but can cause connection loss
        cmd.setInput("interface", wifiIf);
        cmd.setInput("channel", testChannel);
        
        request = new BlockedCommandHandler();
        LoginActivity.loginActivity.cf.sendCommand("netMgr", cmd, lGwJid, request);
        result = request.waitForResult();
    }
    String channel = result.getCmd().getReply("channel");
    */
    
    /*while ((stop == false) && client->cf->isConnectedToServer() == false) {
        sleep(1);
        if (client->cf->isConnectedToServer() == true && !client->cf->isGatewayLocal()) {
            NSLog(@"optimizerun H1");
            // We have stopped trying to connect, user intervention needed
            NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                            selector:@selector(notLocalConnected)];
            [self notifyObservers:inv];
            return;
        }
        NSLog(@"optimizerun H2");
        continue;
    }*/
    NSLog(@"optimizerun I");
    
    /*int channel=cf->cf->getPermanentChannel();
    if (itoa(channel).compare(testChannel) != 0) {
        printf("Channel could not be set %i",channel);
    }*/
    
    // Give it a second to let the change take effect if it did not yet
    sleep(1);
    NSLog(@"optimizerun J");
    if (stop) return;
    NSLog(@"optimizerun K");
    if (client->cf->isGatewayLocal() == false) {
        /*while ((stop == false)
               && (cf->cf->isGatewayLocal() == false)
               && (cf->cf->isTryingToReConnect() == true)) {
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            gwJid = cf->cf->getGatewayJid();
        }*/
        //if (client->cf->isGatewayLocal() == false) {
            NSLog(@"optimizerun K1");
            NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                            selector:@selector(notLocalConnected)];
            [self notifyObservers:inv];
            return;
        //}
    }
    NSLog(@"optimizerun L");
    if (stop) return;
    NSLog(@"optimizerun M");
    if (![self transmit:gwJid maxSeconds:secondsToTest maxBytes:100*1024*1024]) {
        // failed the test
        NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                        selector:@selector(failedTransmitTest)];
        [self notifyObservers:inv];
    }
}

-(void) determineConnection {
    NSLog(@"determineConnection A");
    gwJid = client->cf->getGatewayJID();
    NSLog(@"determineConnection B");
    wifiIf = client->cf->getWifiPermanentIf();
    NSLog(@"determineConnection C");
    permanentSsid = client->cf->getPermanentSsid();
    NSLog(@"determineConnection D PERMSSID = %s",permanentSsid.c_str());
    int ch = client->cf->getPermanentChannel();
    std::ostringstream s;
    s << ch;
    permanentChannel=s.str();
    NSLog(@"determineConnection E PERMCHANNEL = %s", permanentChannel.c_str());
    gwIsLocalAccessible = client->cf->isGatewayLocal();
    NSLog(@"determineConnection F");
    testChannel = permanentChannel;
    NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                    selector:@selector(connectionDetermined)];
    [self notifyObservers:inv];
    NSLog(@"determineConnection notified");
}

-(bool) transmit:(std::string) lDst maxSeconds: (long) lSeconds maxBytes: (long) lBytes {
    
    if (lTestOngoing) {
        printf("Test already ongoing, wait for it to complete\n");
        return false;
    }
    lTestOngoing = true;
    measuredSpeedSending = 0.0;
    measuredSpeedFromEvents = 0.0;
    /* Create a proxy where we locally accept a socket that can connect with
     * the created proxy tunnel. We use local address 127.0.0.1:5557 for this.
     * The other side will be setup with an element that discards the input
     * Events are requested which will show the progress from the other side
     */
    CoreCommunication cc;
    int port =5656;
    while (![self available:port] && port<65535){
        port++;
    }
    std::ostringstream ss;
    ss << "127.0.0.1:" << port;
    std::string loc(ss.str());
    
    std::list<std::string> lSessionIdList= std::list<std::string>();
    Observable *pObs = new Observable();
    OptimizeViewEventHandler* wifiSpeedEventHandler = new OptimizeViewEventHandler();
    wifiSpeedEventHandler->wStatus = self;
    int res = cc.createProxies(lDst, 1, lSessionIdList, "accept", loc,
                               "writeDiscard", "", NULL, (CommandEventHandler*)wifiSpeedEventHandler, pObs);
    if (res < 0) {
        printf("Failed to create the needed proxy tunnel\n");
        lTestOngoing = false;
        delete pObs;
        return false;
    }
    int sock = cc.connectToProxyAccept(loc);
    if (sock == -1) {
        printf("Failed to setup the connection\n");
        cc.closeTransferSessions(lSessionIdList);
        lTestOngoing = false;
        delete pObs;
        return false;
    }
    printf("Initializing timer to calculate transfer speed\n");
    lStart = time(0);
    /* Pump data for a while */
    ssize_t totalWrite = 0;
    ssize_t maxToWrite = lBytes;
    double totalTestTime = lSeconds;
    time_t start = time(0);
    double seconds_since_start = difftime( time(0), start);
    double secondLater = 1;
    bool ioError = false;
    while ((seconds_since_start < totalTestTime) && (totalWrite < maxToWrite)) {
        char buf[64*1024];
        int n = write(sock, buf, 64*1024);
        if (n <= 0) {
            ioError = true;
            printf("ERROR writing to socket\n");
            if(seconds_since_start == 0){
                seconds_since_start = 1;
            }
            break;
        }
        totalWrite += n;
        seconds_since_start = difftime( time(0), start);
        if (secondLater < seconds_since_start) {
            printf("Progress: %ld bytes\n", totalWrite);
            secondLater += 1;
        }
    }
    printf("\n=============================================\n");
    /* Calculate speed */
    printf("Bytes read %ld, over time %f (%fMB/s)\n",
           totalWrite, seconds_since_start,
           (totalWrite/(seconds_since_start * 1024 * 1024)));
    
    char buf[64];
    sprintf(buf, "%f", measuredSpeedSending);
    //bytesPerSecond = buf;
    /* Close connection */
    close(sock);
    /* Close transfer session */
    cc.closeTransferSessions(lSessionIdList);
    //delete pObs;
    lTestOngoing = false;
    if (ioError) { return false; }
    return true;
}


-(bool) available:(int) port{
    int sockfd;
    struct sockaddr_in serv_addr;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        // TODO: Error handling
        printf("Error creating socket");
        return false;
    }
    // connect the socket to the local LAN server
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    serv_addr.sin_port = htons(port);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(struct sockaddr)) < 0) {
        //syslog(LOG_ERR, "ERROR connecting to: %s:%d", ipAddr, port);
        printf("Port: %d available", port);
        close(sockfd);
        return true;
    }
    printf("Port: %d not available", port);
    close(sockfd);
    return false;
}

-(void) channelSet  {
    NSInvocation* inv = [NSInvocation invocationWithProtocol:@protocol(EventingProtocol)
                                                selector:@selector(channelSet)];
    [self notifyObservers:inv];
}

-(void) stopRunning  {
    if (stop) return;
    stop = true;
}

@end