#ifndef PARAM_ENTRY_H__
#define PARAM_ENTRY_H__

#include <list>
#include <string>
#include "stringlist.h"

class ParamEntry
{
    public:
        typedef CoreStringList valueList_t;
        enum paramType {
            paramTypeBool,
            paramTypeLong,
            paramTypeFloat,
            paramTypeString,
            paramTypeTextMulti
        };
        static paramType stringToType(std::string type);

        std::string paramName;
        std::string paramDescr;
        std::string value;
        paramType type;
        valueList_t valueList;
        bool isSet;
};

#endif

