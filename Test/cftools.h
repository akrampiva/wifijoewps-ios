#ifndef _TOOLS_H__
#define _TOOLS_H__

#include <string>

extern std::string cfToolsGetRealPath(const std::string &mountPath);
extern std::string cfToolsExecInSeparateProcess(const char* cmd);
extern std::string cfToolsGetUniqueSessionID(void);
extern void cfToolsStringToUpper(std::string &strToConvert);

#endif

