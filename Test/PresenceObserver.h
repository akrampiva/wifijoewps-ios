//
//  PresenceObserver.h
//  iopsys Wifi Joe
//
//  Created by Kurt Vermeersch on 30/03/14.
//
//

#ifndef iopsys_Wifi_Joe_PresenceObserver_h
#define iopsys_Wifi_Joe_PresenceObserver_h

class PresenceObserver : public ServicePresenceHandler{
public:
    LoginViewController *delegate;
    
    PresenceObserver(){
    }
    
    virtual ~PresenceObserver(){}
    
    void handlePresence(std::string from, State state){
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        if(state==Up){
            SEL ispresentselector = NSSelectorFromString(@"isPresent:");
            [NSThread detachNewThreadSelector:ispresentselector toTarget:delegate withObject:[[NSString alloc] initWithCString:from.c_str() encoding:NSUTF8StringEncoding]];
        } else {
            SEL isnotpresentselector = NSSelectorFromString(@"isNotPresent:");
            [NSThread detachNewThreadSelector:isnotpresentselector toTarget:delegate withObject:[[NSString alloc] initWithCString:from.c_str() encoding:NSUTF8StringEncoding]];
        }
        [pool release];
    }
    
};

#endif
