//
//  SettingsSmallViewController.h
//  Test
//
//  Created by Kurt Vermeersch on 22/01/14.
//
//

#ifndef Test_SettingsSmallViewController_h
#define Test_SettingsSmallViewController_h

//
#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"

@interface SettingsSmallViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>{
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UIImageView    *passwordFieldEye;
    IBOutlet UITextField *serverField;
    IBOutlet UILabel *versionLabel;
    IBOutlet UIView *masterView;
@public
    bool settingChanged;
    bool openA;
    CloudFriendsWrapper* client;
}

@property (nonatomic, retain) UITextField *usernameField;
@property (nonatomic, retain) UITextField *passwordField;
@property (nonatomic, retain) UIImageView *passwordFieldEye;
@property (nonatomic, retain) UITextField *serverField;
@property (nonatomic, retain) UILabel *versionLabel;
@property (nonatomic, retain) UIView *masterView;

- (IBAction)back;
- (IBAction) switchPassVisibility:(id)sender;

@end


#endif
