//
//  getmac.h
//  Test
//
//  Created by Kurt Vermeersch on 11/09/12.
//
//

#ifndef Test_getmac_h
#define Test_getmac_h

#include <sys/types.h>

#include <stdio.h>

#include <string.h>

#include <sys/socket.h>

#include <net/if_dl.h>

#include <ifaddrs.h>

#define IFT_ETHER 0x6

char*  getMacAddress(char* macAddress, const char* ifName);
                


#endif
