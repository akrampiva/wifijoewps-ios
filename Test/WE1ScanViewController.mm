//
//  WE1ScanViewController.m
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import "WE1ScanViewController.h"
#import "WE2InstallViewController.h"
#import "ConnectionType.h"
#import "deviceutils.h"
#import "colors.h"
#import "OptimizeViewStatus.h"

@interface WE1ScanViewController ()

@end

@implementation WE1ScanViewController

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap{
    self = [super init];
    if (self) {
        // Custom initialization
        client = cfwrap;
    }
    return self;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if(IS_IPAD)
        {
            self = [super initWithNibName:[NSString stringWithFormat:@"%@-iPad",nibNameOrNil] bundle:nibBundleOrNil];
        }
        else{
            self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(isIOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    CALayer *btnLayer = [_installButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    btnLayer = [_cancelButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"installer_wifi_extender", nil) ;
    _infoLabel.text = NSLocalizedString(@"walk_around", nil) ;
    [_infoLabel.layer setCornerRadius:10.0f];
    [_infoLabel setBackgroundColor:[UIColor whiteColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    signalViewArray = [[NSMutableArray alloc] initWithCapacity:50];
    
    [_signalContainerView setHidden:YES];
    [_initialIndicator setHidden:NO];
    [_initialIndicator startAnimating];
    helpAlertlaunched = NO;
    connection = NO_SIGNAL;
    [self launchHelpAlert];
}

-(void)launchHelpAlert{
    if(!helpAlertlaunched)
    {
        helpAlertlaunched = YES;
        CALayer *btnLayer = [_closeButtonAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        btnLayer = [_helpAlertView layer];
        [btnLayer setMasksToBounds:YES];
        [btnLayer setCornerRadius:10.0f];
        [btnLayer setBorderWidth:2.0];
        [btnLayer setBorderColor:THEME_COLOR.CGColor];
        btnLayer.zPosition = 999;
       
        [self.view addSubview:_helpAlertView];
        CGRect frame=_helpAlertView.frame;
        frame.origin.y = self.view.frame.size.height / 2 - (frame.size.height/2) - 49/2;
        frame.origin.x = self.view.frame.size.width / 2 - (frame.size.width/2);
        _helpAlertView.frame=frame;
    }
    
}
-(IBAction)hideHelpAlert:(id)sender{
    if(helpAlertlaunched)
    {
        helpAlertlaunched = NO;
        [_helpAlertView removeFromSuperview];
        
    }
}

- (IBAction)back{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)installPressed:(id)sender{
    if(![connection isEqualToString:NO_SIGNAL])
    {
        WE2InstallViewController* installViewC=[[WE2InstallViewController alloc] initWithClient:client andConnectionType:connection];
        [self.navigationController pushViewController:installViewC animated:YES];
        [installViewC release];

    }
    else{
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:@"" message:@"Waiting for signal ..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        
    }
}

-(IBAction)cancelPressed:(id)sender{
    [self back];
}

-(void)updateBlockViewWithProgress:(int) progress{
    // TODO Auto-generated method stub
    if(progressBlockView)
    {
        [progressBlockView removeFromSuperview];
        progressBlockView = nil;
    }
    float heightBloc = _signalView.frame.size.height/100 * progress;
    float yBlock = _signalView.frame.size.height - heightBloc;
    progressBlockView = [[UIImageView alloc] initWithFrame:CGRectMake(0, yBlock, _signalView.frame.size.width, heightBloc)];
    [_signalView addSubview:progressBlockView];
    
    float realYBlock = yBlock + _signalView.frame.origin.y;
    
    if (progress > 0) {
        if(progressIconView1)
        {
            [progressIconView1 removeFromSuperview];
            progressIconView1 = nil;
        }
        if(progressIconView2)
        {
            [progressIconView2 removeFromSuperview];
            progressIconView2 = nil;
        }
        float xIcon = _signalView.frame.origin.x + _signalView.frame.size.width + 2;
        float yIcon = realYBlock - 83/2;
        progressIconView1 = [[UIImageView alloc] initWithFrame:CGRectMake( xIcon , yIcon, 83, 83)];
        [_signalContainerView addSubview:progressIconView1];
    }
    if (progress == 0) {
        _signalLabel.text=NO_SIGNAL;
        _signalLabel.textColor=RED_COLOR;
        progressBlockView.backgroundColor=RED_COLOR;
        connection = NO_SIGNAL;
    } else if (progress < 16) // Bad
    {
        _signalLabel.text=BAD_CONNECTION;
        _signalLabel.textColor=RED_COLOR;
        progressBlockView.backgroundColor=RED_COLOR;
        connection = BAD_CONNECTION;
        connection = BAD_CONNECTION;
        [progressIconView1 setImage:[UIImage imageNamed:@"wifi_ext_bad_signal_img.png"]];
    } else if (progress >= 16 && progress < 26) // Poor
    {
        _signalLabel.text=POOR_CONNECTION;
        _signalLabel.textColor=YELLOW_COLOR;
        progressBlockView.backgroundColor=YELLOW_COLOR;
        connection = POOR_CONNECTION;
        connection = POOR_CONNECTION;
        if(progressIconView1)
        {
            [progressIconView1 removeFromSuperview];
            progressIconView1 = nil;
        }
        float xIcon = _signalView.frame.origin.x + _signalView.frame.size.width + 2;
        float yIcon = realYBlock - 83;
        progressIconView1 = [[UIImageView alloc] initWithFrame:CGRectMake( xIcon , yIcon, 83, 83)];
        [_signalContainerView addSubview:progressIconView1];
        
        if(progressIconView2)
        {
            [progressIconView2 removeFromSuperview];
            progressIconView2 = nil;
        }
        progressIconView2 = [[UIImageView alloc] initWithFrame:CGRectMake( xIcon , realYBlock, 83, 83)];
        [_signalContainerView addSubview:progressIconView2];
        
        [progressIconView1 setImage:[UIImage imageNamed:@"wifi_ext_poor_cabled_signal_img.png"]];
        [progressIconView2 setImage:[UIImage imageNamed:@"wifi_ext_poor_wireless_signal_img.png"]];
    } else // Strong
    {
        _signalLabel.text=STRONG_CONNECTION;
        _signalLabel.textColor=GREEN_COLOR;
        progressBlockView.backgroundColor=GREEN_COLOR;
        connection = STRONG_CONNECTION;
        connection = STRONG_CONNECTION;
        [progressIconView1 setImage:[UIImage imageNamed:@"wifi_ext_strong_signal_img.png"]];
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //start scanning
    closed=false;
    NSLog(@"double 01cc");
    [NSThread detachNewThreadSelector:@selector(lookUpDevices) toTarget:self withObject:nil];
    NSLog(@"double 02cc");
    
}

-(void)lookUpDevices{
    NSLog(@"double 03cc");
    
    while(!closed){
        // All the leases
        NSLog(@"double 04cc");
        double prg = client->cf->getWELeases(NULL);
        NSLog(@"double kkkk %f",prg);
        dispatch_async(dispatch_get_main_queue(), ^()
                       {
                           [self updateBlockViewWithProgress:prg];
                           [_signalContainerView setHidden:NO];
                           [_initialIndicator setHidden:YES];
                           [_initialIndicator stopAnimating];
                       });
        [NSThread sleepForTimeInterval:1.0];
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    // TODO Auto-generated method stub
    [super viewWillDisappear:animated];
    //stop scanning
    closed=true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
