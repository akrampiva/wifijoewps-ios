//
//  WPSViewController.h
//  iopsys Wifi Joe WPS
//
//  Created by PIVA on 15/09/2014.
//
//

#import <UIKit/UIKit.h>
@class WPSUtils;
@class CloudFriendsWrapper;
@interface WPSViewController : UIViewController
{
    CloudFriendsWrapper* client;
    WPSUtils* wpsStatus;
    BOOL wpsActivated;
    
}
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* searchBar;
@property (nonatomic,strong) IBOutlet UIImageView* resultImageView;
@property (nonatomic,strong) IBOutlet UILabel* infoText;
- (id) initWithClient:(CloudFriendsWrapper*) cfwrap;
- (void) updateStatusOrMac:(NSString*)statusOrMac;

@end
