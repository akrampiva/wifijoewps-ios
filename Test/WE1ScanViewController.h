//
//  WE1ScanViewController.h
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"
#import "MyDevicesViewController.h"


@interface WE1ScanViewController : UIViewController
{
    BOOL helpAlertlaunched;
    NSString* connection;
    
    
    UIImageView* progressBlockView;
    UIImageView* progressIconView1;
    UIImageView* progressIconView2;
    
    NSMutableArray *signalViewArray;
    
    bool open;
    bool closed;

    CloudFriendsWrapper *client;
    TableListMapType deviceMap;
    std::vector<TableListEntry> deviceList;
}
@property (nonatomic,strong) IBOutlet UIButton* installButton;
@property (nonatomic,strong) IBOutlet UIButton* cancelButton;
@property (nonatomic,strong) IBOutlet UILabel* infoLabel;

@property (nonatomic,strong) IBOutlet UILabel* signalLabel;
@property (nonatomic,strong) IBOutlet UIView* signalView;
@property (nonatomic,strong) IBOutlet UIView* signalContainerView;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* initialIndicator;


@property (nonatomic,strong) IBOutlet UIView* helpAlertView;
@property (nonatomic,strong) IBOutlet UIButton* closeButtonAlertView;

-(id) initWithClient:(CloudFriendsWrapper*) cfwrap;

@end
