//
//  SettingsViewController.h
//
//  Settings functionality for the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"

@interface SettingsViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>{
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UIImageView    *passwordFieldEye;
    IBOutlet UITextField *serverField;
    IBOutlet UITextField *guestDelayField;
    IBOutlet UITextField *guestSsidField;
    IBOutlet UISwitch    *guestStatusSwitch;
    IBOutlet UITextField *deviceSsidField;
    IBOutlet UITextField *devicePasswordField;
    IBOutlet UIImageView    *devicePasswordFieldEye;
    IBOutlet UIView *masterView;
    IBOutlet UILabel *versionLabel;
    @public
    bool openA;
    bool openB;
    bool settingChanged;
    bool guestSettingChanged;
    bool wifiSettingChanged;
    CloudFriendsWrapper* client;
}

@property (nonatomic, retain) UITextField *usernameField;
@property (nonatomic, retain) UITextField *passwordField;
@property (nonatomic, retain) UIImageView *passwordFieldEye;
@property (nonatomic, retain) UITextField *serverField;
@property (nonatomic, retain) UITextField *guestDelayField;
@property (nonatomic, retain) UILabel *versionLabel;
@property (nonatomic, retain) UITextField *guestSsidField;
@property (nonatomic, retain) UISwitch *guestStatusSwitch;
@property (nonatomic, retain) UITextField *deviceSsidField;
@property (nonatomic, retain) UITextField *devicePasswordField;
@property (nonatomic, retain) UIImageView *devicePasswordFieldEye;
@property (nonatomic, retain) UIView *masterView;
- (IBAction)back;
- (IBAction)switchGuestState;
-(IBAction) switchPassVisibility:(id)sender;
-(IBAction) switchDevicePassVisibility:(id)sender;

@end
