//
//  SettingsViewController.mm
//
//  Settings functionality for the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "SettingsViewController.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#import "deviceutils.h"
#import "cloudfriendsAppDelegate.h"

@implementation SettingsViewController

@synthesize versionLabel;
@synthesize usernameField;
@synthesize passwordField;
@synthesize passwordFieldEye;
@synthesize serverField;
@synthesize guestDelayField;
@synthesize guestSsidField;
@synthesize guestStatusSwitch;
@synthesize deviceSsidField;
@synthesize devicePasswordField;
@synthesize devicePasswordFieldEye;
@synthesize masterView;

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{       
    if(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad){
        const int movementDistance = 130; 
        float movementDuration = 0.3f; 
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void)viewDidLoad
{
    openA=0;
    openB=0;
    settingChanged=FALSE;
    guestSettingChanged=FALSE;
    wifiSettingChanged=FALSE;
   [super viewDidLoad];
    [usernameField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [passwordField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [serverField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [guestDelayField addTarget:self action:@selector(textFieldGuestDidChange:) forControlEvents:UIControlEventEditingChanged];
    [guestSsidField addTarget:self action:@selector(textFieldGuestDidChange:) forControlEvents:UIControlEventEditingChanged];
    [deviceSsidField addTarget:self action:@selector(textFieldWifiDidChange:) forControlEvents:UIControlEventEditingChanged];
    [devicePasswordField addTarget:self action:@selector(textFieldWifiDidChange:) forControlEvents:UIControlEventEditingChanged];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    settingChanged = YES;
}

-(void)textFieldWifiDidChange :(UITextField *)theTextField{
    settingChanged = YES;
    wifiSettingChanged=YES;
}

-(void)textFieldGuestDidChange :(UITextField *)theTextField{
    settingChanged = YES;
    guestSettingChanged=YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    appdelegate.settingIsShowing = NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    cloudfriendsAppDelegate* appdelegate=[[UIApplication sharedApplication] delegate];
    appdelegate.settingIsShowing = YES;
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    usernameField.layer.borderColor=themecolor.CGColor;
    [usernameField.layer setCornerRadius:10.0f];
    usernameField.borderStyle = UITextBorderStyleRoundedRect;
    usernameField.layer.borderWidth=1.0;
    passwordField.layer.borderColor=themecolor.CGColor;
    [passwordField.layer setCornerRadius:10.0f];
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    passwordField.layer.borderWidth=1.0;
    passwordField.secureTextEntry = YES;
    serverField.layer.borderColor=themecolor.CGColor;
    [serverField.layer setCornerRadius:10.0f];
    serverField.borderStyle = UITextBorderStyleRoundedRect;
    serverField.layer.borderWidth=1.0;
    serverField.secureTextEntry = FALSE;
    guestDelayField.layer.borderColor=themecolor.CGColor;
    [guestDelayField.layer setCornerRadius:10.0f];
    guestDelayField.borderStyle = UITextBorderStyleRoundedRect;
    guestDelayField.layer.borderWidth=1.0;
    [guestDelayField setKeyboardType:UIKeyboardTypeNumberPad];
    guestSsidField.layer.borderColor=themecolor.CGColor;
    [guestSsidField.layer setCornerRadius:10.0f];
    guestSsidField.borderStyle = UITextBorderStyleRoundedRect;
    guestSsidField.layer.borderWidth=1.0;
    deviceSsidField.layer.borderColor=themecolor.CGColor;
    [deviceSsidField.layer setCornerRadius:10.0f];
    deviceSsidField.borderStyle = UITextBorderStyleRoundedRect;
    deviceSsidField.layer.borderWidth=1.0;
    devicePasswordField.layer.borderColor=themecolor.CGColor;
    [devicePasswordField.layer setCornerRadius:10.0f];
    devicePasswordField.borderStyle = UITextBorderStyleRoundedRect;
    devicePasswordField.layer.borderWidth=1.0;
    devicePasswordField.secureTextEntry = YES;
    [super viewWillAppear:animated];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"SettingsTitle", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
      [[self.navigationController navigationBar] setHidden:NO];
    [guestSsidField resignFirstResponder];
    guestSsidField.delegate=self;
    NSString* un=[[NSUserDefaults standardUserDefaults] stringForKey:@"name_preference"];
    if(un!=nil){
        usernameField.text=un;
    }
    NSString* pw=[[NSUserDefaults standardUserDefaults] stringForKey:@"password_preference"];
    if(pw!=nil){
        passwordField.text=pw;
    }
    NSString* srv=[[NSUserDefaults standardUserDefaults] stringForKey:@"server_preference"];
    if(srv!=nil){
        serverField.text=srv;
    }
    NSString* version =[[NSString alloc] initWithFormat:@"Version %@",  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    versionLabel.text=version;
    [NSThread detachNewThreadSelector:@selector(loadwifisettings) toTarget:self withObject:nil];
    [NSThread detachNewThreadSelector:@selector(loadguestsettings) toTarget:self withObject:nil];
}

-(void)loadwifisettings{
    try {
        WlSettings wlsettings = client->cf->getWireless();
        NSString *ssid = [NSString stringWithCString:wlsettings.ssid.c_str() encoding:[NSString defaultCStringEncoding]];
        NSString *pass = [NSString stringWithCString:wlsettings.key.c_str() encoding:[NSString defaultCStringEncoding]];
        [devicePasswordField performSelectorOnMainThread:@selector(setText:) withObject:pass waitUntilDone:TRUE];
        [deviceSsidField performSelectorOnMainThread:@selector(setText:) withObject:ssid waitUntilDone:TRUE];
    } catch (std::exception &e) {
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:[NSString stringWithCString:e.what() encoding:NSUTF8StringEncoding] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    } catch (...){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:NSLocalizedString(@"FatalErrorDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    }
}

-(void)loadguestsettings{
    try {
        GuestIface guestSettings = client->cf->getGuestnet();
        if(guestSettings.status==UP){
            [guestStatusSwitch setOn:TRUE];
        } else {
            [guestStatusSwitch setOn:FALSE];
        }
        NSString* delay= [NSString stringWithFormat:@"%d", guestSettings.timeout];
        if(delay!=NULL){
            [guestDelayField performSelectorOnMainThread:@selector(setText:) withObject:delay waitUntilDone:TRUE];
        }
        NSString* ssid= [NSString stringWithCString:guestSettings.ssid.c_str() encoding:[NSString defaultCStringEncoding]];
        if(ssid!=NULL){
            [guestSsidField performSelectorOnMainThread:@selector(setText:) withObject:ssid waitUntilDone:TRUE];
        }
    } catch (std::exception &e) {
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:[NSString stringWithCString:e.what() encoding:NSUTF8StringEncoding] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    } catch (...){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:NSLocalizedString(@"FatalErrorDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    }
}

- (IBAction)back{
    if(settingChanged)
        [self showActionSheet];
    else
        [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)showActionSheet
{
    
    UIActionSheet *actionSheet;
    NSString* deleteMessage = [NSString stringWithFormat:@"Do you want to apply the new settings?"];
    actionSheet = [[UIActionSheet alloc] initWithTitle:deleteMessage
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"NO",@"")
                                destructiveButtonTitle:NSLocalizedString(@"YES",@"")
                                     otherButtonTitles:nil];
    
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    if(IS_IPAD)
    {
        [actionSheet addButtonWithTitle:@"NO"];
    }
	[actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - Action sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
        [self saveSettings];
        
	} else if (buttonIndex==1) {
        
    }
    [self.navigationController popViewControllerAnimated:YES];
    [actionSheet release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/*-(void) setssid{
    // Give warning message
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SettingsGuestDisconnectInfo", nil) message:NSLocalizedString(@"SettingsGuestDisconnectDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
    [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
     client->cf->setGuestnet(GuestIface([guestSsidField.text UTF8String], NOCHANGE, NOCHANGE));
}

-(void) settimeout{
    client->cf->setGuestnet(GuestIface("", NOCHANGE, guestDelayField.text.intValue));
}*/

-(IBAction) switchGuestState{
    settingChanged=TRUE;
    guestSettingChanged=TRUE;
    /*bool state = [guestStatusSwitch isOn];
    if(state == true){
        client->cf->setGuestnet(GuestIface([guestSsidField.text UTF8String], UP, NOCHANGE));
    } else {
        client->cf->setGuestnet(GuestIface("", DOWN, NOCHANGE));
    }*/
}

-(IBAction) switchPassVisibility:(id)sender{
    UIImage *btnImage1;
    if (openA == 0){
        openA = 1;
        btnImage1 = [UIImage imageNamed:@"permanent_access_eye_closed.png"];
        passwordField.secureTextEntry = FALSE;
    }
    else{
        openA = 0;
        btnImage1 = [UIImage imageNamed:@"permanent_access_eye_open.png"];
        passwordField.secureTextEntry = TRUE;
    }
    [passwordFieldEye setImage:btnImage1];
}

-(IBAction) switchDevicePassVisibility:(id)sender{
    UIImage *btnImage1;
    if (openB == 0){
        openB = 1;
        btnImage1 = [UIImage imageNamed:@"permanent_access_eye_closed.png"];
        devicePasswordField.secureTextEntry = FALSE;
    }
    else{
        openB = 0;
        btnImage1 = [UIImage imageNamed:@"permanent_access_eye_open.png"];
        devicePasswordField.secureTextEntry = TRUE;
    }
    [devicePasswordFieldEye setImage:btnImage1];
}

-(IBAction)textFieldDoneEditing:(id)sender{
    // Save the changed settings
    
    /*NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if(sender==usernameField){
        [prefs setObject:usernameField.text forKey:@"name_preference"];
    } else if(sender==passwordField){
        [prefs setObject:passwordField.text forKey:@"password_preference"];
    } else if(sender==serverField){
        [prefs setObject:serverField.text forKey:@"server_preference"];
    } else if(sender==guestDelayField){
        [prefs setObject:guestDelayField.text forKey:@"delayed_preference"];
        [NSThread detachNewThreadSelector:@selector(settimeout) toTarget:self withObject:nil];
    } else if(sender==guestSsidField){
        [prefs setObject:guestSsidField.text forKey:@"ssid_preference"];
        [NSThread detachNewThreadSelector:@selector(setssid) toTarget:self withObject:nil];
    }*/
}

-(id)saveSettings{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:usernameField.text forKey:@"name_preference"];
    [prefs setObject:passwordField.text forKey:@"password_preference"];
    [prefs setObject:serverField.text forKey:@"server_preference"];
    [prefs setObject:guestDelayField.text forKey:@"delayed_preference"];
    //[NSThread detachNewThreadSelector:@selector(settimeout) toTarget:self withObject:nil];
    [prefs setObject:guestSsidField.text forKey:@"ssid_preference"];
    [prefs setObject:deviceSsidField.text forKey:@"ssiddev_preference"];
    [prefs setObject:devicePasswordField.text forKey:@"passdev_preference"];
    
    [prefs synchronize];
    // Give warning message
    if(guestSettingChanged && !wifiSettingChanged){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SettingsGuestDisconnectInfo", nil) message:NSLocalizedString(@"SettingsGuestDisconnectDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
    } else if(wifiSettingChanged){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SettingsWifiDisconnectInfo", nil) message:NSLocalizedString(@"SettingsWifiDisconnectDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
    }
    //[NSThread detachNewThreadSelector:@selector(setssid) toTarget:self withObject:nil];
    if(guestSettingChanged){
        bool state = [guestStatusSwitch isOn];
        if(state == true){
            client->cf->setGuestnet(GuestIface([guestSsidField.text UTF8String], UP, guestDelayField.text.intValue));
        } else {
            client->cf->setGuestnet(GuestIface([guestSsidField.text UTF8String], DOWN, guestDelayField.text.intValue));
        }
    }
    if(wifiSettingChanged){
        client->cf->setWireless(WlSettings([deviceSsidField.text UTF8String],[devicePasswordField.text UTF8String],0));
    }
    return self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
