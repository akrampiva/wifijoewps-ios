//
//  OptimizeViewController.h
//
//  Optimize the channel of the wireless network functionality for the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"
//#import "speedtest.h"
#import "OptimizeViewStatus.h"
//#import "OptimizeViewEventHandler.h"
#import "EventingProtocol.h"
#import "OptimizeViewSetChannel.h"

@class OptimizeViewStatus;

@interface OptimizeViewController : UIViewController<EventingProtocol>{
    UIImageView* bestBlockView;
    IBOutlet UIImageView *backgroundimage;
    IBOutlet UILabel *textBoxLabel;
    IBOutlet UIButton *optimizeButton;
    IBOutlet UIButton *extendButton;
    IBOutlet UIView *currentView;
    IBOutlet UIView *optimizingView;
    IBOutlet UILabel *currentLabel;
    IBOutlet UILabel *optimizingLabel;
    IBOutlet UIImageView *optimizeImage;
    UIAlertView *connAlert;
    UIAlertView *notlocalAlert;
    int currentmax;
    int marge;
    int blocksize;
    int stage;
    @protected
        OptimizeViewStatus *oStatus;
        bool runCurrentScan;
        bool runOptimizeScan;
        std::string currentChannel;
        std::string bestChannel;
        int bestDataSent;
        int currentDataSent;
        int currentTimeSent;
        double currentSpeed;
        int maxTestTime;
        int currentSpeedTestProgress;
        OptimizeViewSetChannel* wifiOptimizeSetChannel;
        NSMutableArray *currentViewArray;
        NSMutableArray *optimizeViewArray;
    int bestblocknr;
        /*int stage;
        int bestResultBytes;
        int bestResultChannel;
        int bestBytes;
        int currentResultBytes;
        int currentBytes;
        bool cancel;
        int blocksize;
        int nrchannels;*/
    @public
        CloudFriendsWrapper* client;
        //SpeedTest *speedTest;
        //bool speedTestDone;
        //std::string resultBytesPerSecond;
}

@property (nonatomic, retain) UIImageView *backgroundimage;
@property (nonatomic, retain) UILabel *textBoxLabel;
@property (nonatomic, retain) IBOutlet UIButton *optimizeButton;
@property (nonatomic, retain) IBOutlet UIButton *extendButton;
@property (nonatomic, retain) UIView *currentView;
@property (nonatomic, retain) UIView *optimizingView;
@property (nonatomic, retain) UILabel *currentLabel;
@property (nonatomic, retain) UILabel *optimizingLabel;
@property (nonatomic, retain) UIAlertView *connAlert;
@property (nonatomic, retain) UIAlertView *notlocalAlert;
@property (nonatomic, retain) UIImageView *optimizeImage;
- (IBAction)back;

- (void)speedTestStopped;
- (void)optimizeGraphUpdate;
- (void)channelSet;
- (void)notLocalConnected;
- (void)connectionDetermined;


@end
