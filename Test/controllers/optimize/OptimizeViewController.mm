//
//  OptimizeViewController.mm
//
//  Optimize the channel of the wireless network functionality for the application
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "OptimizeViewController.h"
#import "WE1ScanViewController.h"
#import "deviceutils.h"
#include <sstream>

@implementation OptimizeViewController

@synthesize backgroundimage;
@synthesize textBoxLabel;
@synthesize optimizeButton;
@synthesize extendButton;
@synthesize currentView;
@synthesize optimizingView;
@synthesize currentLabel;
@synthesize optimizingLabel;
@synthesize connAlert;
@synthesize notlocalAlert;
@synthesize optimizeImage;

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void) paintBestResult{
    if(bestBlockView!=NULL){
        [self.view performSelectorOnMainThread:@selector(addSubview:) withObject:bestBlockView waitUntilDone:YES];
    }
    UIColor *lightthemecolor = [UIColor colorWithRed:0.592 green:0.992 blue:0.392 alpha:1]; /*#97fd64*/
    CGRect newRect=[self.view convertRect:optimizingView.frame toView:nil];
    bestBlockView = [[[UIImageView alloc] initWithFrame:CGRectMake(newRect.origin.x +1, newRect.origin.y+newRect.size.height-((bestblocknr)*blocksize) - marge, newRect.size.width-2,((bestblocknr)*blocksize))] autorelease];
    bestBlockView.backgroundColor=lightthemecolor;
    [self.view performSelectorOnMainThread:@selector(addSubview:) withObject:bestBlockView waitUntilDone:YES];
}

- (void)failedTransmitTest{
        /*if (runCurrentScan) {
            for(UIImageView * myView in currentViewArray) {
                [myView performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:YES];
            }
            [currentViewArray removeAllObjects];
            currentViewArray = [[NSMutableArray alloc] initWithCapacity:50];
        }
        if(runOptimizeScan) {
            for(UIImageView * myView in optimizeViewArray) {
                [myView performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:YES];
            }
            [optimizeViewArray removeAllObjects];
            optimizeViewArray = [[NSMutableArray alloc] initWithCapacity:50];
        }
        [oStatus start];*/
}

- (void)speedTestStopped{
    NSLog(@"optimize-speedTestStopped");
     if (runCurrentScan) {
          //NSLog(@"optimize-runCurrentScan A");
         runCurrentScan = false;
         currentChannel = oStatus->permanentChannel;
         bestChannel = oStatus->permanentChannel;
         bestDataSent = oStatus->measuredDataFromEvents;
         //NSLog(@"optimize-runCurrentScan B");
         currentSpeed = oStatus->measuredSpeedFromEvents;
         currentDataSent = oStatus->measuredDataFromEvents;
         currentTimeSent = oStatus->measuredTimeFromEvents;
         runOptimizeScan = true;
         int progressMax = (currentDataSent/75) * 100;
         //NSLog(@"optimize-runCurrentScan C");
         //printf("Optimize setting current max to %i",progressMax);
         currentmax = progressMax;
         //progressBar.setMax(progressMax);
         //progressBar.setProgress(0);
         //[optimizingView setProgress:0.0];
         //NSLog(@"optimize-runCurrentScan D");
         //progressBar.setSecondaryProgress(0);
         if (currentChannel.compare("1") == 0) {
             //NSLog(@"optimize-runCurrentScan E");
             [oStatus setChannelToTest:"2"];
         }else{
             //NSLog(@"optimize-runCurrentScan F");
             [oStatus setChannelToTest:"1"];
         }
         //NSLog(@"optimize-runCurrentScan G");
          sleep(2);
         [self paintBestResult];
         NSString* optimizeStr=NSLocalizedString(@"OptimizeOptimizedDesc", nil);
         //NSLog(@"optimize-runCurrentScan H");
         NSString *boxLabelText=[optimizeStr stringByAppendingString:[NSString stringWithFormat:@"[%i/%i]",1,12]];
         //NSLog(@"optimize-runCurrentScan I");
         [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:boxLabelText waitUntilDone:YES];
         //NSLog(@"optimize-runCurrentScan J");
         [oStatus start];
         //NSLog(@"optimize-runCurrentScan K");
         NSLog(@"RESULTOPT optimize-runCurrentScan currentchannel=%s currentMax=%i currentDataSent=%i",currentChannel.c_str(), currentmax,currentDataSent);
     }
    if (runOptimizeScan) {
        //NSLog(@"optimize-runOptimizeScan A");
        /*if (progressBar.getProgress() > progressBar.getSecondaryProgress()) {
            progressBar.setSecondaryProgress(progressBar.getProgress());
            printf("Optimize setting optimize secondary to %i", progressBar.getProgress());
        }*/
        //[optimizingView setProgress:0.0];
        if (oStatus->measuredDataFromEvents > bestDataSent) {
            NSLog(@"optimize-runOptimizeScan BETTER RESULT bestDataSent=%i measured=%i",bestDataSent,oStatus->measuredDataFromEvents);
            bestChannel = oStatus->testChannel;
            bestDataSent = oStatus->measuredDataFromEvents;
            [self paintBestResult];
        } else {
            NSLog(@"optimize-runOptimizeScan NO better result bestDataSent=%i measured=%i",bestDataSent,oStatus->measuredDataFromEvents);
        }
        //NSLog(@"optimize-runOptimizeScan B");
        int channel = std::stoi(oStatus->testChannel);
        channel += 1;
        
        if (currentChannel.compare(oStatus->testChannel)==0) {
            channel += 1;
        }
        NSLog(@"optimize-runOptimizeScan next testing channel = %i (tested channel = %s) (original channel = %s)",channel, oStatus->testChannel.c_str(),currentChannel.c_str());
        
        //NSLog(@"optimize-runOptimizeScan C");
        if (channel >= 13) {
            //NSLog(@"optimize-runOptimizeScan D");
            // Test completed
            stage=3;
            for(UIImageView * myView in optimizeViewArray) {
                [myView performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:YES];
            }
            [optimizeViewArray removeAllObjects];
            optimizeViewArray = [[NSMutableArray alloc] initWithCapacity:50];
            //Show results
            if(currentChannel.compare(bestChannel) != 0){
                [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:NSLocalizedString(@"OptimizeApplySettingsDesc", nil) waitUntilDone:NO];
                [self performSelectorOnMainThread:@selector(setOptimizeButtonTitle:) withObject:NSLocalizedString(@"OptimizeApplySettingsTitle", nil) waitUntilDone:YES];
            } else {
                [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:NSLocalizedString(@"OptimizeCurrentSettingsBestDesc", nil) waitUntilDone:YES];
                [self performSelectorOnMainThread:@selector(setOptimizeButtonHidden:) withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
                
            }
        }else{
            //NSLog(@"optimize-runOptimizeScan E");
            // Continue test with new channel
            std::stringstream ss;
            ss << channel;
            std::string testChannel = ss.str();
            [oStatus setChannelToTest:testChannel];
            //NSLog(@"optimize-runOptimizeScan F");
            NSString* optimizeStr=NSLocalizedString(@"OptimizeOptimizedDesc", nil);
            NSString *boxLabelText=[optimizeStr stringByAppendingString:[NSString stringWithFormat:@"[%i/%i]",channel,12]];
            [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:boxLabelText waitUntilDone:YES];
            //NSLog(@"optimize-runOptimizeScan G");
            sleep(2);
            //NSLog(@"optimize-runOptimizeScan H");
            for(UIImageView * myView in optimizeViewArray) {
                [myView performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:YES];
            }
            [optimizeViewArray removeAllObjects];
            optimizeViewArray = [[NSMutableArray alloc] initWithCapacity:50];
            [oStatus start];
        }
        NSLog(@"RESULTOPT optimize-runOptimizeScan bestChannel=%s bestDataSent=%i",bestChannel.c_str(),bestDataSent);
    }
}

- (void)growCurrentPerformanceTo:(NSNumber*) blocknr{
    if([blocknr intValue] > bestblocknr){
        bestblocknr = [blocknr intValue];
    }
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1];
    int already = [currentViewArray count];
    CGRect newRect=[self.view convertRect:currentView.frame toView:nil];
    for (int j = 0+already; j < [blocknr intValue]; j++)
    {
        UIImageView* blockView = [[[UIImageView alloc] initWithFrame:CGRectMake(newRect.origin.x +1, newRect.origin.y+newRect.size.height-((j+1)*blocksize) - marge, newRect.size.width-2,blocksize)] autorelease];
        blockView.backgroundColor=themecolor;
        [currentViewArray addObject: blockView];
        //NSLog(@"addblocktocurrent");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view addSubview:blockView];
        });
        [NSThread sleepForTimeInterval:0.1];
    }
}

- (void)growPerformanceTo:(NSNumber*) blocknr{
    if([blocknr intValue] > bestblocknr){
        bestblocknr = [blocknr intValue];
    }
    NSLog(@"growPerformanceTo %i from %i (count = %i)",[blocknr intValue],[optimizeViewArray count],[optimizeViewArray count]  );
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1];
    int already = [optimizeViewArray count];
    CGRect newRect=[self.view convertRect:optimizingView.frame toView:nil];
    for (int j = 0+already; j < [blocknr intValue]; j++)
    {
        UIImageView* blockView = [[[UIImageView alloc] initWithFrame:CGRectMake(newRect.origin.x +1, newRect.origin.y+newRect.size.height-((j+1)*blocksize) - marge, newRect.size.width-2,blocksize)] autorelease];
        blockView.backgroundColor=themecolor;
        [optimizeViewArray addObject: blockView];
        //NSLog(@"addblocktooptimize %i",[optimizeViewArray count]);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view addSubview:blockView];
        });
        [NSThread sleepForTimeInterval:0.01];
    }
}

- (void)optimizeGraphUpdate{
    //UIColor *lightthemecolor = [UIColor colorWithRed:0.592 green:0.992 blue:0.392 alpha:1];
    //UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1];
     NSLog(@"optimize-optimizeGraphUpdate");
    if (runCurrentScan) {
        [NSThread detachNewThreadSelector:@selector(growCurrentPerformanceTo:) toTarget:self withObject:[NSNumber numberWithInt:35]];
    }
    if (runOptimizeScan) {
        if (currentmax < oStatus->measuredDataFromEvents) {
            NSLog(@"currentmax < oStatus->measuredDataFromEvents");
            NSLog(@"currentmax=%i AND oStatus->measuredDataFromEvents=%i ",currentmax, oStatus->measuredDataFromEvents);
            //progressBar.setProgress(currentmax);
            [NSThread detachNewThreadSelector:@selector(growPerformanceTo:) toTarget:self withObject:[NSNumber numberWithInt:50]];
        }else{
            float prog = ((float) oStatus->measuredDataFromEvents/(float) currentmax);
            int result = ceil(prog/0.02);
            [NSThread detachNewThreadSelector:@selector(growPerformanceTo:) toTarget:self withObject:[NSNumber numberWithInt:result]];
        }
        /*if (progressBar.getProgress() > progressBar.getSecondaryProgress()) {
            progressBar.setSecondaryProgress(progressBar.getProgress());
            printf("Optimize setting optimize secondary to %i",progressBar.getProgress()));
        }*/
    }
}

- (void)channelSet{
    NSLog(@"optimize-channelSet");
}

- (void)noInternetAvailable{
    NSLog(@"optimize-noInternetAvailable");
}

- (void)internetAvailable{
    NSLog(@"optimize-internetAvailable");
}

- (void)isLocalConnected{
    NSLog(@"optimize-isLocalConnected");
    [notlocalAlert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)notLocalConnected{
    NSLog(@"optimize-notLocalConnected");
    UIViewController *lastViewController = [[self.navigationController viewControllers] lastObject];
    if([lastViewController isKindOfClass:[OptimizeViewController class]]) {
        NSString *locString = NSLocalizedString(@"ReConnectLocalDesc", nil);
        NSString *msg = [locString stringByReplacingOccurrencesOfString:@"#ssid#" withString:[NSString stringWithCString:client->cf->getSavedNetworkKey().c_str() encoding:[NSString defaultCStringEncoding]]];
        notlocalAlert= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ReConnectLocal", nil) message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
        [notlocalAlert setValue:spinner forKey:@"accessoryView"];
        [spinner startAnimating];
        [notlocalAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    }
}

- (void)connectionDetermined{
    NSLog(@"optimize-connectionDetermined");
    [oStatus start];
}

-(void)setOptimizeButtonTitle:(NSString *)title
{
    [optimizeButton setTitle:title forState:UIControlStateNormal];
}

-(void)setOptimizeButtonHidden:(NSNumber *)number
{
    [optimizeButton setHidden:[number boolValue]];
}

-(void)setExtendButtonHidden:(NSNumber *)number
{
    [extendButton setHidden:[number boolValue]];
}

-(id)doOptimization{
    oStatus = NULL;
    runCurrentScan = true;
    runOptimizeScan = false;
    currentChannel = "";
    bestChannel = "";
    bestDataSent = 0;
    bestblocknr = 0;
    currentDataSent = 0;
    currentTimeSent = 0;
    currentSpeed = 0.0;
    maxTestTime = 10; // seconds
    currentSpeedTestProgress = 0;
    wifiOptimizeSetChannel = 0;
    [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:NSLocalizedString(@"OptimizeCurrentDesc", nil) waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(setOptimizeButtonTitle:) withObject:NSLocalizedString(@"OptimizeCancel", nil) waitUntilDone:YES];
    [self performSelectorOnMainThread:@selector(setExtendButtonHidden:) withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
    oStatus = [[OptimizeViewStatus alloc] initWithClient:client andMaxSecondsToTest:maxTestTime];
    [oStatus addObserver:self];
    NSLog(@"observer SET");
    [oStatus determineConnection];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        oStatus = NULL;
        runCurrentScan = true;
        runOptimizeScan = false;
        currentChannel = "";
        bestChannel = "";
        bestDataSent = 0;
        bestblocknr = 0;
        currentDataSent = 0;
        currentTimeSent = 0;
        currentSpeed = 0.0;
        maxTestTime = 10; // seconds
        currentSpeedTestProgress = 0;
        wifiOptimizeSetChannel = 0;
        currentViewArray = [[NSMutableArray alloc] initWithCapacity:50];
        optimizeViewArray = [[NSMutableArray alloc] initWithCapacity:50];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    bestBlockView=NULL;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:NSLocalizedString(@"boost_message", nil) waitUntilDone:YES];
    [self performSelectorOnMainThread:@selector(setOptimizeButtonTitle:) withObject:NSLocalizedString(@"Optimize", nil) waitUntilDone:YES];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(actionSheet == connAlert)
    {
        if (buttonIndex == 0)
        {
            [NSThread detachNewThreadSelector:@selector(doOptimization) toTarget:self withObject:nil];
        }
        
    }
}

- (IBAction)buttonPushed:(id)sender {
    if(stage==1){ //Starting the optimization of the network performance
        if(client->cf->deviceIsConnected()){
            optimizeImage.hidden=TRUE;
            CoreCommunication::ConnectList listC;
            client->cf->cf->getLocalAccessList(client->cf->getGatewayJID(), listC);
            if(listC.size()>0){
                connAlert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention", nil) message:NSLocalizedString(@"OptimizeNetworkAttention", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
                [connAlert show];
                backgroundimage.image=NULL;
                [currentView setHidden:FALSE];
                [optimizingView setHidden:FALSE];
                [currentLabel setHidden:FALSE];
                [optimizingLabel setHidden:FALSE];
                stage=2;
            }else {
                notlocalAlert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention", nil) message:NSLocalizedString(@"OptimizeNetworkLocal", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
                [notlocalAlert show];
            }
        } else {
            notlocalAlert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention", nil) message:NSLocalizedString(@"OptimizeNetworkLocal", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
            [notlocalAlert show];
        }
    } else if (stage==2){ //Cancel the optimization of the network performance
        //cancel=TRUE;
        [self.navigationController popViewControllerAnimated:YES];
    } else if (stage==3){ //Apply the best found network configuration
        wifiOptimizeSetChannel = new OptimizeViewSetChannel();
        wifiOptimizeSetChannel->client=client;
        wifiOptimizeSetChannel->wStatus = oStatus;
        wifiOptimizeSetChannel->setChannel(atoi(bestChannel.c_str()));
        stage=4;
        [self.textBoxLabel performSelectorOnMainThread:@selector(setText:) withObject:NSLocalizedString(@"OptimizeApplySettingsDone", nil) waitUntilDone:NO];
        [self performSelectorOnMainThread:@selector(setOptimizeButtonTitle:) withObject:NSLocalizedString(@"OptimizeHome", nil) waitUntilDone:YES];
    } else if(stage==4){
        [self performSelectorOnMainThread:@selector(back) withObject:nil waitUntilDone:YES];
    }
}

-(IBAction)extendWifiPressed:(id)sender{
    
    WE1ScanViewController* scanviewC=[[WE1ScanViewController alloc] initWithClient:client];
    [self.navigationController pushViewController:scanviewC animated:YES];
    [scanviewC release];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    CALayer *btnLayer = [optimizeButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    btnLayer = [extendButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];

    [super viewWillAppear:animated];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"OptimizeTitle", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil) ];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [[self.navigationController navigationBar] setHidden:NO];
    [currentView setHidden:TRUE];
    [optimizingView setHidden:TRUE];
    currentLabel.textColor=themecolor;
    optimizingLabel.textColor=themecolor;
    optimizeImage.hidden=FALSE;
    [currentLabel setHidden:TRUE];
    [optimizingLabel setHidden:TRUE];
    if(IS_IPAD)
    {
        [backgroundimage setImage:[UIImage imageNamed:@"wifi_disabled.png"]];
        blocksize=8;
        stage=1;
        marge = 0;
    } else {
        blocksize=4;
        stage=1;
        marge = 64;
        [backgroundimage setImage:[UIImage imageNamed:@"wifi_disabled.png"]];
    }
}

- (IBAction)back{
    [oStatus stopRunning];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
