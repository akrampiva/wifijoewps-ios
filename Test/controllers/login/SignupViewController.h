//
//  @name SignupViewController.h
//
//  @desc The signup viewcontroller handles the ios application signup screen functions
//  @author Kurt Vermeersch.
//
//  Copyright 2013 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController <UITextFieldDelegate>{
    IBOutlet UITextField *newusernameField;
    IBOutlet UILabel *username;
    IBOutlet UITextField *newpasswordField;
    IBOutlet UILabel *password;
    IBOutlet UITextField *newpasswordconfirmField;
    IBOutlet UILabel *passwordconfirm;
    IBOutlet UIButton *signUpButton;
    IBOutlet UIButton *signInButton;
}

@property (nonatomic, retain) UITextField *newusernameField;
@property (nonatomic, retain) UITextField *newpasswordField;
@property (nonatomic, retain) UITextField *newpasswordconfirmField;
@property (nonatomic, retain) UILabel *username;
@property (nonatomic, retain) UILabel *password;
@property (nonatomic, retain) UILabel *passwordconfirm;
@property (nonatomic, retain) UIButton *signUpButton;
@property (nonatomic, retain) UIButton *signInButton;

-(void)LocalizeStrings;
- (IBAction) signup: (id) sender;

@end
