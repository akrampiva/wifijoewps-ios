//
//  @name SignupViewController.mm
//
//  @desc The signup viewcontroller handles the ios application signup screen functions
//  @author Kurt Vermeersch.
//
//  Copyright 2013 Cloud Friends. All rights reserved.
//

#import "SignupViewController.h"

@implementation SignupViewController

// Text Fields
@synthesize newusernameField;
@synthesize newpasswordField;
@synthesize newpasswordconfirmField;
// Labels
@synthesize username;
@synthesize password;
@synthesize passwordconfirm;
// Buttons
@synthesize signInButton;
@synthesize signUpButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    newusernameField.delegate=self;
    newpasswordField.delegate=self;
    newpasswordconfirmField.delegate=self;
    [self LocalizeStrings];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO; 
}

-(void)LocalizeStrings
{
    username.text = NSLocalizedString(@"ChooseUsername", nil) ;
    password.text = NSLocalizedString(@"ChoosePassword", nil) ;
    passwordconfirm.text = NSLocalizedString(@"Retype", nil) ;
    [signUpButton setTitle:NSLocalizedString(@"SignUp", nil) forState:UIControlStateNormal];
    [signInButton setTitle:NSLocalizedString(@"AlreadyAccount", nil) forState:UIControlStateNormal];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setHidden:YES];
    [newusernameField resignFirstResponder];
    [newpasswordField resignFirstResponder];
    [newpasswordconfirmField resignFirstResponder];
    newusernameField.delegate=self;
    newpasswordField.delegate=self;
    newpasswordconfirmField.delegate=self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)textFieldDoneEditing:(id)sender{
    [sender resignFirstResponder];
}

- (IBAction)signup: (id) sender
{
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"SignupUnavailableAlertTitle", nil) message:NSLocalizedString(@"SignupUnavailableAlertDesc", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] autorelease];
    [av show];
}


@end
