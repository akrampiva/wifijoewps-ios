//
//  SelectionScreenViewController.h
//
//  Selection screen functionality for the application
//  where one chooses which part of the application to launch
//
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "CloudFriendsWrapper.h"
#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
#import "MyDevicesViewController.h"
#import "GuestViewController.h"
#import "OptimizeViewController.h"
#import "SpeedCheckViewController.h"
#import "LoginViewController.h"
#import <UIKit/UIKit.h>

@class SettingsViewController;
@class MyDevicesViewController;
@class GuestViewController;
@class SpeedCheckViewController;
@class OptimizeViewController;
@interface SelectionScreenViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate, UIAlertViewDelegate>{
    // Controllers
    IBOutlet SettingsViewController *settingsViewController;
    IBOutlet MyDevicesViewController *myDevicesViewController;
    IBOutlet GuestViewController *guestViewController;
    IBOutlet SpeedCheckViewController *speedCheckViewController;
    IBOutlet OptimizeViewController *optimizeViewController;
    IBOutlet UIView *masterView;

    UIAlertView *notlocalAlert;
    UIAlertView *alert;
    
@public
    LoginViewController *loginViewController;
    CloudFriendsWrapper *cf;
}
@property (strong, nonatomic) IBOutlet UICollectionView *myCollectView;
@property (nonatomic, retain) SettingsViewController *settingsViewController;
@property (nonatomic, retain) MyDevicesViewController *myDevicesViewController;
@property (nonatomic, retain) GuestViewController *guestViewController;
@property (nonatomic, retain) OptimizeViewController *optimizeViewController;
@property (nonatomic, retain) SpeedCheckViewController *speedCheckViewController;
@property (nonatomic, retain) UIAlertView *notlocalAlert;
@property (nonatomic, retain) UIAlertView *alert;
@property (nonatomic, retain) UIView *masterView;

- (IBAction) settings: (id) sender;
- (IBAction) myDevices: (id) sender;
- (IBAction) logout: (id) sender;
- (IBAction) guest: (id) sender;
- (IBAction) optimize:(id)sender;
- (IBAction) speedCheck:(id)sender;
- (void) devicedownmessage;
@end