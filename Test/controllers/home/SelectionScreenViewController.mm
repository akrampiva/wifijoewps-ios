//
//  SelectionScreenViewController.mm
//
//  Selection screen functionality for the application
//  where one chooses which part of the application to launch
//
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "SelectionScreenViewController.h"
#import "SelectionScreenCell.h"
#import "deviceutils.h"
#import "ApiManager.h"
#import "WPSViewController.h"

#define WPS 451

@interface SelectionScreenViewController ()
{
    NSArray *arrayOfTexts;
    NSArray *arrayOfImages;
    NSArray *arrayOfClasses;
}
@end

@implementation SelectionScreenViewController

// Controllers
@synthesize notlocalAlert;
@synthesize alert;
@synthesize settingsViewController;
@synthesize myDevicesViewController;
@synthesize speedCheckViewController;
@synthesize guestViewController;
@synthesize optimizeViewController;
@synthesize masterView;

- (void) deviceAdjust{
    if(IS_IPHONE_5)
    {
            CGRect frame=_myCollectView.frame;
            frame.size.height= 482;
            frame.origin.y = 100;
            _myCollectView.frame=frame;
    }
    else if(IS_IPHONE){
  
            CGRect frame=_myCollectView.frame;
            frame.size.height= 394;
            frame.origin.y = 68;
            _myCollectView.frame=frame;
    }
    else
    {
        //_myCollectView.frame=CGRectMake(28, 120,712, 884);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad
{
    [[self.navigationController navigationBar] setHidden:YES];
    [self deviceAdjust];
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [[self myCollectView]setDelegate:self];
    [[self myCollectView]setDataSource:self];
        [[self myCollectView] registerClass:[SelectionScreenCell class] forCellWithReuseIdentifier:@"Cell"];
    // Do any additional setup after loading the view, typically from a nib.
    arrayOfTexts=[[NSArray alloc]initWithObjects:@"My Devices",@"Easy Guest Access",@"Speed Check",@"WPS",@"Settings",@"Logout", nil];
    arrayOfImages=[[NSArray alloc]initWithObjects:@"home_devices.png",@"guest_access.png",@"home_speed.png",@"home_wps_ios.png",@"home_configure.png",@"home_logout.png", nil];
    NSValue* selCommandMyDevices = [NSValue valueWithPointer:@selector(myDevices:)];
    NSValue* selCommandGuest = [NSValue valueWithPointer:@selector(guest:)];
    NSValue* selCommandSpeed = [NSValue valueWithPointer:@selector(speedCheck:)];
    //NSValue* selCommandOptimize = [NSValue valueWithPointer:@selector(optimize:)];
    NSValue* selCommandWPS = [NSValue valueWithPointer:@selector(wps:)];
    NSValue* selCommandConfigure = [NSValue valueWithPointer:@selector(settings:)];
    NSValue* selCommandLogout = [NSValue valueWithPointer:@selector(logout:)];
    arrayOfClasses = [[NSArray alloc]initWithObjects:selCommandMyDevices, selCommandGuest,selCommandSpeed, selCommandWPS,selCommandConfigure, selCommandLogout, nil ];
    if(isIOS7)
    {
        CGRect frame = self.masterView.frame;
        frame.origin.y += 20;
        self.masterView.frame = frame;
    }
    [NSThread detachNewThreadSelector:@selector(performChecks) toTarget:self withObject:nil];
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayOfTexts count];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_5)
    {
        return CGSizeMake(136.f, 136.f);
    } else if(IS_IPHONE){
        return CGSizeMake(136.f, 120.f);
    }
    else{
        return CGSizeMake(300.f, 240.f);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    NSValue *method = [arrayOfClasses objectAtIndex:indexPath.item  ];
    SEL handle = (SEL)([method pointerValue]);
    [self performSelector:handle];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"Cell";
    SelectionScreenCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.labelView.text= [arrayOfTexts objectAtIndex:indexPath.item  ];
    UIImage *image = [UIImage imageNamed: [arrayOfImages objectAtIndex:indexPath.item  ]];
    cell.imageView.image=image;
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) logout: (id) sender
{
    [loginViewController performSelectorOnMainThread:@selector(logout:) withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

- (IBAction) settings: (id) sender
{
    if (cf->cf->deviceIsConnected()){
        if(IS_IPHONE_5)
        {
            settingsViewController =[[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        } else if(IS_IPHONE){
            settingsViewController =[[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        }
        else{
            settingsViewController =[[SettingsViewController alloc] initWithNibName:@"SettingsViewController-iPad" bundle:nil];
        }
        settingsViewController->client = cf;
        [[self navigationController] pushViewController:settingsViewController animated:YES];
    }
    else{
        [self devicedownmessage];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (IBAction) myDevices:(id)sender
{
    if (cf->cf->deviceIsConnected()){
        if(IS_IPHONE_5)
        {
            myDevicesViewController =[[MyDevicesViewController alloc] initWithNibName:@"MyDevicesViewController" bundle:nil];
        } else if(IS_IPHONE){
            myDevicesViewController =[[MyDevicesViewController alloc] initWithNibName:@"MyDevicesViewController" bundle:nil];
        }
        else{
            myDevicesViewController =[[MyDevicesViewController alloc] initWithNibName:@"MyDevicesViewController-iPad" bundle:nil];
        }
        myDevicesViewController->client = cf;
        [[self navigationController] pushViewController:myDevicesViewController animated:YES];
    }
    else{
        [self devicedownmessage];
    }
}

- (IBAction) guest:(id)sender
{
    if (cf->cf->deviceIsConnected()){
        if(IS_IPHONE_5)
        {
            guestViewController =[[GuestViewController alloc] initWithNibName:@"GuestViewController" bundle:nil];
        } else if(IS_IPHONE){
            guestViewController =[[GuestViewController alloc] initWithNibName:@"GuestViewController" bundle:nil];
        }
        else{
            guestViewController =[[GuestViewController alloc] initWithNibName:@"GuestViewController-iPad" bundle:nil];
        }
        guestViewController->client = cf;
        [[self navigationController] pushViewController:guestViewController animated:YES];
    }
    else{
        [self devicedownmessage];
    }
}

- (IBAction) speedCheck:(id)sender
{
    if(IS_IPHONE_5)
    {
        speedCheckViewController =[[SpeedCheckViewController alloc] initWithNibName:@"SpeedCheckViewController" bundle:nil];
    } else if(IS_IPHONE){
        speedCheckViewController =[[SpeedCheckViewController alloc] initWithNibName:@"SpeedCheckViewController" bundle:nil];
    }
    else{
        speedCheckViewController =[[SpeedCheckViewController alloc] initWithNibName:@"SpeedCheckViewController-iPad" bundle:nil];
    }
    speedCheckViewController->client = cf;
    [[self navigationController] pushViewController:speedCheckViewController animated:YES];
}

- (IBAction) optimize:(id)sender
{
    if (cf->cf->deviceIsConnected()){
        NSLog(@"Device is connected");
        if(cf->cf->isGatewayLocal()){
            NSLog(@"Gateway is local");
            if(IS_IPHONE_5)
            {
                optimizeViewController =[[OptimizeViewController alloc] initWithNibName:@"OptimizeViewController" bundle:nil];
            } else if(IS_IPHONE){
                optimizeViewController =[[OptimizeViewController alloc] initWithNibName:@"OptimizeViewController" bundle:nil];
            }
            else{
                optimizeViewController =[[OptimizeViewController alloc] initWithNibName:@"OptimizeViewController-iPad" bundle:nil];
            }
            optimizeViewController->client = cf;
            [[self navigationController] pushViewController:optimizeViewController animated:YES];
        } else {
            notlocalAlert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention", nil) message:NSLocalizedString(@"OptimizeNetworkLocal", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
            [notlocalAlert show];
        }
       
    }
    else{
        [self devicedownmessage];
    }
}

- (IBAction) wps:(id)sender
{
    if (cf->cf->deviceIsConnected()){
        NSLog(@"Device is connected");
        if(cf->cf->isGatewayLocal()){
            NSLog(@"Gateway is local");
            WPSViewController* wpsController=[[WPSViewController alloc] initWithClient:cf];
            [[self navigationController] pushViewController:wpsController animated:YES];
            [wpsController release];
           
            
        } else {
            notlocalAlert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attention", nil) message:NSLocalizedString(@"OptimizeNetworkLocal", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
            [notlocalAlert show];
        }
        
    }
    else{
        [self devicedownmessage];
    }
}

-(void)dismiss:(UIAlertView*)alertpar
{
    NSLog(@"alertpar");
    dispatch_async(dispatch_get_main_queue(), ^{
        [alertpar dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"clickedButtonAtIndex");
    if (alertView == alert) {
        [loginViewController performSelectorOnMainThread:@selector(logout:) withObject:[NSNumber numberWithBool:YES] waitUntilDone:YES];
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
}

- (void) performChecks {
    alert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WaitingForDevice", nil) message:NSLocalizedString(@"WaitingForDeviceDesc", nil) delegate:self cancelButtonTitle:@"Logout" otherButtonTitles: nil] autorelease];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    [self startUpCheckingGateway:cf andDelegate:self finishBlock:^(BOOL availableOrinstalled) {
        if(availableOrinstalled)
        {
            [self performSelector:@selector(dismiss:) withObject:alert];
            if (cf->cf->isConnectedToServer()){
            [self startUpChecking:cf andDelegate:self finishBlock:^(BOOL availableOrinstalled) {
                if(availableOrinstalled)
                {
                }
            }];
            }
        }
    }];
    

}

- (void) startUpCheckingGateway:(CloudFriendsWrapper*) cwf andDelegate:(UIViewController*)delegate finishBlock:(void (^)(BOOL availableOrinstalled))block{
    NSLog(@"startUpCheckingGateway");
    while(cf->cf->isConnectedToServer() && ![[ApiManager sharedInstance] gatewayAvailable:cwf])
    {
        NSLog(@"back to sleep waiting for gateway");
        sleep(1);
    }
    NSLog(@"startUpCheckingGateway after");
    block(YES);
    return;
}

- (void) startUpChecking:(CloudFriendsWrapper*) cwf andDelegate:(UIViewController*)delegate finishBlock:(void (^)(BOOL availableOrinstalled))block{
    if([[ApiManager sharedInstance] isApiAvailableWithCloudFriendWrapper:cwf])
    {
        block(YES);
        return;
    }
    alert=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WaitingForInstallation", nil) message:NSLocalizedString(@"WaitingForInstallationDesc", nil) delegate:self cancelButtonTitle:@"Logout" otherButtonTitles: nil] autorelease];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];

    NSString* gwJid = [NSString stringWithFormat:@"%s",cwf->cf->getGatewayJID().c_str()];
    [[ApiManager sharedInstance] makeAPIsAvailableWithdeviceAccount:gwJid andTopic:A_TOPIC andApiName:A_APINAME andCloudFriendWrapper:cf finishBlock:^(BOOL installed) {
        if(installed)
        {
            [self performSelector:@selector(dismiss:) withObject:alert];
            block(YES);
            return;
        }
        else{
            [[ApiManager sharedInstance] showAlertWithTitle:NSLocalizedString(@"WaitingForInstallationFail",nil)  andMessage:NSLocalizedString(@"WaitingForInstallationFailDesc",nil) andDelegate:delegate  andTag:UPDATE_ALERT];
        }
    }];
}

-(void) devicedownmessage{
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DeviceDownError",nil) message:NSLocalizedString(@"DeviceDownErrorDesc",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
    [av show];
}




@end
