//
//  MyDevicesViewController.h
//
//  List the devices that have permanent LAN access
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"
#import "SpeechBalloonView.h"

struct TableListEntry{
    inline TableListEntry(){}
	inline TableListEntry(std::string m, std::string i, std::string h, Result t,SignalStrength str, bool a):
    mac(m), ip(i), hostname(h), type(t), strength(str), active(a)
	{}
	std::string	mac;
	std::string	ip;
	std::string	hostname;
	Result		type;
    SignalStrength strength;
    bool    active;
};

typedef std::map<std::string,TableListEntry> TableListMapType;

@interface MyDevicesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet SpeechBalloonView *speechBalloon;
    IBOutlet UILabel *ssidLabel;
    IBOutlet UIActivityIndicatorView *loginIndicator;
    IBOutlet UITextField *devicePasswordField;
    IBOutlet UIImageView *devicePasswordFieldEye;
    IBOutlet UITableView *devicesTable;
    bool open;
    bool closed;
    @public
    CloudFriendsWrapper *client;
    TableListMapType deviceMap;
    std::vector<TableListEntry> deviceList;
}

void parse_mac(std::vector<uint8_t>& out, std::string const& in);
- (void)lookUpDevices;
- (IBAction)back;
- (IBAction) switchPassVisibility:(id)sender;
@property (nonatomic, retain) SpeechBalloonView *speechBalloon;
@property (nonatomic, retain) UILabel *ssidLabel;
@property (nonatomic, retain) UIActivityIndicatorView *loginIndicator;
@property (nonatomic, retain) UITextField *devicePasswordField;
@property (nonatomic, retain) UIImageView *devicePasswordFieldEye;
@property (nonatomic, retain) UITableView *devicesTable;


@end
