//
//  MyDevicesViewController.mm
//
//  List the devices that have permanent LAN access
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "MyDevicesViewController.h"
#include <vector>
#include <string.h>
#include "hash.h"

@implementation MyDevicesViewController

@synthesize speechBalloon;
@synthesize ssidLabel;
@synthesize loginIndicator;
@synthesize devicePasswordField;
@synthesize devicePasswordFieldEye;
@synthesize devicesTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) { }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    open = 0;
    [NSThread detachNewThreadSelector:@selector(lookUpDevices) toTarget:self withObject:nil];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

-(void)loadsettings{
    try {
        WlSettings wlsettings = client->cf->getWireless();
        NSString *ssid = [NSString stringWithCString:wlsettings.ssid.c_str() encoding:[NSString defaultCStringEncoding]];
        NSString *pass = [NSString stringWithCString:wlsettings.key.c_str() encoding:[NSString defaultCStringEncoding]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            ssidLabel.text = ssid;
            devicePasswordField.text = pass;
        }];
    } catch (std::exception &e) {
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:[NSString stringWithCString:e.what() encoding:NSUTF8StringEncoding] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    } catch (...){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:NSLocalizedString(@"FatalErrorDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return deviceList.size()+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

#define customSeparatorHeight 3
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return [[UIView alloc] initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, 0)];
    }
    return [[UIView alloc] initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, customSeparatorHeight)];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 0;
    }
    return customSeparatorHeight;
}

#define LABEL_TAG 1234
#define ICON_TAG 2345

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    cell.backgroundColor =themecolor;
    cell.contentView.backgroundColor = themecolor;
    CALayer *cellImageLayer = cell.layer;
    [cellImageLayer setCornerRadius:10];
    [cellImageLayer setMasksToBounds:YES];
        CGRect customframe = CGRectMake(55, 0, 220, 44);
        UILabel *lbl = [[UILabel alloc] initWithFrame:customframe];
        lbl.textColor=[UIColor whiteColor];
        lbl.backgroundColor=[UIColor clearColor];
        UIActivityIndicatorView *spinner = [[[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
        if([indexPath section]==0){
            lbl.text = NSLocalizedString(@"DevicesSearching", nil);
            UIImage *spacer = [UIImage imageNamed:@"spacer"];
            UIGraphicsBeginImageContext(spinner.frame.size);
            [spacer drawInRect:CGRectMake(0,0,spinner.frame.size.width,spinner.frame.size.height)];
            UIImage* resizedSpacer = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            cell.imageView.image = resizedSpacer;
            [spinner setFrame:CGRectMake(5.0, 0.0, spinner.frame.size.width,spinner.frame.size.height)];
            [cell.imageView addSubview:spinner];
            [spinner startAnimating];
        } else {
            int nrEl=[indexPath section];
            TableListEntry entry= deviceList.at(nrEl-1);
            UIImageView * myimageview = [[UIImageView alloc] initWithFrame:CGRectMake(20, (44-20)/2, spinner.frame.size.width,spinner.frame.size.height)];
            UIImage *spacer = [UIImage imageNamed:@"full_access_white_icon.png"];
            if(!entry.active){
                spacer=[UIImage imageNamed:@"full_access_grey_icon.png"];
            }
            UIGraphicsBeginImageContext(spinner.frame.size);
            [spacer drawInRect:CGRectMake(0,0,spinner.frame.size.width,spinner.frame.size.height)];
            UIImage* resizedSpacer = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            myimageview.image = resizedSpacer;
            [cell.contentView addSubview:myimageview];
            if([[NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]] isEqualToString:@"*"]){
                lbl.text = [NSString stringWithCString:entry.ip.c_str() encoding:[NSString defaultCStringEncoding]];
            } else {
                lbl.text = [NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]];
            }
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;
            UIImageView * myimageviewStrength = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth-40, (44-20)/2, spinner.frame.size.width,spinner.frame.size.height)];
            UIImage *spacerStrength = [UIImage imageNamed:@"wifi_good.png"];
            if(entry.strength==SignalStrength::BAD){
                spacerStrength=[UIImage imageNamed:@"wifi_bad.png"];
            } else if (entry.strength==SignalStrength::SLOW){
                spacerStrength=[UIImage imageNamed:@"wifi_slow.png"];
            } else if (entry.strength==SignalStrength::PERFECT){
                spacerStrength=[UIImage imageNamed:@"wifi_perfect.png"];
            }
            int type = entry.type;
            if(type == WIRED)
            {
                spacerStrength=[UIImage imageNamed:@"wifi_extender_network_cable.png"];
            }
            UIGraphicsBeginImageContext(spinner.frame.size);
            [spacerStrength drawInRect:CGRectMake(0,0,spinner.frame.size.width,spinner.frame.size.height)];
            UIImage* resizedSpacerStrength = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            myimageviewStrength.image = resizedSpacerStrength;
            [cell.contentView addSubview:myimageviewStrength];

        }
        lbl.font = [UIFont systemFontOfSize:20];
        lbl.tag=LABEL_TAG;
        [cell.contentView addSubview:lbl];
        [lbl release];
        return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        [devicesTable deselectRowAtIndexPath:indexPath animated:YES];
    [devicesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic]; 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(void)lookUpDevices{
    [self loadsettings];
    while(!closed){
        // All the leases
        std::vector<DhcpLease> leases = client->cf->getLeases(NULL);
        std::string ip;
        std::string vendor;
        std::string ip_vendor;
        for (int i=0; i < leases.size(); i++) {
            if(closed){
                break;
            }
            ip = leases.at(i).ip.c_str();
            if(leases.at(i).type==GUEST){
            }
            //else if(leases.at(i).type==WIRED){
            //}
            else {
                NSLog(@"Currently connected type %d",leases.at(i).type);
                deviceMap[leases.at(i).mac]=TableListEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type,leases.at(i).strength, true);
            }
        }
        deviceList.clear();
        for (TableListMapType::iterator it= deviceMap.begin();it!=deviceMap.end();++it){
            deviceList.push_back(it->second);
        }
        [devicesTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        [NSThread sleepForTimeInterval:5.0];
    }
}

-(IBAction)textFieldDoneEditing:(id)sender{
    [sender resignFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    devicePasswordField.layer.borderColor=themecolor.CGColor;
    [devicePasswordField.layer setCornerRadius:10.0f];
    devicePasswordField.borderStyle = UITextBorderStyleRoundedRect;
    devicePasswordField.layer.borderWidth=1.0;
    devicePasswordField.secureTextEntry = YES;
    closed=FALSE;
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setHidden:NO];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"DevicesTitle", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35 , 35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (IBAction)back{
    closed=TRUE;
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) switchPassVisibility:(id)sender{
        UIImage *btnImage1;
        if (open == 0){
            open = 1;
            btnImage1 = [UIImage imageNamed:@"permanent_access_eye_closed.png"];
            devicePasswordField.secureTextEntry = FALSE;
        }
        else{
            open = 0;
            btnImage1 = [UIImage imageNamed:@"permanent_access_eye_open.png"];
            devicePasswordField.secureTextEntry = TRUE;
        }
        [devicePasswordFieldEye setImage:btnImage1];
}

@end
