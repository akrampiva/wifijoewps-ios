//
//  SpeedCheckViewController.h
//
//  Speed check functionality for the application
//  where internet and gateway connectivity is checked.
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"
#import "speedtest.h"

@interface SpeedCheckViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *array;
    IBOutlet UITableView *myTable;
     NSMutableArray *tableViewData;
    NSIndexPath *selectedIndexPath;
    
    IBOutlet UILabel *firstLabel;
    IBOutlet UIActivityIndicatorView *firstActivity;
    IBOutlet UILabel *secondLabel;
     IBOutlet UIActivityIndicatorView *secondActivity;
    IBOutlet UIProgressView *progressBar;
    IBOutlet UIButton *leftButton;
    IBOutlet UIButton *middleButton;
    IBOutlet UIButton *rightButton;
    IBOutlet UIButton *textBoxButton;
    IBOutlet UIImageView *optimizeImage;
    
    BOOL isTestingConnection;
    NSInteger testingConnectionPhaseA;
    NSInteger testingConnectionPhaseB;
    BOOL deviceUp;
    BOOL deviceConnected;
    float connectionTestProgress;
    
    BOOL isDiscoveringDevices;
    float deviceDiscoveryProgress;
 
    @public
    CloudFriendsWrapper *client;
    SpeedTest *speedTest;
    bool speedTestDone;
    std::string resultBytesPerSecond;
    SpeedTest *speedTestUp;
    bool speedTestUpDone;
    std::string resultBytesUpPerSecond;
}

@property (nonatomic, retain) UITableView *myTable;
@property (nonatomic, retain) NSMutableArray *array;
@property (nonatomic, retain) NSMutableArray *tableViewData;
@property (nonatomic, retain) NSIndexPath *selectedIndexPath;

@property (nonatomic, retain) UILabel *firstLabel;
@property (nonatomic, retain) UILabel *secondLabel;
@property (nonatomic, retain) UIActivityIndicatorView *firstActivity;
@property (nonatomic, retain) UIActivityIndicatorView *secondActivity;
@property (nonatomic, retain) UIProgressView *progressBar;
@property (nonatomic, retain) UIButton *leftButton;
@property (nonatomic, retain) UIButton *middleButton;
@property (nonatomic, retain) UIButton *rightButton;
@property (nonatomic, retain) UIButton *textBoxButton;
@property (nonatomic, retain) UIImageView *optimizeImage;
- (IBAction)back;

@end
