//
//  SpeedCheckViewController.mm
//
//  Speed check functionality for the application
//  where internet and gateway connectivity is checked.
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "SpeedCheckViewController.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include "getmac.h"
#include <sstream>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "Reachability.h"
#import "OptimizeViewController.h"
#import "deviceutils.h"

@implementation SpeedCheckViewController

@synthesize array;
@synthesize myTable;
@synthesize tableViewData = _tableViewData;
@synthesize selectedIndexPath = _selectedIndexPath;
@synthesize firstLabel;
@synthesize firstActivity;
@synthesize secondLabel;
@synthesize secondActivity;
@synthesize progressBar;
@synthesize leftButton;
@synthesize middleButton;
@synthesize rightButton;
@synthesize textBoxButton;
@synthesize optimizeImage;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction) optimize:(id)sender
{
    if (client->cf->deviceIsConnected()){
        OptimizeViewController *optimizeViewController;
            if(IS_IPHONE_5)
            {
                optimizeViewController =[[OptimizeViewController alloc] initWithNibName:@"OptimizeViewController" bundle:nil];
            } else if(IS_IPHONE){
                optimizeViewController =[[OptimizeViewController alloc] initWithNibName:@"OptimizeViewController" bundle:nil];
            }
            else{
                optimizeViewController =[[OptimizeViewController alloc] initWithNibName:@"OptimizeViewController-iPad" bundle:nil];
            }
        optimizeViewController->client = client;
        NSMutableArray* newViewControllers = [self.navigationController.viewControllers mutableCopy];
        [newViewControllers replaceObjectAtIndex:[newViewControllers indexOfObject:self] withObject:optimizeViewController];
        [self.navigationController setViewControllers:newViewControllers animated:YES];
    }
    else{
        [self devicedownmessage];
    }
}

-(void) devicedownmessage{
    UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DeviceDownError",nil) message:NSLocalizedString(@"DeviceDownErrorDesc",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
    [av show];
}

-(void)startConnectionTest{
    isTestingConnection=YES;
    testingConnectionPhaseA=1;
    testingConnectionPhaseB=1;
    connectionTestProgress=0.0f;
    deviceConnected = 1;
    deviceUp = 1;
    self.firstLabel.hidden=NO;
    self.firstLabel.text=NSLocalizedString(@"SpeedCheckStartMessage", nil);
    [progressBar setProgress:connectionTestProgress];
    progressBar.hidden=FALSE;
    NSInteger startindexDevices=3;
    for (id object in array){
        [object class];
        [self.tableViewData
         removeObjectAtIndex:startindexDevices];
    }
    [array removeAllObjects];
    [NSThread detachNewThreadSelector:@selector(doConnectionTest) toTarget:self withObject:nil];
}

-(void)doConnectionSpeedTest{
    speedTestDone=false;
    if(client->cf->deviceIsConnected()){
        speedTest=new SpeedTest(client->cf->getGatewayJID(),10,std::numeric_limits<ssize_t>::max());
        speedTest->receive(resultBytesPerSecond);
    } else {
        deviceUp=0;
        speedTest=NULL;
    }
    speedTestDone=true;
}

-(void)doConnectionSpeedTestUp{
    speedTestUpDone=false;
    if(client->cf->deviceIsConnected()){
        speedTestUp=new SpeedTest(client->cf->getGatewayJID(),10,std::numeric_limits<ssize_t>::max());
        speedTestUp->transmit(resultBytesUpPerSecond);
    } else {
        deviceUp=0;
        speedTestUp=NULL;
    }
    speedTestUpDone=true;
}

-(void)doConnectionTest{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    [self performSelectorOnMainThread:@selector(performProgressTickConnectionTest) withObject:nil waitUntilDone:false];
    [NSThread sleepForTimeInterval:3.0];
    [self performSelectorOnMainThread:@selector(performProgressTickConnectionTest) withObject:nil waitUntilDone:false];
    if ([[Reachability reachabilityWithHostName:NSLocalizedString(@"SpeedCheckReachableSite", nil)] currentReachabilityStatus] == NotReachable) {
        testingConnectionPhaseA=3;
        testingConnectionPhaseB=3;
    }
    else{
        testingConnectionPhaseA=2;
        [self performSelectorOnMainThread:@selector(performReloadImageConnectionTest) withObject:nil waitUntilDone:false];
        [NSThread sleepForTimeInterval:3.0];
        [self performSelectorOnMainThread:@selector(performProgressTickConnectionTest) withObject:nil waitUntilDone:false];
        testingConnectionPhaseB=2;
        if(deviceUp && client->cf->deviceIsConnected()){
            deviceUp=1;
            CoreCommunication::ConnectList listC;
            client->cf->cf->getLocalAccessList(client->cf->getGatewayJID(), listC);
            if(listC.size()>0){
                deviceConnected=1;
            }else {
                deviceConnected=0;
            }
        } else {
            deviceUp=0;
            deviceConnected=0;
        }
        [self performSelectorOnMainThread:@selector(performReloadImageConnectionTest) withObject:nil waitUntilDone:false];
        [NSThread sleepForTimeInterval:1.0];
    }
    speedTestDone=FALSE;
    [self performSelectorOnMainThread:@selector(performStartConnectionTest) withObject:nil waitUntilDone:true];
    if ([[Reachability reachabilityWithHostName:NSLocalizedString(@"SpeedCheckReachableSite", nil)] currentReachabilityStatus] == NotReachable) {
    }
    else if (!deviceUp){
    } else {
        [self performSelectorOnMainThread:@selector(performStartConnection) withObject:nil waitUntilDone:false];
        while(!speedTestDone){
            sleep(10);
        }
        [self performSelectorOnMainThread:@selector(performFinishedConnectionTest) withObject:nil waitUntilDone:true];
        speedTestUpDone=FALSE;
        [self performSelectorOnMainThread:@selector(performStartConnectionTestUp) withObject:nil waitUntilDone:true];
        [self performSelectorOnMainThread:@selector(performStartConnectionUp) withObject:nil waitUntilDone:false];
        while(!speedTestUpDone){
            sleep(10);
        }
        [self performSelectorOnMainThread:@selector(performFinishedConnectionTestUp) withObject:nil waitUntilDone:true];
        textBoxButton.hidden=FALSE;
        optimizeImage.hidden=FALSE;
    }
    [pool release];
}

-(void)performReloadImageConnectionTest{
    [myTable reloadData];
}

-(void)performProgressTickConnectionTest{
    float step=1.0f/(float)3;
    connectionTestProgress+= step;
    [progressBar setProgress:connectionTestProgress];
    [myTable reloadData];
}

-(void)performStartConnection{
    [NSThread detachNewThreadSelector:@selector(doConnectionSpeedTest) toTarget:self withObject:nil];
}

-(void)performStartConnectionUp{
    [NSThread detachNewThreadSelector:@selector(doConnectionSpeedTestUp) toTarget:self withObject:nil];
}

-(void)performStartConnectionTest{
    if ([[Reachability reachabilityWithHostName:NSLocalizedString(@"SpeedCheckReachableSite", nil)] currentReachabilityStatus] == NotReachable) {
        self.firstLabel.text=NSLocalizedString(@"SpeedCheckStatusNoInternet", nil);
    }
    else if (!deviceUp){
        self.firstLabel.text=NSLocalizedString(@"SpeedCheckStatusNoGateway", nil);
    }
    else{
        progressBar.hidden=TRUE;
        firstActivity.hidden=FALSE;
        self.firstLabel.text=NSLocalizedString(@"SpeedCheckStatusSpeedDetermining", nil);
        [myTable reloadData];
    }
}

-(void)performFinishedConnectionTest{
    firstActivity.hidden=TRUE;
    double speedMeasured=0.0;
    if(speedTest!=NULL){
        speedMeasured=speedTest->getMeasuredSpeedDown()*8/1000000;
    }
    if(deviceConnected==1){
        self.firstLabel.text=[[NSLocalizedString(@"SpeedCheckStatusLocalSpeed", nil) stringByAppendingString:[NSString stringWithFormat:@"%.2f",speedMeasured]]stringByAppendingString:NSLocalizedString(@"SpeedCheckStatusMBITS", nil)];
    } else {
        self.firstLabel.text=[[NSLocalizedString(@"SpeedCheckStatusRemoteSpeed", nil) stringByAppendingString:[NSString stringWithFormat:@"%.2f",speedMeasured]]stringByAppendingString:NSLocalizedString(@"SpeedCheckStatusMBITS", nil)];
    }
    self.rightButton.hidden=YES;
    progressBar.hidden=TRUE;
    isTestingConnection=NO;
    [myTable reloadData];
    if(speedTest) { delete speedTest; speedTest=NULL; }
}

-(void)performStartConnectionTestUp{
    secondActivity.hidden=FALSE;
    self.secondLabel.text=NSLocalizedString(@"SpeedCheckStatusSpeedUpDetermining", nil);
    self.secondLabel.hidden=FALSE;
    [myTable reloadData];
}

-(void)performFinishedConnectionTestUp{
    secondActivity.hidden=TRUE;
    double speedMeasuredUp=0.0;
    if(speedTestUp!=NULL){
        speedMeasuredUp=speedTestUp->getMeasuredSpeedUp()*8/1000000;
    }
    if(deviceConnected==1){
        self.secondLabel.text=[[NSLocalizedString(@"SpeedCheckStatusUpLocalSpeed", nil) stringByAppendingString:[NSString stringWithFormat:@"%.2f",speedMeasuredUp]]stringByAppendingString:NSLocalizedString(@"SpeedCheckStatusMBITS", nil)];
    } else {
        self.secondLabel.text=[[NSLocalizedString(@"SpeedCheckStatusUpRemoteSpeed", nil) stringByAppendingString:[NSString stringWithFormat:@"%.2f",speedMeasuredUp]]stringByAppendingString:NSLocalizedString(@"SpeedCheckStatusMBITS", nil)];
    }
    self.rightButton.hidden=YES;
    progressBar.hidden=TRUE;
    isTestingConnection=NO;
    [myTable reloadData];
    if(speedTestUp) { delete speedTestUp; speedTestUp=NULL; }
}

- (void)viewDidLoad
{
    self.progressBar.hidden=YES;
    self.leftButton.hidden=YES;
    self.middleButton.hidden=YES;
    self.rightButton.hidden=YES;
    self.firstLabel.hidden=YES;
    self.optimizeImage.hidden=YES;
    isTestingConnection=FALSE;
    testingConnectionPhaseA=0;
    testingConnectionPhaseB=0;
    deviceConnected=1;
    deviceUp=1;
    self.tableViewData = [NSMutableArray arrayWithObjects:
                          @"",
                          nil];
    self.selectedIndexPath = nil;
    array=[[NSMutableArray alloc]init];
    [myTable reloadData];
    // Autoload the first tableview
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    if ([myTable.delegate respondsToSelector:@selector(tableView:willSelectRowAtIndexPath:)]) {
        [myTable.delegate tableView:myTable willSelectRowAtIndexPath:indexPath];
    }
    [myTable selectRowAtIndexPath:indexPath animated:YES scrollPosition: UITableViewScrollPositionNone];    
    if ([myTable.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
        [myTable.delegate tableView:myTable didSelectRowAtIndexPath:indexPath];
    }
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.tableViewData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (self.selectedIndexPath) {
        NSIndexPath *currentdropDownCellIndexPath = [NSIndexPath indexPathForRow:self.selectedIndexPath.row + 1       inSection:self.selectedIndexPath.section];
        if (indexPath.row == currentdropDownCellIndexPath.row && indexPath.row ==1) {
            static NSString *DropDownCellIdentifier = @"DropDownCell";
            cell = [tableView
                    dequeueReusableCellWithIdentifier:DropDownCellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc]
                        initWithStyle:UITableViewCellStyleDefault
                        reuseIdentifier:DropDownCellIdentifier];
            }
            cell.accessoryView=nil;
            cell.textLabel.opaque=NO;
            cell.textLabel.backgroundColor=[UIColor clearColor];
            cell.detailTextLabel.backgroundColor=[UIColor clearColor];
            NSString* fileName=@"sc_h_oo.png";
            if(testingConnectionPhaseA==1 && (testingConnectionPhaseB==0||   testingConnectionPhaseB==1)){
                fileName=@"sc_h_oo.png";
            } else if(testingConnectionPhaseA==3){ //No Internet Connection (check with google.com)
                fileName=@"sc_h_oo.png";
            } else if(testingConnectionPhaseA==2 && (testingConnectionPhaseB==0||testingConnectionPhaseB==1)){
                fileName=@"sc_o_vx.png";
            } else if(testingConnectionPhaseA==2 && testingConnectionPhaseB==2 && deviceUp==1 && deviceConnected==1){ // directly connected with device
                fileName=@"sc_h_vv.png";
            } else if(testingConnectionPhaseA==2 && testingConnectionPhaseB==2 && deviceUp==1){ // connected via the internet with the device
                fileName=@"sc_o_vv.png";
            } else if(testingConnectionPhaseA==2 && testingConnectionPhaseB==2 && deviceUp==0){ // connected with the internet but device is offline
                fileName=@"sc_o_vx.png";
            }
            cell.backgroundView.clipsToBounds = YES;
            UIImageView *bgView=[ [[UIImageView alloc] initWithImage:[UIImage imageNamed:fileName] ]autorelease];
            bgView.contentMode=UIViewContentModeScaleAspectFit;
            cell.backgroundView =bgView;
            [bgView sizeToFit];
            cell.textLabel.text = @"";
        }
        else {
            static NSString *DataCellIdentifier = @"DataCell";
            cell = [tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc]
                        initWithStyle:UITableViewCellStyleDefault
                        reuseIdentifier:DataCellIdentifier];
            }
            cell.textLabel.opaque=NO;
            cell.textLabel.backgroundColor=[UIColor clearColor];
            cell.detailTextLabel.backgroundColor=[UIColor clearColor];
            cell.textLabel.textColor=[UIColor blackColor];
            cell.textLabel.text = [self.tableViewData objectAtIndex:indexPath.row];
        }
    } else {
        static NSString *DataCellIdentifier = @"DataCell";
        cell = [tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:DataCellIdentifier];
        }        
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if(indexPath.row==0){
            return 150;
        } else if(indexPath.row==1){
            // process item image height
            return 444;
        }
        return 0;
    }
    else{
        if (!self.selectedIndexPath) {
            return 44;
        } else {
            NSIndexPath *currentdropDownCellIndexPath = [NSIndexPath indexPathForRow:(self.selectedIndexPath.row + 1) inSection:self.selectedIndexPath.section];
            if (indexPath.row == currentdropDownCellIndexPath.row && indexPath.row ==1) {
                return 480-44-44-44-183+24;
            } else {
                return 44;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *dropDownCellIndexPath = [NSIndexPath
                                          indexPathForRow:indexPath.row + 1
                                          inSection:indexPath.section];
    if (!self.selectedIndexPath) {
        // Show Drop Down Cell
        if(indexPath.row==0){
            [self.tableViewData insertObject:@"DropDownCell"
                                     atIndex:dropDownCellIndexPath.row];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:dropDownCellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        self.selectedIndexPath = indexPath;
        if(!isTestingConnection){
            [self startConnectionTest];
        }
    } else {
        NSIndexPath *currentdropDownCellIndexPath = [NSIndexPath indexPathForRow:(self.selectedIndexPath.row + 1) inSection:self.selectedIndexPath.section];
        if (indexPath.row == self.selectedIndexPath.row) {
        } else {
            // Switch Dropdown Cell Location
            [tableView beginUpdates];
            // Hide Dropdown Cell
            if(currentdropDownCellIndexPath.row==1){
                [self.tableViewData
                 removeObjectAtIndex:currentdropDownCellIndexPath.row];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentdropDownCellIndexPath]
                                 withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            // Show Dropdown Cell
            NSInteger dropDownCellRow = indexPath.row +
            ((indexPath.row >= currentdropDownCellIndexPath.row) ? 0 : 1);
            dropDownCellIndexPath = [NSIndexPath indexPathForRow:dropDownCellRow inSection:indexPath.section];
            if(indexPath.row==0){
                [self.tableViewData insertObject:@"DropDownCell"
                                         atIndex:dropDownCellIndexPath.row];
                [tableView insertRowsAtIndexPaths:[NSArray
                                            arrayWithObject:dropDownCellIndexPath]
                                 withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            self.selectedIndexPath = [NSIndexPath
                                      indexPathForRow:dropDownCellIndexPath.row - 1
                                      inSection:dropDownCellIndexPath.section];
            [tableView endUpdates];
            if(indexPath.row==0 && !isTestingConnection){
                [self startConnectionTest];
            }            
        }
    }
    [tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    CALayer *btnLayer = [textBoxButton layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    textBoxButton.hidden=TRUE;
    firstActivity.hidden=TRUE;
    secondActivity.hidden=TRUE;
    optimizeImage.hidden=TRUE;
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setHidden:NO];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"SpeedCheckTitle", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:NSLocalizedString(@"BackButton", nil)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (IBAction)back{
    if(speedTest!=NULL){
        NSLog(@"closeTransfers");
        speedTest->closeTransfers();
    } else if (speedTestUp!=NULL){
        speedTestUp->closeTransfers();
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}


@end






