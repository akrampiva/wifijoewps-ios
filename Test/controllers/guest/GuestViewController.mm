//
//  GuestViewController.mm
//
//  List the devices that have guest access, either internet or full access.
//  And provide possibility to change access level of your wifi guests.
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import "GuestViewController.h"
#include <vector>
#include <string.h>
#include "hash.h"

@implementation GuestViewController

@synthesize speechBalloon;
@synthesize guestSSID;
@synthesize devicesTable;
@synthesize slideUpText;
@synthesize slideUp;
@synthesize greyout;
@synthesize slideUpImage1;
@synthesize slideUpImage2;
@synthesize slideUpImage3;
@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize cancelbutton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        guestEventHandler=new GuestStatusCommandEventHandler();
        guestEventHandler->delegate=self;
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [NSThread detachNewThreadSelector:@selector(loadsettings) toTarget:self withObject:nil];
    [NSThread detachNewThreadSelector:@selector(lookUpDevices) toTarget:self withObject:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(section==0){
        return noaccessList.size()+1; //No access
    } else if (section==1){
        return internetaccessList.size(); //Internet access
    } else if (section==2){
        return fullaccessList.size(); //Full access
    } else if (section==3){
        return lanaccessList.size(); //lan access
    }
    return 0;
}

#define LABEL_TAG 1234
#define ICON_TAG 2345

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    cell.contentView.contentMode = UIViewContentModeRedraw;
    [cell layoutSubviews];
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    cell.contentView.backgroundColor =[UIColor clearColor];
    UIView *greenRoundedCornerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,devicesTable.frame.size.width,44)];
    greenRoundedCornerView.backgroundColor = themecolor;
    greenRoundedCornerView.layer.masksToBounds = YES;
    greenRoundedCornerView.layer.cornerRadius = 10.0;
    [cell.contentView addSubview:greenRoundedCornerView];
    [cell.contentView sendSubviewToBack:greenRoundedCornerView];
    UIView *whiteRoundedCornerView = [[UIView alloc] initWithFrame:CGRectMake(0,44,devicesTable.frame.size.width,6.0)];
    whiteRoundedCornerView.backgroundColor = [UIColor clearColor];
    whiteRoundedCornerView.layer.masksToBounds = NO;
    [cell.contentView addSubview:whiteRoundedCornerView];
    [cell.contentView sendSubviewToBack:whiteRoundedCornerView];
    cell.backgroundColor =[UIColor clearColor];
    CGRect custframe = CGRectMake(30, 7, 30, 30);
    UIImageView *img = [[UIImageView alloc] initWithFrame:custframe];
    img.backgroundColor=[UIColor clearColor];
    UIImage *internetaccess = [self imageWithImage:[UIImage imageNamed:@"internet_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *homeaccess = [self imageWithImage:[UIImage imageNamed:@"home_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *fullaccess = [self imageWithImage:[UIImage imageNamed:@"full_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *noaccess = [self imageWithImage:[UIImage imageNamed:@"no_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    if([indexPath section]==0 && [indexPath row]==0){
        [img setImage:[UIImage imageNamed:@"noaccessiconsearchingblue.png"]];
    } else if([indexPath section]==0){
        [img setImage:noaccess];
    } else if ([indexPath section]==1){
        [img setImage:internetaccess];
    } else if([indexPath section]==2){
        [img setImage:fullaccess];
    } else if([indexPath section]==3){
        [img setImage:homeaccess];
    }
    [cell.contentView addSubview:img];
	cell.selectionStyle=UITableViewCellSelectionStyleGray;
        CGRect customframe = CGRectMake(70, 0, devicesTable.frame.size.width-70, 44);
        UILabel *lbl = [[UILabel alloc] initWithFrame:customframe];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = [UIColor whiteColor];
        lbl.font=[UIFont systemFontOfSize:20];
        if([indexPath section]==0 && [indexPath row]==0){
            lbl.text=NSLocalizedString(@"GuestSearching", nil);
            UIActivityIndicatorView *spinner = [[[UIActivityIndicatorView alloc]
                                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
            UIImage *spacer = [UIImage imageNamed:@"spacer"];
            UIGraphicsBeginImageContext(spinner.frame.size);
            [spacer drawInRect:CGRectMake(0,0,30, 30)];
            UIImage* resizedSpacer = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [img setImage:resizedSpacer];
            [spinner setColor:[UIColor whiteColor]];
            [spinner setFrame:CGRectMake(0.0, 0.0, 30, 30)];
            [img addSubview:spinner];
            [spinner startAnimating];
            lbl.textColor=[UIColor whiteColor];
        } else {
            int nrEl=[indexPath row];
            if([indexPath section]==0){ //no access
                GuestEntry entry= noaccessList.at(nrEl-1);
                if([[NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]] isEqualToString:@"*"]){
                    lbl.text = [NSString stringWithCString:entry.ip.c_str() encoding:[NSString defaultCStringEncoding]];
                } else {
                    lbl.text = [NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]];
                }
            } else if ([indexPath section]==1){ //internet access
                GuestEntry entry= internetaccessList.at(nrEl);
                if([[NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]] isEqualToString:@"*"]){
                    lbl.text = [NSString stringWithCString:entry.ip.c_str() encoding:[NSString defaultCStringEncoding]];
                } else {
                    lbl.text = [NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]];
                }
            } else if([indexPath section]==2){ //full access
                GuestEntry entry= fullaccessList.at(nrEl);
                if([[NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]] isEqualToString:@"*"]){
                    lbl.text = [NSString stringWithCString:entry.ip.c_str() encoding:[NSString defaultCStringEncoding]];
                } else {
                    lbl.text = [NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]];
                }
            } else if([indexPath section]==3){ //lan access
                GuestEntry entry= lanaccessList.at(nrEl);
                if([[NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]] isEqualToString:@"*"]){
                    lbl.text = [NSString stringWithCString:entry.ip.c_str() encoding:[NSString defaultCStringEncoding]];
                } else {
                    lbl.text = [NSString stringWithCString:entry.hostname.c_str() encoding:[NSString defaultCStringEncoding]];
                }
            }
        }
        lbl.font = [UIFont systemFontOfSize:20];
        lbl.tag=LABEL_TAG;
        [cell.contentView addSubview:lbl];
        [lbl release];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [devicesTable deselectRowAtIndexPath:indexPath animated:YES];
    if([indexPath section]==0 && [indexPath row]>0){
        currentGuest=&noaccessList.at([indexPath row]-1);
        [self performSelectorOnMainThread:@selector(noAccess:) withObject:indexPath waitUntilDone:YES];
    } else if ([indexPath section]==1){
        currentGuest=&internetaccessList.at([indexPath row]);
        [self performSelectorOnMainThread:@selector(internetAccess:) withObject:indexPath waitUntilDone:YES];
    } else if([indexPath section]==2){
        currentGuest=&fullaccessList.at([indexPath row]);
        [self performSelectorOnMainThread:@selector(fullAccess:) withObject:indexPath waitUntilDone:YES];
    } else if([indexPath section]==3){
        currentGuest=&lanaccessList.at([indexPath row]);
        [self performSelectorOnMainThread:@selector(lanAccess:) withObject:indexPath waitUntilDone:YES];
    }
    [devicesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==1 && internetaccessList.size()==0){
        return 0;
    } else if(section==2 && fullaccessList.size()==0){
        return 0;
    } else if(section==3 && lanaccessList.size()==0){
        return 0;
    }
    return 40;
}

-(UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CGRect frame = tableView.bounds;
    UIView* customView;
    if(section==1 && internetaccessList.size()==0){
         frame.size.height = 0;
        customView = [[[UIView alloc] initWithFrame:frame] autorelease];
        customView.backgroundColor = [UIColor clearColor];
    } else if(section==2 && fullaccessList.size()==0){
         frame.size.height = 0;
        customView = [[[UIView alloc] initWithFrame:frame] autorelease];
        customView.backgroundColor = [UIColor clearColor];
    } else if(section==3 && lanaccessList.size()==0){
         frame.size.height = 0;
        customView = [[[UIView alloc] initWithFrame:frame] autorelease];
        customView.backgroundColor = [UIColor clearColor];
    } else {
    UIColor *themecolor = [UIColor colorWithRed:0.4 green:0.8 blue:0.2 alpha:1]; /*#66cc33*/
    frame.size.height = 30;
    customView = [[[UIView alloc] initWithFrame:frame] autorelease];
    customView.backgroundColor = [UIColor clearColor];
    UILabel *headerLabel = [[[UILabel alloc] initWithFrame:CGRectInset(frame, 10, 0)] autorelease];
    headerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:20];
    if(section==0){
        headerLabel.text=NSLocalizedString(@"GuestNoAccess", nil);
    } else  if (section==1 && internetaccessList.size()>0){
        headerLabel.text=NSLocalizedString(@"GuestInternetAccess", nil);
    } else if (section==2 && fullaccessList.size()>0){
        headerLabel.text=NSLocalizedString(@"GuestFullAccess", nil);
    } else if (section==3 && lanaccessList.size()>0){
        headerLabel.text=NSLocalizedString(@"GuestLanAccess", nil);
    }
    headerLabel.textColor=[UIColor blackColor];
    [customView addSubview:headerLabel];
    UIView *whiteRoundedCornerView = [[UIView alloc] initWithFrame:CGRectMake(0,30,tableView.bounds.size.width,3)];
    whiteRoundedCornerView.backgroundColor = themecolor;
    whiteRoundedCornerView.layer.masksToBounds = NO;
    [customView addSubview:whiteRoundedCornerView];
    [customView sendSubviewToBack:whiteRoundedCornerView];
    UIView *roundedCornerView = [[UIView alloc] initWithFrame:CGRectMake(0,33,tableView.bounds.size.width,7)];
    roundedCornerView.backgroundColor = [UIColor clearColor];
    roundedCornerView.layer.masksToBounds = NO;
    [customView addSubview:roundedCornerView];
    [customView sendSubviewToBack:roundedCornerView];
    }
    return customView;
}

-(void)loadsettings{
    try {
        GuestIface wlsettings = client->cf->getGuestnet();
        NSString *ssid = [NSString stringWithCString:wlsettings.ssid.c_str() encoding:[NSString defaultCStringEncoding]];
        guestSSID.text = ssid;
    } catch (std::exception &e) {
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:[NSString stringWithCString:e.what() encoding:NSUTF8StringEncoding] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    } catch (...){
        UIAlertView *av=[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FatalError", nil) message:NSLocalizedString(@"FatalErrorDesc", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [av performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:TRUE];
        if(client!=NULL && client->cf!=NULL && client->cf->cf!=NULL){
            client->cf->cf->disconnect();
        }
    }
}

-(IBAction)lookUpDevices{
        std::vector<DhcpLease> leases = client->cf->getLeases(guestEventHandler);
        std::string ip;
        std::string vendor;
        std::string ip_vendor;
        deviceMap.clear();
        for (int i=0; i < leases.size(); i++) {
            if(closed){
                break;
            }
            ip = leases.at(i).ip.c_str();
            if(leases.at(i).type==GUEST){
                if (client->cf->arpPing(ip) == 1){
                    if(leases.at(i).access == LAN){
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, true, LANACCESS);
                    } else if(leases.at(i).access == WAN){
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, true, INTERNETACCESS);
                    } else if(leases.at(i).access == FULL){
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, true, FULLACCESS);
                    } else {
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, true, NOACCESS);
                    }
                }
                else{
                    if(leases.at(i).access == LAN){
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, false, LANACCESS);
                    } else if(leases.at(i).access == WAN){
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, false, INTERNETACCESS);
                    } else if(leases.at(i).access == FULL){
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, false, FULLACCESS);
                    } else {
                        deviceMap[leases.at(i).mac]=GuestEntry(leases.at(i).mac, leases.at(i).ip, leases.at(i).hostname, leases.at(i).type, false, NOACCESS);
                    }
                }
            } else {
                NSLog(@"NO GUEST");
            }
        }
        NSLog(@"\n\n\n%li leases done\n\n",leases.size());
        NSLog(@"\n%li nr of no access\n",noaccessList.size());
        NSLog(@"\n%li nr of internet access\n",internetaccessList.size());
        NSLog(@"\n%li nr of full access\n",fullaccessList.size());
        NSLog(@"\n%li nr of lan access\n",lanaccessList.size());
        noaccessList.clear();
        internetaccessList.clear();
        fullaccessList.clear();
        lanaccessList.clear();
        for (GuestMapType::iterator it= deviceMap.begin();it!=deviceMap.end();++it){
            GuestEntry entry=it->second;
            if(entry.access==NOACCESS){
                noaccessList.push_back(entry);
            } else if(entry.access==INTERNETACCESS){
                internetaccessList.push_back(entry);
            } else if(entry.access==FULLACCESS){
                fullaccessList.push_back(entry);
            } else if(entry.access==LANACCESS){
                lanaccessList.push_back(entry);
            }
        }
        [devicesTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

-(IBAction)textFieldDoneEditing:(id)sender{
    [sender resignFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    CALayer *btnLayer1 = [cancelbutton layer];
    [btnLayer1 setMasksToBounds:YES];
    [btnLayer1 setCornerRadius:10.0f];
    CALayer *btnLayer2 = [button1 layer];
    [btnLayer2 setMasksToBounds:YES];
    [btnLayer2 setCornerRadius:10.0f];
    CALayer *btnLayer3 = [button2 layer];
    [btnLayer3 setMasksToBounds:YES];
    [btnLayer3 setCornerRadius:10.0f];
    CALayer *btnLayer4 = [button3 layer];
    [btnLayer4 setMasksToBounds:YES];
    [btnLayer4 setCornerRadius:10.0f];
    slideUp.backgroundColor=[UIColor clearColor];
    slideUp.hidden = TRUE;
    greyout.hidden = TRUE;
    devicesTable.backgroundColor=[UIColor clearColor];
    [devicesTable setBackgroundView:nil];
    [super viewWillAppear:animated];
    [[self.navigationController navigationBar] setHidden:NO];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = NSLocalizedString(@"GuestTitle", nil) ;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.navigationItem setTitleView:titleLabel];
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 35,35);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (IBAction)back{
    guestEventHandler->unsubscribe();
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

// When a new device is clicked, show possibilities "internet access", "full access" and "cancel".
- (IBAction) noAccess: (id) sender{
    NSIndexPath * index=(NSIndexPath *) sender;
    if([index row]==0){
        NSString* startString=NSLocalizedString(@"GuestAllowAccess", nil);
        [slideUpText setText:[startString stringByAppendingString:NSLocalizedString(@"GuestSearching", nil)]];
    } else {
        NSString* startString=NSLocalizedString(@"GuestAllowAccess", nil);
        [slideUpText setText:[startString stringByAppendingString:[NSString stringWithCString:currentGuest->ip.c_str() encoding:[NSString defaultCStringEncoding] ]]];
    }
    [button1 setTitle:[NSLocalizedString(@"GuestInternetAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button2 setTitle:[NSLocalizedString(@"GuestLanAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button3 setTitle:[NSLocalizedString(@"GuestFullAccess", nil) uppercaseString] forState:UIControlStateNormal];
    UIImage *internetaccess = [self imageWithImage:[UIImage imageNamed:@"internet_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *homeaccess = [self imageWithImage:[UIImage imageNamed:@"home_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *fullaccess = [self imageWithImage:[UIImage imageNamed:@"full_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    [slideUpImage1 setImage:internetaccess];
    [slideUpImage2 setImage:homeaccess];
    [slideUpImage3 setImage:fullaccess];
    [self showSlideUpBar];
}

// When an internet-access device is clicked, show possibilities "no access", "full access" and "cancel".
- (IBAction) internetAccess:(id)sender{
    NSString* startString=NSLocalizedString(@"GuestAllowAccess", nil);
    [slideUpText setText:[startString stringByAppendingString:[NSString stringWithCString:currentGuest->ip.c_str() encoding:[NSString defaultCStringEncoding] ]]];
    [button1 setTitle:[NSLocalizedString(@"GuestNoAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button2 setTitle:[NSLocalizedString(@"GuestLanAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button3 setTitle:[NSLocalizedString(@"GuestFullAccess", nil) uppercaseString] forState:UIControlStateNormal];
    UIImage *homeaccess = [self imageWithImage:[UIImage imageNamed:@"home_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *fullaccess = [self imageWithImage:[UIImage imageNamed:@"full_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *noaccess = [self imageWithImage:[UIImage imageNamed:@"no_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    [slideUpImage1 setImage:noaccess];
    [slideUpImage2 setImage:homeaccess];
    [slideUpImage3 setImage:fullaccess];
    [self showSlideUpBar];
}

// When a full-access device is clicked, show possibilities "no access", "internet access" and "cancel".
- (IBAction) fullAccess:(id)sender{
    NSString* startString=NSLocalizedString(@"GuestAllowAccess", nil);
    [slideUpText setText:[startString stringByAppendingString:[NSString stringWithCString:currentGuest->ip.c_str() encoding:[NSString defaultCStringEncoding] ]]];
    [button1 setTitle:[NSLocalizedString(@"GuestNoAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button2 setTitle:[NSLocalizedString(@"GuestLanAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button3 setTitle:[NSLocalizedString(@"GuestInternetAccess", nil) uppercaseString] forState:UIControlStateNormal];
    UIImage *internetaccess = [self imageWithImage:[UIImage imageNamed:@"internet_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *homeaccess = [self imageWithImage:[UIImage imageNamed:@"home_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *noaccess = [self imageWithImage:[UIImage imageNamed:@"no_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    [slideUpImage1 setImage:noaccess];
    [slideUpImage2 setImage:homeaccess];
    [slideUpImage3 setImage:internetaccess];
    [self showSlideUpBar];
}

- (IBAction) lanAccess:(id)sender{
    NSString* startString=NSLocalizedString(@"GuestAllowAccess", nil);
    [slideUpText setText:[startString stringByAppendingString:[NSString stringWithCString:currentGuest->ip.c_str() encoding:[NSString defaultCStringEncoding] ]]];
    [button1 setTitle:[NSLocalizedString(@"GuestNoAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button2 setTitle:[NSLocalizedString(@"GuestInternetAccess", nil) uppercaseString] forState:UIControlStateNormal];
    [button3 setTitle:[NSLocalizedString(@"GuestFullAccess", nil) uppercaseString] forState:UIControlStateNormal];
    UIImage *internetaccess = [self imageWithImage:[UIImage imageNamed:@"internet_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *fullaccess = [self imageWithImage:[UIImage imageNamed:@"full_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    UIImage *noaccess = [self imageWithImage:[UIImage imageNamed:@"no_access_white_icon.png"] scaledToSize:CGSizeMake(30, 30)];
    [slideUpImage1 setImage:noaccess];
    [slideUpImage2 setImage:internetaccess];
    [slideUpImage3 setImage:fullaccess];
    [self showSlideUpBar];
}

// Hide the slide bar with options
- (IBAction) cancel:(id)sender{
    slideUp.backgroundColor=[UIColor clearColor];
    slideUp.hidden = TRUE;
    greyout.hidden = TRUE;
}

-(void)setGuestRule:(NSArray*) val{
    Result res=DENY;
    if([[val objectAtIndex:1] intValue] == 1 ){
        res=WAN;
    } else if([[val objectAtIndex:1] intValue] == 2 ){
        res=LAN;
    } else if([[val objectAtIndex:1] intValue] == 3 ){
        res=FULL;
    }
    GuestRule mystruct=GuestRule([[val objectAtIndex:0] cString],res );
    client->cf->setGuestRule(mystruct.ip, mystruct.level);
}

- (IBAction) firstButtonClicked:(id)sender{
    if(currentGuest!=nil){
        if(currentGuest->access==NOACCESS){
            //clicked internet access
            std::string guestIP=currentGuest->ip;
            internetaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,INTERNETACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= noaccessList.begin();iter!=noaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            noaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:WAN],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        } else if (currentGuest->access==INTERNETACCESS){
            //clicked no access
            std::string guestIP=currentGuest->ip;
            noaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,NOACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= internetaccessList.begin();iter!=internetaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            internetaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:DENY],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        }  else if (currentGuest->access==FULLACCESS){
            //clicked no access
            std::string guestIP=currentGuest->ip;
            noaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,NOACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= fullaccessList.begin();iter!=fullaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            fullaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:DENY],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        } else if (currentGuest->access==LANACCESS){
            //clicked no access
            std::string guestIP=currentGuest->ip;
            noaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,NOACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= lanaccessList.begin();iter!=lanaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            lanaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:DENY],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        }
    }
    slideUp.backgroundColor=[UIColor clearColor];
    slideUp.hidden = TRUE;
    greyout.hidden = TRUE;
    [devicesTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction) secondButtonClicked:(id)sender{
    if(currentGuest!=nil){
        if(currentGuest->access==NOACCESS){
            //clicked full access
            std::string guestIP=currentGuest->ip;
            lanaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,LANACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= noaccessList.begin();iter!=noaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            noaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:LAN],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        } else if (currentGuest->access==INTERNETACCESS){
            //clicked full access
            std::string guestIP=currentGuest->ip;
            lanaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,LANACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= internetaccessList.begin();iter!=internetaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            internetaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:LAN],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        }  else if (currentGuest->access==FULLACCESS){
            //clicked internet access
            std::string guestIP=currentGuest->ip;
            lanaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,LANACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= fullaccessList.begin();iter!=fullaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            fullaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:LAN],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        } else if (currentGuest->access==LANACCESS){
            //clicked internet access
            std::string guestIP=currentGuest->ip;
            internetaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,INTERNETACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= lanaccessList.begin();iter!=lanaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            lanaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:WAN],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        }
    }
    slideUp.backgroundColor=[UIColor clearColor];
    slideUp.hidden = TRUE;
    greyout.hidden = TRUE;
    [devicesTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction) thirdButtonClicked:(id)sender{
    if(currentGuest!=nil){
        if(currentGuest->access==NOACCESS){
            //clicked full access
            std::string guestIP=currentGuest->ip;
            fullaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,FULLACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= noaccessList.begin();iter!=noaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            noaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:FULL],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        } else if (currentGuest->access==INTERNETACCESS){
            //clicked full access
            std::string guestIP=currentGuest->ip;
            fullaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,FULLACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= internetaccessList.begin();iter!=internetaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            internetaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:FULL],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        }  else if (currentGuest->access==FULLACCESS){
            //clicked internet access
            std::string guestIP=currentGuest->ip;
            internetaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,INTERNETACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= fullaccessList.begin();iter!=fullaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            fullaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:WAN],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        } else if (currentGuest->access==LANACCESS){
            //clicked internet access
            std::string guestIP=currentGuest->ip;
            fullaccessList.push_back(GuestEntry(currentGuest->mac, currentGuest->ip, currentGuest->hostname, currentGuest->type, currentGuest->active,FULLACCESS));
            std::vector<GuestEntry>::iterator toremove;
            for (std::vector<GuestEntry>::iterator iter= lanaccessList.begin();iter!=lanaccessList.end();++iter){
                if(std::strcmp(iter->mac.c_str(),currentGuest->mac.c_str())==0){
                    toremove=iter;
                }
            }
            lanaccessList.erase(toremove);
            NSString *strIP = [[NSString alloc] initWithUTF8String:guestIP.c_str()];
            NSArray  * myArray = [[NSArray alloc] initWithObjects:strIP,[NSNumber numberWithInt:FULL],nil];
            [NSThread detachNewThreadSelector:@selector(setGuestRule:) toTarget:self withObject:myArray];
        }
    }
    slideUp.backgroundColor=[UIColor clearColor];
    slideUp.hidden = TRUE;
    greyout.hidden = TRUE;
    [devicesTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

// show slide bar with options
- (void) showSlideUpBar{
    slideUp.backgroundColor=[UIColor whiteColor];
    slideUp.hidden = FALSE;
    greyout.hidden = FALSE;
}

@end