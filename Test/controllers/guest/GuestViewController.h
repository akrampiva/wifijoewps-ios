//
//  GuestViewController.h
//
//  List the devices that have guest access, either internet or full access.
//  And provide possibility to change access level of your wifi guests.
//
//  Created by Kurt Vermeersch.
//  Copyright 2012 Cloud Friends. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"
#import "SpeechBalloonView.h"
#import "WifiJoe.h"


typedef enum{NOACCESS=1, INTERNETACCESS=2, FULLACCESS=3, LANACCESS=4} AccessLevel;

struct GuestRule{
    inline GuestRule(){}
	inline GuestRule(std::string i, Result r):
    ip(i), level(r)
	{}

    
    std::string ip;
    Result level;
};

struct GuestEntry{
    inline GuestEntry(){}
	inline GuestEntry(std::string m, std::string i, std::string h, Result t, bool a,AccessLevel ac):
    mac(m), ip(i), hostname(h), type(t), active(a), access(ac)
	{}
    
	std::string	mac;
	std::string	ip;
	std::string	hostname;
	Result		type;
    bool    active;
    AccessLevel access;
};

typedef std::map<std::string,GuestEntry> GuestMapType;
typedef struct GuestRule GuestRule;

@interface GuestViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    IBOutlet SpeechBalloonView *speechBalloon;
    IBOutlet UILabel *guestSSID;
    IBOutlet UITableView *devicesTable;
    IBOutlet UIView *slideUp;
    IBOutlet UIView *greyout;
    IBOutlet UILabel *slideUpText;
    IBOutlet UIButton *button1;
    IBOutlet UIButton *button2;
    IBOutlet UIButton *button3;
    IBOutlet UIButton *cancelbutton;
    IBOutlet UIImageView *slideUpImage1;
    IBOutlet UIImageView *slideUpImage2;
    IBOutlet UIImageView *slideUpImage3;
    bool closed;
    @public
    CloudFriendsWrapper *client;
    GuestMapType deviceMap;
    std::vector<GuestEntry> noaccessList;
    std::vector<GuestEntry> internetaccessList;
    std::vector<GuestEntry> fullaccessList;
    std::vector<GuestEntry> lanaccessList;
    GuestEntry* currentGuest;
    
    class GuestStatusCommandEventHandler : public CommandEventHandler
    {
    public:
        GuestViewController *delegate;
        
        GuestStatusCommandEventHandler(){
        }
        
        virtual ~GuestStatusCommandEventHandler(){
        }
        
        virtual void commandReply(const std::string &topic,
                                  CommandEntry *cmd, const std::string & from){
        }
        virtual void commandError(const std::string &topic, CommandEntry *cmd,
                                  const std::string & from, CommandHandler::ErrorType err){
        }
        virtual void commandEvent(const std::string  eventSubject, const std::string value){
            SEL selector = NSSelectorFromString(@"lookUpDevices");
            [NSThread detachNewThreadSelector:selector toTarget:delegate withObject:nil];
        }

    };
    GuestStatusCommandEventHandler *guestEventHandler;
    

}
@property (nonatomic, retain) SpeechBalloonView *speechBalloon;
@property (nonatomic, retain) UILabel *guestSSID;
@property (nonatomic, retain) UITableView *devicesTable;
@property (nonatomic, retain) UILabel *slideUpText;
@property (nonatomic, retain) UIView *slideUp;
@property (nonatomic, retain) UIView *greyout;
@property (nonatomic, retain) UIImageView *slideUpImage1;
@property (nonatomic, retain) UIImageView *slideUpImage2;
@property (nonatomic, retain) UIImageView *slideUpImage3;
@property (nonatomic, retain) UIButton *button1;
@property (nonatomic, retain) UIButton *button2;
@property (nonatomic, retain) UIButton *button3;
@property (nonatomic, retain) UIButton *cancelbutton;

- (void) showSlideUpBar;
- (IBAction) noAccess: (id) sender;
- (IBAction) internetAccess: (id) sender;
- (IBAction) fullAccess: (id) sender;
- (IBAction) lanAccess: (id) sender;
- (IBAction) firstButtonClicked:(id)sender;
- (IBAction) secondButtonClicked:(id)sender;
- (IBAction) thirdButtonClicked:(id)sender;
- (IBAction) cancel: (id)sender;
- (IBAction) back;
- (IBAction) lookUpDevices;

@end
