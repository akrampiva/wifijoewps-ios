//
//  ApiManager.m
//  iopsys Wifi Joe
//
//  Created by PIVA on 21/03/14.
//
//

#import "ApiManager.h"
#import "CloudFriendsWrapper.h"
#import "conf_store.h"




@implementation ApiManager

    dispatch_queue_t queue;
    UIAlertView* alertView;
    BOOL apisAvailable;

+ (ApiManager *)sharedInstance
{
    static ApiManager *sharedInstance;
    
    @synchronized(self)
    {
        if (!sharedInstance)
        sharedInstance = [[ApiManager alloc] init];
        queue = dispatch_queue_create("com.example.MyQueue", NULL);
        apisAvailable = NO;
        return sharedInstance;
    }
}

//Api checking
- (BOOL) isApiAvailableWithCloudFriendWrapper:(CloudFriendsWrapper*) cf{
    std::string to = cf->cf->getGatewayJID();
    NSString* gatewayJid = [NSString stringWithFormat:@"%s",to.c_str()];
    ParamEntry::valueList_t valueList = cf->cf->cf->getRemoteTopicList([gatewayJid UTF8String]);
    
    NSMutableArray* topicList=[[NSMutableArray alloc] init];
    for (std::list<std::string>::iterator itn = valueList.begin(); itn != valueList.end(); itn++) {
        NSString* tc=[NSString stringWithUTF8String:(*itn).c_str()];
        [topicList addObject:tc];
    }
    
    int j = 0;
    while (j < topicList.count) {
        //NSLog(@"LoginActivity: topic present: %@",[topicList objectAtIndex:j]);
        if ([[topicList objectAtIndex:j] isEqualToString:A_TOPIC]) {
            //NSLog(@"LoginActivity: topic already installed: %@",[topicList objectAtIndex:j]);
                    return YES;
        }
        j++;
    }
    return NO;
}

- (void) showAlertWithTitle:(NSString*)title andMessage:(NSString*) message andDelegate:(UIViewController*)delegate andTag:(int)tag{
    if(alertView && alertView.isVisible)
    {
        
    }
    else{
        alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:NSLocalizedString(@"Cancel", nil), nil];
        alertView.tag=tag;
        [alertView performSelector:@selector(show) withObject:nil afterDelay:0.3];
    }
    
}


- (BOOL) connectedToTheServer:(CloudFriendsWrapper*) cf{
    return cf->cf->deviceIsConnected();
}

- (BOOL) gatewayAvailable:(CloudFriendsWrapper*) cf{
    NSLog(@"gatewayAvailable");
    std::string to = cf->cf->getGatewayJID();
    NSString* gatewayJid = [NSString stringWithFormat:@"%s",to.c_str()];
    if(gatewayJid && gatewayJid.length > 0)
        return YES;
    else
        return NO;
}

- (void) makeAPIsAvailableWithdeviceAccount:(NSString*) deviceAccount andTopic:(NSString*)topic andApiName:(NSString*)apiName andCloudFriendWrapper:(CloudFriendsWrapper*) cf finishBlock:(void (^)(BOOL installed))block{
    
    dispatch_async(queue, ^()
                   {
                       
                      //NSLog(@"LoginActivity: Need to install topic: %@", topic);
                       
                       CommandEntry* cmd = new CommandEntry("execute", "");
                       cmd->setInput("gatewayJID", [deviceAccount UTF8String]);
                       cmd->setInput("widget", [apiName UTF8String]);
                       
                       
                       BlockedCommandHandler* request = new BlockedCommandHandler();
                       
                       char buffer[100];
                       char * serverStr=configGet("Core","cloudfriends_host",buffer,sizeof(buffer),true);
                       NSString* server = [NSString stringWithFormat:@"%s",serverStr];
                       
                       NSString* dst = [NSString stringWithFormat:@"packagemanager.%@",server ];
                       
                       
                       cf->cf->cf->sendCommand("upgrade", cmd, [dst UTF8String], request);
                       
                       
                       BlockedCommandResult response = request->waitForResult();
                       if (response.resultType != BlockedCommandResult::REPLY) {
                           dispatch_async(dispatch_get_main_queue(), ^()
                                          {
                                              block(NO);
                                             
                                          });
                            return;
                       }
                       else
                       {
                           dispatch_async(dispatch_get_main_queue(), ^()
                                          {
                                              block(YES);
                                              return;
                                          });
                           
                       }
                       
                       
                   });
}

@end
