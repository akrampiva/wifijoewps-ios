//
//  WE3ConnectViewController.h
//  iopsys Wifi Joe
//
//  Created by PIVA on 22/07/14.
//
//

#import <UIKit/UIKit.h>
#import "CloudFriendsWrapper.h"

@interface WE3ConnectViewController : UIViewController
{
    NSString* instalationType;
    CloudFriendsWrapper *client;
    BOOL processCanceled;
    int currentInstruction;
    BOOL wirelessStepAlertViewlaunched;
    BOOL tryToConnectAlertViewLaunched;
    BOOL successAlertViewLaunched;
    BOOL failAlertViewLaunched;
    BOOL extenderAvailable;
    std::string wpsStatus;
    NSTimer* autoTimer;
    int maxTime;
    UIView* alertDisabledView;
}


@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* step1Indicator;
@property (nonatomic,strong) IBOutlet UIImageView* step1CheckImage;

@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* step2Indicator;
@property (nonatomic,strong) IBOutlet UIImageView* step2CheckImage;

@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* step3Indicator;
@property (nonatomic,strong) IBOutlet UIImageView* step3CheckImage;

//
@property (nonatomic,strong) IBOutlet UIView* instructionsView;

@property (nonatomic,strong) IBOutlet UILabel* instruction1Label;
@property (nonatomic,strong) IBOutlet UILabel* instruction2Label;
@property (nonatomic,strong) IBOutlet UILabel* instruction3Label;
@property (nonatomic,strong) IBOutlet UILabel* instruction4Label;

@property (nonatomic,strong) IBOutlet UIImageView* instruction1Next;
@property (nonatomic,strong) IBOutlet UIImageView* instruction2Next;
@property (nonatomic,strong) IBOutlet UIImageView* instruction3Next;
@property (nonatomic,strong) IBOutlet UIImageView* instruction4Next;

@property (nonatomic,strong) IBOutlet UIButton* okButton;
@property (nonatomic,strong) IBOutlet UIButton* cancelButton;

@property (nonatomic,strong) IBOutlet UIImageView* currentInstructionImage;

@property (nonatomic,strong) IBOutlet UIView* wirelessStepAlertView;
@property (nonatomic,strong) IBOutlet UIView* tryToConnectAlertView;
@property (nonatomic,strong) IBOutlet UIView* successAlertView;
@property (nonatomic,strong) IBOutlet UIView* failAlertView;


@property (nonatomic,strong) IBOutlet UIActivityIndicatorView* tryToConnectAlertIndicator;

@property (nonatomic,strong) IBOutlet UIButton* cancelButtonFailAlertView;
@property (nonatomic,strong) IBOutlet UIButton* restartButtonFailAlertView;

@property (nonatomic,strong) IBOutlet UIButton* okButtonSuccessAlertView;


-(id) initWithClient:(CloudFriendsWrapper*) cfwrap andInstalationType:(NSString*)type;


@end
